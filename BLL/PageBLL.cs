﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.ComponentModel;
using System.Data.SqlClient;
using Common.Entity;

namespace BLL
{
    public static class PageBLL
    {

        #region Methods

        public static Common.Entity.Page GetRow(int pageId)
        {
            return DAL.PageDAL.GetRowByLevel(pageId, Utility.RowFetchLevel);
        }

        public static Common.Entity.Page GetRowLastLevel(int pageId)
        {
            return DAL.PageDAL.GetRowByLevel(pageId, 0);
        }

        public static int GetCount()
        {
            return DAL.PageDAL.GetCount();
        }

        public static short GetPageId(string strPageName)
        {
            return DAL.PageDAL.GetPageId(strPageName);
        }

        #endregion /Methods


    }

}
