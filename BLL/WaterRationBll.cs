﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class WaterRationBLL
    {

        public static Common.Entity.WaterRation GetRowByLevel(int WaterRationId, int fetchLevel)
        {
            return DAL.WaterRationDAL.GetRowByLevel(WaterRationId,fetchLevel);
        }

        public static Common.Entity.WaterRation Insert(Common.Entity.WaterRation waterRation)
        {
            return DAL.WaterRationDAL.Insert(waterRation);
        }

        public static Common.Entity.WaterRation Update(Common.Entity.WaterRation waterRation)
        {
            return DAL.WaterRationDAL.Update(waterRation);
        }

        public static void Delete(int WaterRationId)
        {
            DAL.WaterRationDAL.Delete(WaterRationId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.WaterRationDAL.GetList();
        }

    }
}
