﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class MapBLL
    {

        public static Common.Entity.Map GetRowByLevel(int MapId, int fetchLevel)
        {
            return DAL.MapDAL.GetRowByLevel(MapId,fetchLevel);
        }
        

        public static Common.Entity.Map Insert(Common.Entity.Map map)
        {
            return DAL.MapDAL.Insert(map);
        }

        public static Common.Entity.Map Update(Common.Entity.Map map)
        {
            return DAL.MapDAL.Update(map);
        }

        public static void Delete(int MapId)
        {
            DAL.MapDAL.Delete(MapId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.MapDAL.GetList();
        }

    }
}
