﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class PlantBLL
    {


        public static Common.Entity.Plant GetRowByLevel(int PlantId, int fetchLevel)
        {
            return DAL.PlantDAL.GetRowByLevel(PlantId,fetchLevel);
        }
        
        public static Common.Entity.Plant Insert(Common.Entity.Plant plant)
        {
            return DAL.PlantDAL.Insert(plant);
        }

        public static Common.Entity.Plant Update(Common.Entity.Plant plant)
        {
            return DAL.PlantDAL.Update(plant);
        }

        public static void Delete(int PlantId)
        {
            DAL.PlantDAL.Delete(PlantId);
            return;
        }

        //public static ArrayList GetList()
        //{
        //    return DAL.PlantDAL.GetList();
        //}


    }
}
