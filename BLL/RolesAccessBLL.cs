﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.ComponentModel;

namespace BLL
{
    public static class RolesAccessBLL
    {

        #region Methods
        
        public static bool HasPageAccess(Int32 userId, string pageName)
        {
            return DAL.RoleAccessDAL.HasPageAccess(userId, pageName);
        }


        #endregion /Methods

    }
}