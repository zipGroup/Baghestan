﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class CellarBLL
    {


        public static Common.Entity.Cellar GetRowByLevel(int CellarId, int fetchLevel)
        {
            return DAL.CellarDAL.GetRowByLevel(CellarId,fetchLevel);
        }
        
        public static Common.Entity.Cellar Insert(Common.Entity.Cellar cellar)
        {

            return DAL.CellarDAL.Insert(cellar);
        }

        public static Common.Entity.Cellar Update(Common.Entity.Cellar cellar)
        {
            return DAL.CellarDAL.Update(cellar);
        }

        public static void Delete(int cellarId)
        {
            DAL.CellarDAL.Delete(cellarId);
            return;
        }
        
        public static ArrayList GetList()
        {
            return DAL.CellarDAL.GetList();
        }

        public static ArrayList GetInfo(String Name,int AreaFrom,int AreaTo)
        {
            return DAL.CellarDAL.GetInfo(Name, AreaFrom, AreaTo);
        }

    }
}
