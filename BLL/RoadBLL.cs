﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class RoadBLL
    {


        public static Common.Entity.Road GetRowByLevel(int RoadId, int fetchLevel)
        {
            return DAL.RoadDAL.GetRowByLevel(RoadId,fetchLevel);
        }
        
        public static Common.Entity.Road Insert(Common.Entity.Road road)
        {
            return DAL.RoadDAL.Insert(road);
        }

        public static Common.Entity.Road Update(Common.Entity.Road road)
        {
            return DAL.RoadDAL.Update(road);
        }

        public static void Delete(int RoadId)
        {
            DAL.RoadDAL.Delete(RoadId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.RoadDAL.GetList();
        }

    }
}
