﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class ScrollBLL
    {

        public static Common.Entity.Scroll GetRowByLevel(int ScrollId, int fetchLevel)
        {
            return DAL.ScrollDAL.GetRowByLevel(ScrollId,fetchLevel);
        }

        public static Common.Entity.Scroll Insert(Common.Entity.Scroll scroll)
        {
            return DAL.ScrollDAL.Insert(scroll);
        }

        public static Common.Entity.Scroll Update(Common.Entity.Scroll scroll)
        {
            return DAL.ScrollDAL.Update(scroll);
        }

        public static void Delete(int ScrollId)
        {
            DAL.ScrollDAL.Delete(ScrollId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.ScrollDAL.GetList();
        }

    }
}
