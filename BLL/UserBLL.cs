﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class UserBLL
    {
        public static Common.Entity.User GetRow(string userName, string password)
        {
            return DAL.UserDAL.GetRow(userName, password);
        }

    }
}
