﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class RepairBLL
    {


        public static Common.Entity.Repair GetRowByLevel(int RepairId, int fetchLevel)
        {
            return DAL.RepairDAL.GetRowByLevel(RepairId,fetchLevel);
        }
        
        public static Common.Entity.Repair Insert(Common.Entity.Repair repair)
        {
            return DAL.RepairDAL.Insert(repair);
        }

        public static Common.Entity.Repair Update(Common.Entity.Repair repair)
        {
            return DAL.RepairDAL.Update(repair);
        }

        public static void Delete(int RepairId)
        {
            DAL.RepairDAL.Delete(RepairId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.RepairDAL.GetList();
        }


    }
}
