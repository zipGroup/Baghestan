﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class TreeBLL
    {

        public static Common.Entity.Tree GetRowByLevel(int TreeId, int fetchLevel)
        {
            return DAL.TreeDAL.GetRowByLevel(TreeId, fetchLevel);
        }

        public static Common.Entity.Tree Insert(Common.Entity.Tree tree)
        {
            return DAL.TreeDAL.Insert(tree);
        }

        public static Common.Entity.Tree Update(Common.Entity.Tree tree)
        {
            return DAL.TreeDAL.Update(tree);
        }

        public static void Delete(int TreeId)
        {
            DAL.TreeDAL.Delete(TreeId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.TreeDAL.GetList();
        }


        public static ArrayList GetInfo(int AgeFrom, int AgeTo,
            int ProductId, String GeneticInformation, String Name, String Status, int GardenId)
        {
            return DAL.TreeDAL.GetInfo(AgeFrom, AgeTo,
            ProductId, GeneticInformation, Name, Status, GardenId);
        }
    }
}
