﻿using DAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class BushBLL
    {


        public static Common.Entity.Bush GetRowByLevel(int BushId, int fetchLevel)
        {
            return DAL.BushDAL.GetRowByLevel(BushId, fetchLevel);
        }

        public static Common.Entity.Bush Insert(Common.Entity.Bush bush)
        {
            return DAL.BushDAL.Insert(bush);
        }

        public static Common.Entity.Bush Update(Common.Entity.Bush bush)
        {
            return DAL.BushDAL.Update(bush);
        }

        public static void Delete(int BushId)
        {
            DAL.BushDAL.Delete(BushId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.BushDAL.GetList();
        }





    }
}
