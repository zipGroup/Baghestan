﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class ExpressionBLL
    {

        public static Common.Entity.Expression GetRowByLevel(int ExpressionId, int fetchLevel)
        {
            return DAL.ExpressionDAL.GetRowByLevel(ExpressionId,fetchLevel);
        }  

        public static Common.Entity.Expression Insert(Common.Entity.Expression expression)
        {
            return DAL.ExpressionDAL.Insert(expression);
        }

        public static Common.Entity.Expression Update(Common.Entity.Expression expression)
        {
            return DAL.ExpressionDAL.Update(expression);
        }

        public static void Delete(int ExpressionId)
        {
            DAL.ExpressionDAL.Delete(ExpressionId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.ExpressionDAL.GetList();

        }


    }
}
