﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class DistrictBLL
    {
        public static Common.Entity.District GetRowByLevel(int IdDistrict, int fetchLevel)
        {
            return DAL.DistrictDAL.GetRowByLevel(IdDistrict, fetchLevel);
        }

        public static Common.Entity.District Insert(Common.Entity.District district)
        {
            return DAL.DistrictDAL.Insert(district);
        }

        public static Common.Entity.District Update(Common.Entity.District district)
        {
            return DAL.DistrictDAL.Update(district);
        }

        public static void Delete(int districtId)
        {
            DAL.DistrictDAL.Delete(districtId);
        }

        public static ArrayList GetList()
        {
            return DAL.DistrictDAL.GetList();
        }

    }
}
