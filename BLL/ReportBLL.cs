﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class ReportBLL
    {

        public static ArrayList getWaterRations()
        {
            return DAL.ReportDAL.getWaterRations();
        }

        public static ArrayList getWaterRationsSearch(int areaFrom, int areaTo, int gardenId)
        {
            return DAL.ReportDAL.getWaterRationsSearch(areaFrom, areaTo, gardenId);
        }

        public static ArrayList getTreeGardensList()
        {
            return DAL.ReportDAL.getTreeGardenList();
        }

        public static ArrayList getGardenTreeSearch(int areaFrom, int areaTo, int gardenId)
        {
            return DAL.ReportDAL.getGardenTreeSearch(areaFrom, areaTo,   gardenId);
        }
    }
}
