﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.ComponentModel;
using System.Data.SqlClient;

namespace BLL
{
    public static class MenuBLL
    {

        #region Methods

        public static Common.Entity.Menu GetRow(int menuId)
        {
            return DAL.MenuDAL.GetRowByLevel(menuId, Utility.RowFetchLevel);
        }

        public static Common.Entity.Menu GetRowLastLevel(int menuId)
        {
            return DAL.MenuDAL.GetRowByLevel(menuId, 0);
        }

        public static Common.Entity.Menu GetRowByName(string name)
        {
            return DAL.MenuDAL.GetRowByName(name);
        }

        public static List<Common.Entity.Menu> GetList(int userId)
        {
            return DAL.MenuDAL.GetList(userId);
        }

        #endregion /Methods

    }
}
