﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class PersonTypeBLL
    {

        public static Common.Entity.PersonType GetRowByLevel(int PersonTypeId, int fetchLevel)
        {
            return DAL.PersonTypeDAL.GetRowByLevel(PersonTypeId,fetchLevel);
        }
        
        public static Common.Entity.PersonType Insert(Common.Entity.PersonType personType)
        {
            return DAL.PersonTypeDAL.Insert(personType);
        }
        public static Common.Entity.PersonType Update(Common.Entity.PersonType personType)
        {
            return DAL.PersonTypeDAL.Update(personType);
        }

        public static void Delete(int PersonTypeId)
        {
            DAL.PersonTypeDAL.Delete(PersonTypeId);
            return;
        }
        public static ArrayList GetList()
        {
            return DAL.PersonTypeDAL.GetList();
        }


    }
}
