﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class ProductBLL
    {

        public static Common.Entity.Product GetRowByLevel(int ProductId, int fetchLevel)
        {
            return DAL.ProductDAL.GetRowByLevel(ProductId,fetchLevel);
        }
        
        public static Common.Entity.Product Insert(Common.Entity.Product product)
        {
            return DAL.ProductDAL.Insert(product);
        }

        public static Common.Entity.Product Update(Common.Entity.Product product)
        {
            return DAL.ProductDAL.Update(product);
        }

        public static void Delete(int ProductId)
        {
            DAL.ProductDAL.Delete(ProductId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.ProductDAL.GetList();

        }

    }
}
