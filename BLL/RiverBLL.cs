﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class RiverBLL
    {



        public static Common.Entity.River GetRowByLevel(int RiverId, int fetchLevel)
        {
            return DAL.RiverDAL.GetRowByLevel(RiverId,fetchLevel);
        }
        
        public static Common.Entity.River Insert(Common.Entity.River river)
        {
            return DAL.RiverDAL.Update(river);
        }

        public static Common.Entity.River Update(Common.Entity.River river)
        {
            return DAL.RiverDAL.Update(river);
        }

        public static void Delete(int RiverId)
        {
            DAL.RiverDAL.Delete(RiverId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.RiverDAL.GetList();
        }


    }
}
