﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class PersonBLL
    {

        public static Common.Entity.Person GetRowByLevel(String SSN, int fetchLevel)
        {
            return DAL.PersonDAL.GetRowByLevel(SSN,fetchLevel);
        }

        public static Common.Entity.Person Insert(Common.Entity.Person person)
        {
            return DAL.PersonDAL.Insert(person);
        }
        public static Common.Entity.Person Update(Common.Entity.Person person)
        {
            return DAL.PersonDAL.Update(person);
        }

        public static void Delete(String SSN)
        {
            DAL.PersonDAL.Delete(SSN);
            return;
        }
        public static ArrayList GetList()
        {
            return DAL.PersonDAL.GetList();
        }
    }
}
