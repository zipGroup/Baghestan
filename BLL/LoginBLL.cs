﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Threading.Tasks;

namespace BLL
{
    public static class LoginBLL
    {
        public static Boolean Login(String UserName,String Password)
        {
            String connectionString = String.Format(
                System.Configuration.ConfigurationManager.ConnectionStrings["BaghestanConnectionString"].ConnectionString
                ,UserName,Password);
            if( DAL.DALUtility.TestConection(connectionString))
            {
                HttpContext.Current.Session["BaghestanConnectionString"] = connectionString;
                return true;
            }else
            {
                return false;
            }
        }
    }
}
