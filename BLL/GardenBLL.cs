﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Threading.Tasks;

namespace BLL
{
    public static class GardenBLL
    {

        public static Common.Entity.Garden GetRowByLevel(int GardenId, int fetchLevel)
        {
            return DAL.GardenDAL.GetRowByLevel(GardenId,fetchLevel);
        }
        

        public static Common.Entity.Garden Insert(Common.Entity.Garden garden)
        {
            return DAL.GardenDAL.Insert(garden);
        }

        public static Common.Entity.Garden Update(Common.Entity.Garden garden)
        {
            return DAL.GardenDAL.Update(garden);
        }

        public static void Delete(Common.Entity.Garden garden)
        {
            DAL.GardenDAL.Delete(garden);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.GardenDAL.GetList();
        }

        public static ArrayList GetInfo(String Name, int AreaFrom,
            int AreaTo, double LongitudeFrom, double LongitudeTo, double LatitudeFrom, double LatitudeTo
            , int AgeFrom, int AgeTo)
        {
            return DAL.GardenDAL.GetInfo(Name,AgeFrom,AreaTo,LongitudeFrom,LongitudeTo
                ,LatitudeFrom,LatitudeTo,AgeFrom,AgeTo);
        }
    }
}
