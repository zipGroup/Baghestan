﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class PhotoBll
    {

        public static Common.Entity.Photo GetRowByLevel(int PhotoId, int fetchLevel)
        {
            return DAL.PhotoDAL.GetRowByLevel(PhotoId,fetchLevel);
        }
        
        public static Common.Entity.Photo Insert(Common.Entity.Photo photo)
        {
            return DAL.PhotoDAL.Insert(photo);
        }

        public static Common.Entity.Photo Update(Common.Entity.Photo photo)
        {
            
            return DAL.PhotoDAL.Update(photo);
        }

        public static void Delete(int PhotoId)
        {
            DAL.PhotoDAL.Delete(PhotoId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.PhotoDAL.GetList();
        }


    }
}
