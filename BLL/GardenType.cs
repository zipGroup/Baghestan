﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public static class GardenType
    {

        public static Common.Entity.GardenType GetRowByLevel(int GardenTypeId, int fetchLevel)
        {
            return DAL.GardenTypeDAL.GetRowByLevel(GardenTypeId,fetchLevel);
        }
        
        public static Common.Entity.GardenType Insert(Common.Entity.GardenType gardenType)
        {
            return DAL.GardenTypeDAL.Insert(gardenType);
        }

        public static Common.Entity.GardenType Update(Common.Entity.GardenType gardenType)
        {
            return DAL.GardenTypeDAL.Update(gardenType);
        }

        public static void Delete(int GardenTypeId)
        {
            DAL.GardenTypeDAL.Delete(GardenTypeId);
            return;
        }

        public static ArrayList GetList()
        {
            return DAL.GardenTypeDAL.GetList();
        }

    }
}
