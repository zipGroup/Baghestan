﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{
    public static class ScrollDAL
    {
        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties



        public static Common.Entity.Scroll GetRowByLevel(int ScrollId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(ScrollId);
        }


        private static Common.Entity.Scroll GetRow(int ScrollId)
        {
            var result = new Common.Entity.Scroll();

            string strCacheKey = "Scroll_ById" + ScrollId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                { 
                    dp.Command.CommandText = "Scroll_ById";
                    dp.Command.Parameters.AddWithValue("@ScrollId", ScrollId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Scroll>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Scroll>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Scroll>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.Scroll Insert(Common.Entity.Scroll scroll)
        {
            var result = new Common.Entity.Scroll();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Scroll_Insert";
           
                dp.Command.Parameters.AddWithValue("@Verifier", scroll.Verifier);
                dp.Command.Parameters.AddWithValue("@WaterRationId", scroll.WaterRation.WaterRationId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Scroll>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Scroll Update(Common.Entity.Scroll scroll)
        {
            var result = new Common.Entity.Scroll();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Scroll_Update";
                dp.Command.Parameters.AddWithValue("@ScrollId", scroll.ScrollId);
                dp.Command.Parameters.AddWithValue("@Verifier", scroll.Verifier);
                dp.Command.Parameters.AddWithValue("@WaterRationId", scroll.WaterRation.WaterRationId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Scroll>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int ScrollId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Scroll_Delete";
                dp.Command.Parameters.AddWithValue("@ScrollId", ScrollId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "River_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Scroll>(reader, FetchLevel));
                }
            }
            return result;
        }

        public static ArrayList GetInfo( String Verifier ,DateTime VerifierDateFrom , DateTime VerifierDateTo , int RiverId )
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Scroll_Info";
                dp.Connection.Open();
                if (Verifier != null || !Verifier.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Verifier", Verifier); }
                if (VerifierDateFrom != null) { dp.Command.Parameters.AddWithValue("@VerifierDateFrom", VerifierDateFrom); }
                if (VerifierDateTo != null) { dp.Command.Parameters.AddWithValue("@VerifierDateTo", VerifierDateTo); }
                if (RiverId > 0)
                { dp.Command.Parameters.AddWithValue("@RiverId", RiverId); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Scroll>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
