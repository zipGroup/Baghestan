﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{
    public static class RepairDAL
    {
        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties



        public static Common.Entity.Repair GetRowByLevel(int RepairId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(RepairId);
        }


        private static Common.Entity.Repair GetRow(int RepairId)
        {
            var result = new Common.Entity.Repair();

            string strCacheKey = "Repair_ById" + RepairId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Repair_ById";
                    dp.Command.Parameters.AddWithValue("@RepairId", RepairId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Repair>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Repair>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Repair>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.Repair Insert(Common.Entity.Repair repair)
        {
            var result = new Common.Entity.Repair();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Repair_Insert";
                dp.Command.Parameters.AddWithValue("@Date", repair.Date);
                dp.Command.Parameters.AddWithValue("@Performer", repair.Performer);
                dp.Command.Parameters.AddWithValue("@Description", repair.Description);
                dp.Command.Parameters.AddWithValue("@CellarId", repair.Cellar.CellarId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Repair>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Repair Update(Common.Entity.Repair repair)
        {
            var result = new Common.Entity.Repair();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Repair_Update";
                dp.Command.Parameters.AddWithValue("@RepairId", repair.RepairId);
                dp.Command.Parameters.AddWithValue("@Date", repair.Date);
                dp.Command.Parameters.AddWithValue("@Performer", repair.Performer);
                dp.Command.Parameters.AddWithValue("@Description", repair.Description);
                dp.Command.Parameters.AddWithValue("@CellarId", repair.Cellar.CellarId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Repair>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int RepairId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Repair_Delete";
                dp.Command.Parameters.AddWithValue("@RepairId", RepairId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        } 

        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Repair_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.River>(reader, FetchLevel));
                }
            }
            return result;

        }

        public static ArrayList GetInfo(DateTime DateFrom, DateTime DateTo,String Performer,String Description,int CellarId)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Repair_Info";
                dp.Connection.Open();
                if (DateFrom != null) { dp.Command.Parameters.AddWithValue("@DateFrom", DateFrom); }
                if (DateTo != null) { dp.Command.Parameters.AddWithValue("@DateTo", DateTo); }
                if (Performer != null || !Performer.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Performer", Performer); }
                if (Description != null || !Description.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Description", Description); }
                if (CellarId > 0)
                { dp.Command.Parameters.AddWithValue("@CellarId", CellarId); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Repair>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
