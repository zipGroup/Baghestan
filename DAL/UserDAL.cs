﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;

namespace DAL
{
    public static class UserDAL
    {

        #region Properties

        private static int FetchLevel { get; set; }

        #endregion /Properties

        #region Constructors

        static UserDAL()
        {
            FetchLevel = 1;
        }

        #endregion /Constructors

         #region Methods

        private static Common.Entity.User GetRow(int UserId)
        {
            var result = new Common.Entity.User();

            string strCacheKey = "User_ById" + UserId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "User_Profile";
                    dp.Command.Parameters.AddWithValue("@UserId", UserId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.User>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.User>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.User>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.User GetRow(string userName, string password)
        {
            var result = new Common.Entity.User();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "User_Info";
                dp.Command.Parameters.AddWithValue("@UserName", userName);
                dp.Command.Parameters.AddWithValue("@Password", password);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.User>(reader);
                }
            }
            return result;
        }

        public static Common.Entity.User GetRowByLevel(int IdUser, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(IdUser);
        }


        public static int Insert(Common.Entity.User entity)
        {
            if (!CheckCode(entity.UserId))
            {
                CacheHelper.RemoveStarts("UserList_");

                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Insert_User";
                    if (!string.IsNullOrEmpty(entity.UserName))
                    {
                        dp.Command.Parameters.AddWithValue("@UserName", entity.UserName);
                    }
                    else
                    {
                        dp.Command.Parameters.AddWithValue("@UserName", DBNull.Value);
                    }
                    if (!string.IsNullOrEmpty(entity.Password))
                    {
                        dp.Command.Parameters.AddWithValue("@Password", entity.Password);
                    }
                    else
                    {
                        dp.Command.Parameters.AddWithValue("@Password", DBNull.Value);
                    }


                    if (!string.IsNullOrEmpty(entity.FirstName))
                    {
                        dp.Command.Parameters.AddWithValue("@FirstName", entity.FirstName);
                    }
                    else
                    {
                        dp.Command.Parameters.AddWithValue("@FirstName", DBNull.Value);
                    }


                    if (!string.IsNullOrEmpty(entity.LastName))
                    {
                        dp.Command.Parameters.AddWithValue("@LastName", entity.LastName);
                    }
                    else
                    {
                        dp.Command.Parameters.AddWithValue("@LastName", DBNull.Value);
                    }

                    dp.Connection.Open();
                    entity.UserId = int.Parse(dp.Command.ExecuteScalar().ToString());
                }
            }
            return entity.UserId;
        }

        public static void Update(Common.Entity.User entity)
        {
            if (!CheckCode(entity.UserId))
            {


                CacheHelper.Remove("User_ById" + entity.UserId.ToString());
                CacheHelper.RemoveStarts("UserList_");

                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Update_User";
                    dp.Command.Parameters.AddWithValue("@IdUser", entity.UserId);
                    if (!string.IsNullOrEmpty(entity.UserName))
                    {
                        dp.Command.Parameters.AddWithValue("@UserName", entity.UserName);
                    }
                    else
                    {
                        dp.Command.Parameters.AddWithValue("@UserName", DBNull.Value);
                    }
                    if (!string.IsNullOrEmpty(entity.Password))
                    {
                        dp.Command.Parameters.AddWithValue("@Password", entity.Password);
                    }
                    else
                    {
                        dp.Command.Parameters.AddWithValue("@Password", DBNull.Value);
                    }

                    if (!string.IsNullOrEmpty(entity.FirstName))
                    {
                        dp.Command.Parameters.AddWithValue("@FirstName", entity.FirstName);
                    }
                    else
                    {
                        dp.Command.Parameters.AddWithValue("@FirstName", DBNull.Value);
                    }

                    if (!string.IsNullOrEmpty(entity.LastName))
                    {
                        dp.Command.Parameters.AddWithValue("@LastName", entity.LastName);
                    }
                    else
                    {
                        dp.Command.Parameters.AddWithValue("@LastName", DBNull.Value);
                    }
                    dp.Connection.Open();
                    dp.Command.ExecuteNonQuery();
                }
            }
        }

        public static void Delete(int id)
        {
            try
            {
                CacheHelper.Remove("User_ById" + id.ToString());
                CacheHelper.RemoveStarts("UserList_");

                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Delete_User";
                    dp.Command.Parameters.AddWithValue("@IdUser", id);
                    dp.Connection.Open();
                    dp.Command.ExecuteNonQuery();
                }
            }
            catch (SqlException sqlEx)
            {
                throw new CustomException((ExceptionKey)sqlEx.Number, sqlEx);
            }
        }

        public static List<Common.Entity.User> SelectAll()
        {
            var result = new List<Common.Entity.User>();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "UserShow_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.User>(reader, DALUtility.ListFetchLevel));
                }
            }
            return result;
        }

        public static bool Check(string userName, string firstName, string lastName, Int32 IdUser)
        {
            var result = false;
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "User_ExistsNames";
                dp.Command.Parameters.AddWithValue("@UserName", userName);
                dp.Command.Parameters.AddWithValue("@FirstName", firstName);
                dp.Command.Parameters.AddWithValue("@LastName", lastName);
                dp.Command.Parameters.AddWithValue("@IdUser", IdUser);
                dp.Connection.Open();
                result = Convert.ToBoolean(dp.Command.ExecuteScalar());
            }
            return result;
        }

        public static void ChangePassword(string password, int IdUser)
        {
            CacheHelper.Remove("User_ById" + IdUser.ToString());
            CacheHelper.RemoveStarts("UserList_");

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "User_ChangePassword";
                dp.Command.Parameters.AddWithValue("@Password", password);
                dp.Command.Parameters.AddWithValue("@IdUser", IdUser);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
        }

        public static bool CheckCode(int IdUser)
        {
            var result = false;
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "User_ExistsCode";
                dp.Command.Parameters.AddWithValue("@IdUser", IdUser);
                dp.Connection.Open();
                result = Convert.ToBoolean(dp.Command.ExecuteScalar());
            }
            return result;
        }

        

        #endregion /Methods

    }
}
