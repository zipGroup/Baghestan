﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{ 
    public static class TreeDAL
    {


        #region Properties

        private static int FetchLevel { get; set; }

        #endregion /Properties


        public static Common.Entity.Tree GetRowByLevel(int TreeId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(TreeId);
        }
        private static Common.Entity.Tree GetRow(int TreeId)
        {
            var result = new Common.Entity.Tree();

            string strCacheKey = "Tree_ById" + TreeId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Tree_ById";
                    dp.Command.Parameters.AddWithValue("@TreeId", TreeId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Tree>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Tree>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Tree>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.Tree Insert(Common.Entity.Tree tree)
        {
            var result = new Common.Entity.Tree();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Tree_Insert";
                dp.Command.Parameters.AddWithValue("@Age", tree.Age);
                dp.Command.Parameters.AddWithValue("@ProductId", tree.Product.ProductId);
                dp.Command.Parameters.AddWithValue("@GeneticInformation", tree.GeneticInformation);
                dp.Command.Parameters.AddWithValue("@Name", tree.Name);
                dp.Command.Parameters.AddWithValue("@Status", tree.Status);
                dp.Command.Parameters.AddWithValue("@GardenId", tree.Garden.GardenId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Tree>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Tree Update(Common.Entity.Tree tree)
        {
            var result = new Common.Entity.Tree();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Tree_Update";
                dp.Command.Parameters.AddWithValue("@TreeId", tree.TreeId);
                dp.Command.Parameters.AddWithValue("@Age", tree.Age);
                dp.Command.Parameters.AddWithValue("@ProductId", tree.Product.ProductId);
                dp.Command.Parameters.AddWithValue("@GeneticInformation", tree.GeneticInformation);
                dp.Command.Parameters.AddWithValue("@Name", tree.Name);
                dp.Command.Parameters.AddWithValue("@Status", tree.Status);
                dp.Command.Parameters.AddWithValue("@GardenId", tree.Garden.GardenId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Tree>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int TreeId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Tree_Delete";
                dp.Command.Parameters.AddWithValue("@TreeId", TreeId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Tree_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Tree>(reader, FetchLevel));
                }
            }
            return result;

        }

        public static ArrayList GetInfo(int AgeFrom,int AgeTo,
            int ProductId, String GeneticInformation, String Name, String Status, int GardenId)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Tree_Info";
                dp.Connection.Open();
                if (AgeFrom > 0)
                { dp.Command.Parameters.AddWithValue("@AgeFrom", AgeFrom); }
                if (AgeTo > 0)
                { dp.Command.Parameters.AddWithValue("@AgeTo", AgeTo); }
                if (ProductId > 0)
                { dp.Command.Parameters.AddWithValue("@ProductId", ProductId); }
                if (GeneticInformation != null || !GeneticInformation.Equals(""))
                { dp.Command.Parameters.AddWithValue("@GeneticInformation", GeneticInformation); }
                if (Name != null && !Name.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Name", Name); }
                if (Status != null &&  !Status.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Status", Status); }
                if (GardenId > 0)
                { dp.Command.Parameters.AddWithValue("@GardenId", GardenId); }

                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Tree>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}


