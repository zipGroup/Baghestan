﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public class DataProvider : IDisposable
    {

        #region Properties
        public string BaghestanConnectionString
        {
            get
            {
                if(HttpContext.Current.Session["BaghestanConnectionString"] != null)
                {
                    return HttpContext.Current.Session["BaghestanConnectionString"].ToString();
                }
                else
                {
                    throw new HttpException(401,"AccessDenies");
                }
            }
        }

        private SqlConnection connection;
        public SqlConnection Connection
        {
            get
            {
                if (connection == null)
                {
                    connection = new SqlConnection(BaghestanConnectionString);
                }
                return connection;
            }
        }

        private SqlCommand command;
        public SqlCommand Command
        {
            get
            {
                if (command == null)
                {
                    command = new SqlCommand()
                    {
                        Connection = this.Connection,
                        CommandType = this.Type
                    };
                }
                return command;
            }
        }

        private SqlDataAdapter dataAdapter;
        public SqlDataAdapter DataAdapter
        {
            get
            {
                if (dataAdapter == null)
                {
                    dataAdapter = new SqlDataAdapter();
                    dataAdapter.SelectCommand = this.Command;
                }
                return dataAdapter;
            }
        }

        public CommandType Type { get; set; }

        private IntPtr handle;

        // Track whether Dispose has been called.
        private bool disposed = false;

        // Use interop to call the method necessary  
        // to clean up the unmanaged resource.
        [System.Runtime.InteropServices.DllImport("Kernel32")]
        private extern static Boolean CloseHandle(IntPtr handle);

        #endregion /Properties

        #region Constructors

        public DataProvider()
        {
            this.Type = CommandType.StoredProcedure;
        }

        public DataProvider(CommandType type)
        {
            this.Type = type;
        }

        #endregion /Constructors

        #region Methods
        #endregion /Methods

        #region IDisposable Implementation

        // Implement IDisposable.
        // Do not make this method virtual.
        // A derived class should not be able to override this method.
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SupressFinalize to
            // take this object off the finalization queue 
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the 
        // runtime from inside the finalizer and you should not reference 
        // other objects. Only unmanaged resources can be disposed.
        private void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called.
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources.
                if (disposing)
                {
                    // Dispose managed resources.
                    Connection.Dispose();
                    Command.Dispose();
                    DataAdapter.Dispose();
                }

                // Call the appropriate methods to clean up 
                // unmanaged resources here.
                // If disposing is false, 
                // only the following code is executed.
                CloseHandle(handle);
                handle = IntPtr.Zero;
            }
            disposed = true;
        }

        #endregion /IDisposable Implementation

        #region Destructors

        // Use C# destructor syntax for finalization code.
        // This destructor will run only if the Dispose method 
        // does not get called.
        // It gives your base class the opportunity to finalize.
        // Do not provide destructors in types derived from this class.
        ~DataProvider()
        {
            // Do not re-create Dispose clean-up code here.
            // Calling Dispose(false) is optimal in terms of
            // readability and maintainability.
            Dispose(false);
        }

        #endregion /Destructors

    }
}
