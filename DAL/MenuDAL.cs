﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;

namespace DAL
{
    public static class MenuDAL
    {

        #region Properties

        private static int FetchLevel { get; set; }

        #endregion /Properties

        #region Constructors

        static MenuDAL()
        {
            FetchLevel = 1;
        }

        #endregion /Constructors

        #region Methods

        private static Common.Entity.Menu GetRow(int menuId)
        {
            var result = new Common.Entity.Menu();

            string strCacheKey = "Menu_ById" + menuId.ToString();
            if (FetchLevel < Utility.RowFetchLevel || !CacheHelper.Exists(strCacheKey))
            {
                using (var
                    dp = new DataProvider())
                {
                    dp.Command.CommandText = "Menu_Profile";
                    dp.Command.Parameters.AddWithValue("@MenuId", menuId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Menu>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Menu>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Menu>(strCacheKey);
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Menu GetRowByLevel(int MenuId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(MenuId);
        }

        public static Common.Entity.Menu GetRowByName(string name)
        {
            var result = new Common.Entity.Menu();

            string strCacheKey = "Menu_ByName" + name;
            if (FetchLevel < Utility.RowFetchLevel || !CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Menu_ByName";
                    dp.Command.Parameters.AddWithValue("@Name", name);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Menu>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Menu>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Menu>(strCacheKey);
            }
            return result.DeepCopy();
        }

        public static int GetCount()
        {
            var result = 0;
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Menu_Count";
                dp.Connection.Open();
                result = int.Parse(dp.Command.ExecuteScalar().ToString());
            }
            return result;
        }

        public static List<Common.Entity.Menu> GetList(int userId)
        {
            var result = new List<Common.Entity.Menu>();
            string strCacheKey = "MenuList_ByUser" + userId.ToString();
            
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "AccessUserToMenu_List";
                    dp.Command.Parameters.AddWithValue("@UserId", userId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    while (reader.Read())
                    {
                        result.Add(DALUtility.GenerateFullObject<Common.Entity.Menu>(reader));
                    }
                }

            return result;//.DeepCopy();
        }

        #endregion /Methods

    }
}
