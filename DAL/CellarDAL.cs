﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;
using Common.Entity;

namespace DAL
{
    public static class CellarDAL
    {
        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties


        public static Common.Entity.Cellar GetRowByLevel(int CellarId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(CellarId);
        }




        private static Common.Entity.Cellar GetRow(int CellarId)
        {
            var result = new Common.Entity.Cellar();

            string strCacheKey = "Cellar_ById" + CellarId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Cellar_ById";
                    dp.Command.Parameters.AddWithValue("@CellarId", CellarId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Cellar>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Cellar>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Cellar>(strCacheKey);
            }
            return result.DeepCopy();
        }

        public static Cellar Insert(Cellar cellar)
        {

            var result = new Common.Entity.Cellar();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Cellar_Insert";
                dp.Command.Parameters.AddWithValue("@Name", cellar.Name);
                dp.Command.Parameters.AddWithValue("@Area", cellar.Area);
                dp.Command.Parameters.AddWithValue("@IdDistrict", cellar.District.DistrictId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Cellar>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Cellar Update(Cellar cellar)
        {
            var result = new Common.Entity.Cellar();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Cellar_Update";
                dp.Command.Parameters.AddWithValue("@CellarId", cellar.CellarId);
                dp.Command.Parameters.AddWithValue("@Name", cellar.Name);
                dp.Command.Parameters.AddWithValue("@Area", cellar.Area);
                dp.Command.Parameters.AddWithValue("@DistirctId", cellar.District.DistrictId);
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Cellar>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int cellarId)
        {
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Cellar_Delete";
                dp.Command.Parameters.AddWithValue("@CellarId", cellarId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }



        public static ArrayList GetList()
        {

            ArrayList result = new ArrayList();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Cellar_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Cellar>(reader, FetchLevel));
                }
            }
            return result;
        }

        public static ArrayList GetInfo(String Name, int AreaFrom , int AreaTo)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Cellar_Info";
                if (Name != null && !Name.Equals(""))
                { dp.Command.Parameters.AddWithValue("@CellarName", Name); }
                else
                { dp.Command.Parameters.AddWithValue("@CellarName", DBNull.Value); }
                if (AreaFrom > 0)
                { dp.Command.Parameters.AddWithValue("@AreaFrom", AreaFrom); }
                else
                { dp.Command.Parameters.AddWithValue("@AreaFrom", DBNull.Value); }
                if (AreaTo > 0)
                { dp.Command.Parameters.AddWithValue("@AreaTo", AreaTo); }
                else
                { dp.Command.Parameters.AddWithValue("@AreaTo", DBNull.Value); }
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Cellar>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
