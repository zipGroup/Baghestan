﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Collections;
using System.Data;

namespace DAL
{
    public static class DistrictDAL
    {
        private static int FetchLevel { get; set; }


        public static Common.Entity.District GetRowByLevel(int DistrictId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(DistrictId);
        }


        private static Common.Entity.District GetRow(int DistrictId)
        {
            var result = new Common.Entity.District();

            string strCacheKey = "District_ById" + DistrictId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "District_ById";
                    dp.Command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.District>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.District>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.District>(strCacheKey);
            }
            return result.DeepCopy();
        }

        
        public static Common.Entity.District Insert(Common.Entity.District district)
        {
            var result = new Common.Entity.District();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "District_Insert";
                dp.Command.Parameters.AddWithValue("@CurrentStatus", district.CurrentStatus);
                dp.Command.Parameters.AddWithValue("@Latitude", district.Latitude);
                dp.Command.Parameters.AddWithValue("@Longitude", district.Longitude);
                dp.Command.Parameters.AddWithValue("@IsBlock", district.IsBlock);
                dp.Command.Parameters.AddWithValue("@History", district.History);
                dp.Command.Parameters.AddWithValue("@Name", district.Name);
                dp.Command.Parameters.AddWithValue("@SSNChief", district.Person.SSN);
                dp.Command.Parameters.AddWithValue("@OwnerId", district.Owner.OwnerId);
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.District>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.District Update(Common.Entity.District district)
        {
            var result = new Common.Entity.District();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "District_Update";
                dp.Command.Parameters.AddWithValue("@DistrictId",district.DistrictId);
                dp.Command.Parameters.AddWithValue("@CurrentStatus",district.CurrentStatus);
                dp.Command.Parameters.AddWithValue("@Latitude",district.Latitude);
                dp.Command.Parameters.AddWithValue("@Longitude",district.Longitude);
                dp.Command.Parameters.AddWithValue("@IsBlock",district.IsBlock);
                dp.Command.Parameters.AddWithValue("@History",district.History);
                dp.Command.Parameters.AddWithValue("@Name",district.Name);
                dp.Command.Parameters.AddWithValue("@SSNChief",district.Person.SSN);
                dp.Command.Parameters.AddWithValue("@OwnerId",district.Owner.OwnerId);
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.District>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int districtId)
        {
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "District_Delete";
                dp.Command.Parameters.AddWithValue("@DistrictId", districtId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {

            ArrayList result = new ArrayList();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "District_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.District>(reader, Utility.RowFetchLevel));
                }
            }
            return result;
        }

        public static ArrayList GetInfo(String CurrentStatus,double LatitudeFrom,double LatitudeTo
            ,double LongitudeFrom,double LogitudeTo, bool IsBlock , String Name,String History, String SSNChief , int OwnerId)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "District_Info";
                dp.Connection.Open();
                if (CurrentStatus != null || !CurrentStatus.Equals("") )
                { dp.Command.Parameters.AddWithValue("@CurrentStatus", CurrentStatus); }
                if (LatitudeFrom > 0) { dp.Command.Parameters.AddWithValue("@LatitudeFrom", LatitudeFrom); }
                if (LatitudeTo > 0) { dp.Command.Parameters.AddWithValue("@LatitudeTo", LatitudeTo); }
                if (LongitudeFrom > 0) { dp.Command.Parameters.AddWithValue("@LongitudeFrom", LongitudeFrom); }
                if (LogitudeTo > 0) { dp.Command.Parameters.AddWithValue("@LogitudeTo", LogitudeTo); }
                dp.Command.Parameters.AddWithValue("@IsBlock", IsBlock);
                if (Name != null || !Name.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Name", Name); }
                if (History != null || !History.Equals(""))
                { dp.Command.Parameters.AddWithValue("@History", History); }
                if (SSNChief != null || !SSNChief.Equals(""))
                { dp.Command.Parameters.AddWithValue("@SSNChief", SSNChief); }
                if (OwnerId > 0) { dp.Command.Parameters.AddWithValue("@OwnerId", OwnerId); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.District>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
 