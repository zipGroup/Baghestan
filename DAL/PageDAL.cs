﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;

namespace DAL
{
    public static class PageDAL
    {

        #region Properties

        private static int FetchLevel { get; set; }

        #endregion /Properties

        #region Constructors

        static PageDAL()
        {
            FetchLevel = 1;
        }

        #endregion /Constructors

        #region Methods

        private static Common.Entity.Page GetRow(int pageId)
        {
            var result = new Common.Entity.Page();

            string strCacheKey = "Page_ById" + pageId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Page_ById";
                    dp.Command.Parameters.AddWithValue("@PageId", pageId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Page>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Page>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Page>(strCacheKey);
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Page GetRowByLevel(int PageId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(PageId);
        }

        public static int GetCount()
        {
            var result = 0;
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Page_Count";
                dp.Connection.Open();
                result = int.Parse(dp.Command.ExecuteScalar().ToString());
            }
            return result;
        }
        
        public static short GetPageId(string strPageName)
        {
            Int16 result = 0;
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "GetPageId";
                dp.Command.Parameters.AddWithValue("@PageName", strPageName);
                dp.Connection.Open();
                result = Int16.Parse(dp.Command.ExecuteScalar().ToString());
            }
            return result;
        }

        #endregion /Methods


    }
}
