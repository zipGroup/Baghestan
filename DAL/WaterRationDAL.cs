﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Common;
using System.Collections;

namespace DAL
{
    public static class WaterRationDAL
    {

        private static int FetchLevel { get; set; }

        public static Common.Entity.WaterRation GetRowByLevel(int WaterRationId, int fetchLevel)
        {

            FetchLevel = fetchLevel;
            return GetRow(WaterRationId);
        }

        private static Common.Entity.WaterRation GetRow(int WaterRationId)
        {
            var result = new Common.Entity.WaterRation();

            string strCacheKey = "WaterRation_ById" + WaterRationId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "WaterRation_ById";
                    dp.Command.Parameters.AddWithValue("@WaterRationId", WaterRationId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.WaterRation>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.WaterRation>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.WaterRation>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.WaterRation Insert(Common.Entity.WaterRation waterRation)
        {
            var result = new Common.Entity.WaterRation();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "WaterRation_Insert";
                dp.Command.Parameters.AddWithValue("@Time", waterRation.Time);
                dp.Command.Parameters.AddWithValue("@TimeFrom", waterRation.TimeFrom);
                dp.Command.Parameters.AddWithValue("@Date", waterRation.Date);
                dp.Command.Parameters.AddWithValue("@ScrollId", waterRation.Scroll.ScrollId);
                dp.Command.Parameters.AddWithValue("@GardenId", waterRation.Garden.GardenId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.WaterRation>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.WaterRation Update(Common.Entity.WaterRation waterRation)
        {
            var result = new Common.Entity.WaterRation();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "WaterRation_Update";
                dp.Command.Parameters.AddWithValue("@WaterRationId", waterRation.WaterRationId);
                dp.Command.Parameters.AddWithValue("@Time", waterRation.Time);
                dp.Command.Parameters.AddWithValue("@TimeFrom", waterRation.TimeFrom);
                dp.Command.Parameters.AddWithValue("@Date", waterRation.Date);
                dp.Command.Parameters.AddWithValue("@ScrollId", waterRation.Scroll.ScrollId);
                dp.Command.Parameters.AddWithValue("@GardenId", waterRation.Garden.GardenId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.WaterRation>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int WaterRationId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "WaterRation_Delete";
                dp.Command.Parameters.AddWithValue("@WaterRationId", WaterRationId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "WaterRation_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.WaterRation>(reader, FetchLevel));
                }
            }
            return result;

        }



        public static ArrayList GetInfo(DateTime DateFrom,DateTime DateTo,String TimeFromPrimary ,
        String TimeFromSecondary ,DateTime TimeFrom ,DateTime TimeTo,int GardenId,int ScrollId)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "WaterRation_Info";
                dp.Connection.Open();
                if (DateFrom != null) { dp.Command.Parameters.AddWithValue("@DateFrom", DateFrom); }
                if (TimeTo != null) { dp.Command.Parameters.AddWithValue("@DateFrom", DateFrom); }
                if (TimeFromPrimary != null || !TimeFromPrimary.Equals(""))
                { dp.Command.Parameters.AddWithValue("@TimeFromPrimary", TimeFromPrimary); }
                if (TimeFromSecondary != null || !TimeFromSecondary.Equals(""))
                { dp.Command.Parameters.AddWithValue("@TimeFromSecondary", TimeFromSecondary); }
                if (TimeFrom != null) { dp.Command.Parameters.AddWithValue("@TimeFrom", TimeFrom); }
                if (TimeTo != null) { dp.Command.Parameters.AddWithValue("@TimeTo", TimeTo); }
                if (ScrollId > 0)
                { dp.Command.Parameters.AddWithValue("@ScrollId", ScrollId); }
                if (GardenId > 0)
                { dp.Command.Parameters.AddWithValue("@GardenId", GardenId); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.WaterRation>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
