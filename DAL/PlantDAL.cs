﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{
    public static class PlantDAL
    {

        #region Properties


        private static int FetchLevel { get; set; }

        #endregion /Properties



        public static Common.Entity.Plant GetRowByLevel(int PlantId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(PlantId);
        }

        private static Common.Entity.Plant GetRow(int PlantId)
        {
            var result = new Common.Entity.Plant();

            string strCacheKey = "Plant_ById" + PlantId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Plant_ById";
                    dp.Command.Parameters.AddWithValue("@PlantId", PlantId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Plant>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Plant>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Plant>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.Plant Insert(Common.Entity.Plant plant)
        {
         
            var result = new Common.Entity.Plant();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Plant_Insert";
                dp.Command.Parameters.AddWithValue("@GardenId", plant.Garden.GardenId);
                dp.Command.Parameters.AddWithValue("@ProductId", plant.Product.ProductId);
                dp.Command.Parameters.AddWithValue("@GeneticInformation", plant.GeneticInformation);
                dp.Command.Parameters.AddWithValue("@Status", plant.GeneticInformation);
                dp.Command.Parameters.AddWithValue("@Name", plant.Name);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Plant>(reader, FetchLevel);
                }
            }
            return result;
        }

        public static Common.Entity.Plant Update(Common.Entity.Plant plant)
        {
            var result = new Common.Entity.Plant();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Plant_Update";
                dp.Command.Parameters.AddWithValue("@GardenId", plant.Garden.GardenId);
                dp.Command.Parameters.AddWithValue("@ProductId", plant.Product.ProductId);
                dp.Command.Parameters.AddWithValue("@GeneticInformation", plant.GeneticInformation);
                dp.Command.Parameters.AddWithValue("@Status", plant.GeneticInformation);
                dp.Command.Parameters.AddWithValue("@Name", plant.Name);
                dp.Connection.Open();

                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Plant>(reader, FetchLevel);
                }
            }
                return result.DeepCopy();
        }

        public static void Delete(int PlantId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Plant_Delete";
                dp.Command.Parameters.AddWithValue("@PlantId", PlantId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetInfo(int ProductId, String GeneticInformation,String Name,String Status,int GardenId)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Plant_Info";
                dp.Connection.Open();
                if (ProductId > 0)
                { dp.Command.Parameters.AddWithValue("@ProductId", ProductId); }
                if (GeneticInformation != null || !GeneticInformation.Equals(""))
                { dp.Command.Parameters.AddWithValue("@GeneticInformation", GeneticInformation); }
                if (Name != null || !Name.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Name", Name); }
                if (Status != null || !Status.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Status", Status); }
                if (GardenId > 0)
                { dp.Command.Parameters.AddWithValue("@GardenId", GardenId); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Plant>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}