﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{
    class WorkInDAL
    {
        #region Properties

        private static int FetchLevel { get; set; }

        #endregion /Properties
        public static Common.Entity.WorkIn GetRowByLevel(int WorkInId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return null;
        }

        private static Common.Entity.WorkIn GetRow(int GardenId,String SSNFarmer)
        {
            var result = new Common.Entity.WorkIn();

            string strCacheKey = "Work_In_ById" + GardenId.ToString()+SSNFarmer.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Work_In_ById";
                    dp.Command.Parameters.AddWithValue("@GardenId", GardenId);
                    dp.Command.Parameters.AddWithValue("@SSNFarmer", SSNFarmer);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.WorkIn>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.WorkIn>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.WorkIn>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.WorkIn Insert(Common.Entity.WorkIn workIn)
        {

            var result = new Common.Entity.WorkIn();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Work_In_Insert";
                dp.Command.Parameters.AddWithValue("@GardenId", workIn.Garden.GardenId);
                dp.Command.Parameters.AddWithValue("@SSNFarmer", workIn.SSNFarmer);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.WorkIn>(reader, FetchLevel);
                }
            }
            return result;
        }

        public static Common.Entity.Plant Update(Common.Entity.Plant plant)
        {
            var result = new Common.Entity.Plant();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Plant_Update";
                dp.Command.Parameters.AddWithValue("@GardenId", plant.Garden.GardenId);
                dp.Command.Parameters.AddWithValue("@ProductId", plant.Product.ProductId);
                dp.Command.Parameters.AddWithValue("@GeneticInformation", plant.GeneticInformation);
                dp.Command.Parameters.AddWithValue("@Status", plant.GeneticInformation);
                dp.Command.Parameters.AddWithValue("@Name", plant.Name);
                dp.Connection.Open();

                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Plant>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int PlantId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Plant_Delete";
                dp.Command.Parameters.AddWithValue("@PlantId", PlantId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {

            ArrayList result = new ArrayList();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Plant_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Plant>(reader, FetchLevel));
                }
            }
            return result;
        }
    }
}
