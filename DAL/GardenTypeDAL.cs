﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Common;
using System.Collections;


namespace DAL
{
    public static class GardenTypeDAL
    {
        private static int FetchLevel { get; set; }




        public static Common.Entity.GardenType GetRowByLevel(int GardenTypeId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(GardenTypeId);
        }

        private static Common.Entity.GardenType GetRow(int GardenTypeId)
        {
            var result = new Common.Entity.GardenType();

            string strCacheKey = "GardenType_ById" + GardenTypeId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "GardenType_ById";
                    dp.Command.Parameters.AddWithValue("@GardenTypeId", GardenTypeId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.GardenType>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.GardenType>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.GardenType>(strCacheKey);
            }
            return result.DeepCopy();
        }


         public static Common.Entity.GardenType Insert(Common.Entity.GardenType gardenType)
        {
            var result = new Common.Entity.GardenType();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "GardenType_Insert"; 
                dp.Command.Parameters.AddWithValue("@Name",gardenType.Name);
                dp.Command.Parameters.AddWithValue("@HaveProduct", gardenType.HaveProduct);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.GardenType>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.GardenType Update(Common.Entity.GardenType gardenType)
        {
            var result = new Common.Entity.GardenType();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Bush_Update";
                dp.Command.Parameters.AddWithValue("@GardenTypeId", gardenType.GardenTypeId);
                dp.Command.Parameters.AddWithValue("@Name", gardenType.Name);
                dp.Command.Parameters.AddWithValue("@HaveProduct", gardenType.HaveProduct);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.GardenType>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int GardenTypeId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "GardenType_Delete";
                dp.Command.Parameters.AddWithValue("@GardenTypeId", GardenTypeId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {

            ArrayList result = new ArrayList();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "GardenType_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.GardenType>(reader, Utility.RowFetchLevel));
                }
            }
            return result;
        }

        public static ArrayList GetInfo(String Name, bool HaveProduct)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "GardenType_Info";
                dp.Connection.Open();
                if (Name != null || !Name.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Name", Name); }
                dp.Command.Parameters.AddWithValue("@HaveProduct", HaveProduct);
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.GardenType>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
