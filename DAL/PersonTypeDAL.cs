﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{
    public static class PersonTypeDAL
    {
        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties



        public static Common.Entity.PersonType GetRowByLevel(int PersonTypeId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(PersonTypeId);
        }


        private static Common.Entity.PersonType GetRow(int PersonTypeId)
        {
            var result = new Common.Entity.PersonType();

            string strCacheKey = "PersonType_ById" + PersonTypeId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "PersonType_ById";
                    dp.Command.Parameters.AddWithValue("@PersonTypeId", PersonTypeId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.PersonType>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.PersonType>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.PersonType>(strCacheKey);
            }

            return result.DeepCopy();
        }
        public static Common.Entity.PersonType Insert(Common.Entity.PersonType personType)
        {
            var result = new Common.Entity.PersonType();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "PersonType_Insert";
                
                dp.Connection.Open();
                dp.Command.Parameters.AddWithValue("@personType", personType);
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.PersonType>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }
        public static Common.Entity.PersonType Update(Common.Entity.PersonType personType)
        {
            var result = new Common.Entity.PersonType();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "PersonType_Update";
                dp.Command.Parameters.AddWithValue("@PersonTypeId", personType.PersonTypeId);

                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.PersonType>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int PersonTypeId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "PersonType_Delete";
                dp.Command.Parameters.AddWithValue("@PersonTypeId", PersonTypeId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }
        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "PersonType_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Person>(reader, Utility.RowFetchLevel));
                }
            }
            return result;
        }

        public static ArrayList GetInfo(String Name)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "PersonType_Info";
                dp.Connection.Open();
                if (Name != null || !Name.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Name", Name); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.PersonType>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
