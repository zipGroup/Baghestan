﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;
using Common.Entity;


namespace DAL
{

    public static class ExpressionDAL
    {
        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties


        public static Common.Entity.Expression GetRowByLevel(int ExpressionId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(ExpressionId);
        }


        private static Common.Entity.Expression GetRow(int ExpressionId)
        {
            var result = new Common.Entity.Expression();

            string strCacheKey = "Expression_ById" + ExpressionId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Scroll_ById";
                    dp.Command.Parameters.AddWithValue("@ExpressionId", ExpressionId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Expression>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Expression>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Expression>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.Expression Insert(Common.Entity.Expression expression)
        {
            var result = new Common.Entity.Expression();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Scroll_Insert";

                dp.Command.Parameters.AddWithValue("@Title", expression.Title);
                dp.Command.Parameters.AddWithValue("@Description", expression.Description);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Expression>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Expression Update(Common.Entity.Expression expression)
        {
            var result = new Common.Entity.Expression();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Scroll_Update";
                dp.Command.Parameters.AddWithValue("@ExpressionId", expression.ExpressionId);
                dp.Command.Parameters.AddWithValue("@Title", expression.Title);
                dp.Command.Parameters.AddWithValue("@Description", expression.Description);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Expression>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int ExpressionId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Expression_Delete";
                dp.Command.Parameters.AddWithValue("@ExpressionId", ExpressionId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Expression_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Expression>(reader, FetchLevel));
                }
            }
            return result;

        }

        public static ArrayList GetInfo(String Title , String Description)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Expression_Info";
                dp.Connection.Open();
                if (Title != null || !Title.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Title", Title); }
                if (Description != null || !Description.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Description", Description); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Bush>(reader, FetchLevel));
                }

            }
            return result;
        }

    }
}
