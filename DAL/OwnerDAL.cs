﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{
    class OwnerDAL
    {
        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties


        public static Common.Entity.Owner GetRowByLevel(int OwnerId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(OwnerId);
        }

        private static Common.Entity.Owner GetRow(int OwnerId)
        {
            var result = new Common.Entity.Owner();

            string strCacheKey = "Owner_ById" + OwnerId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Owner_ById";
                    dp.Command.Parameters.AddWithValue("@OwnerId", OwnerId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Owner>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Owner>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Owner>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.Owner Insert(Common.Entity.Owner owner)
        {
            var result = new Common.Entity.Owner();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Owner_Insert";
                dp.Command.Parameters.AddWithValue("@MapId", owner.Map.MapId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Owner>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }
        public static Common.Entity.Owner Update(Common.Entity.Owner owner)
        {
            var result = new Common.Entity.Owner();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Owner_Update";
                dp.Command.Parameters.AddWithValue("@OwnerId", owner.OwnerId);
                dp.Command.Parameters.AddWithValue("@MapId", owner.Map.MapId);

                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Owner>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int ownerId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Owner_Delete";
                dp.Command.Parameters.AddWithValue("@OwnerId", ownerId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }
        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Owner_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Owner>(reader, Utility.RowFetchLevel));
                }
            }
            return result;
        }
    }
}
