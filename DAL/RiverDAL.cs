﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;


namespace DAL
{
    public static class RiverDAL
    {
        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties



        public static Common.Entity.River GetRowByLevel(int RiverId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(RiverId);
        }


        private static Common.Entity.River GetRow(int RiverId)
        {
            var result = new Common.Entity.River();

            string strCacheKey = "River_ById" + RiverId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "River_ById";
                    dp.Command.Parameters.AddWithValue("@RiverId", RiverId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.River>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.River>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.River>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.River Insert(Common.Entity.River river)
        {
            var result = new Common.Entity.River();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "River_Insert";
                dp.Command.Parameters.AddWithValue("@LatitudeStart",river.LatitudeStart);
                dp.Command.Parameters.AddWithValue("@LongitudeStart", river.LongitudeStart);
                dp.Command.Parameters.AddWithValue("@LatitudeEnd", river.LatitudeEnd);
                dp.Command.Parameters.AddWithValue("@LongitudeEnd", river.LongitudeEnd);
                dp.Command.Parameters.AddWithValue("@OwnerId", river.Owner.OwnerId);
                dp.Command.Parameters.AddWithValue("@ScrollId", river.Scroll.ScrollId);
                dp.Command.Parameters.AddWithValue("@DistrictId", river.District.DistrictId);
                dp.Command.Parameters.AddWithValue("@Name", river.Name);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.River>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.River Update(Common.Entity.River river)
        {
            var result = new Common.Entity.River();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "River_Update";
                dp.Command.Parameters.AddWithValue("@LatitudeStart", river.LatitudeStart);
                dp.Command.Parameters.AddWithValue("@LongitudeStart", river.LongitudeStart);
                dp.Command.Parameters.AddWithValue("@LatitudeEnd", river.LatitudeEnd);
                dp.Command.Parameters.AddWithValue("@LongitudeEnd", river.LongitudeEnd);
                dp.Command.Parameters.AddWithValue("@OwnerId", river.Owner.OwnerId);
                dp.Command.Parameters.AddWithValue("@ScrollId", river.Scroll.ScrollId);
                dp.Command.Parameters.AddWithValue("@DistrictId", river.District.DistrictId);
                dp.Command.Parameters.AddWithValue("@Name", river.Name);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.River>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int RiverId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "River_Delete";
                dp.Command.Parameters.AddWithValue("@riverId", RiverId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "River_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.River>(reader, FetchLevel));
                }
            }
            return result;

        }

        public static ArrayList GetInfo(double LatitudeStartTo
        , double LatitudeStartFrom
        , double LongitudeStartTo
        , double LongitudeStartFrom
        , double LatitudeEndTo
        , double LatitudeEndFrom
        , double LongitudeEndTo
        , double LongitudeEndFrom
        , String Name
        , int DistrictId
        , int ScrollId
        , int OwnerId )
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "River_Info";
                dp.Connection.Open();
                if(LatitudeStartTo > 0) { dp.Command.Parameters.AddWithValue("@LatitudeStartTo", LatitudeStartTo); }
                if (LatitudeStartFrom > 0) { dp.Command.Parameters.AddWithValue("@LatitudeStartFrom", LatitudeStartFrom); }
                if (LatitudeEndFrom > 0) { dp.Command.Parameters.AddWithValue("@LatitudeEndFrom", LatitudeEndFrom); }
                if (LatitudeEndTo > 0) { dp.Command.Parameters.AddWithValue("@LatitudeEndTo", LatitudeEndTo); }
                if (LongitudeStartFrom > 0) { dp.Command.Parameters.AddWithValue("@LongitudeStartFrom ", LongitudeStartFrom); }
                if (LongitudeStartTo > 0) { dp.Command.Parameters.AddWithValue("@LongitudeStartTo", LongitudeStartTo); }
                if (LongitudeEndFrom > 0) { dp.Command.Parameters.AddWithValue("@LongitudeEndFrom", LongitudeEndFrom); }
                if (LongitudeEndTo > 0) { dp.Command.Parameters.AddWithValue("@LongitudeEndTo", LongitudeEndTo); }
                if (Name != null || !Name.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Name", Name); }
                if (DistrictId > 0)
                { dp.Command.Parameters.AddWithValue("@DistrictId ", DistrictId); }
                if (OwnerId > 0)
                { dp.Command.Parameters.AddWithValue("@OwnerId", OwnerId); }
                if (ScrollId > 0)
                { dp.Command.Parameters.AddWithValue("@ScrollId", ScrollId); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.River>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
