﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Common;
using System.Collections;

namespace DAL
{
    public static class GardenDAL
    {


        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties



        public static Common.Entity.Garden GetRowByLevel(int GardenId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(GardenId);
        }

        private static Common.Entity.Garden GetRow(int GardenId)
        {
            var result = new Common.Entity.Garden();

            string strCacheKey = "Garden_ById" + GardenId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Garden_ById";
                    dp.Command.Parameters.AddWithValue("@GardenId", GardenId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Garden>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Garden>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Garden>(strCacheKey);
            }
            return result.DeepCopy();
        }




        public static Common.Entity.Garden Insert(Common.Entity.Garden garden)
        {

            var result = new Common.Entity.Garden();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Garden_Insert";
                dp.Command.Parameters.AddWithValue("@Name", garden.Name);
                dp.Command.Parameters.AddWithValue("@Age", garden.Age);
                dp.Command.Parameters.AddWithValue("@Area", garden.Area);
                dp.Command.Parameters.AddWithValue("@Latitude", garden.Latitude);
                dp.Command.Parameters.AddWithValue("@Longitude", garden.Longitude);
                if (garden.GardenType != null)
                {
                    dp.Command.Parameters.AddWithValue("@GardenTypeId", garden.GardenType.GardenTypeId);
                }
                else
                {
                    dp.Command.Parameters.AddWithValue("@GardenTypeId", DBNull.Value);
                }
                if (garden.Cellar != null)
                {
                    dp.Command.Parameters.AddWithValue("@CellarId", garden.Cellar.CellarId);
                }
                else
                {
                    dp.Command.Parameters.AddWithValue("@CellarId", DBNull.Value);
                }
                if (garden.Gardener != null)
                {
                    dp.Command.Parameters.AddWithValue("@SSNGardener", garden.Gardener.SSN);
                }
                else
                {
                    dp.Command.Parameters.AddWithValue("@SSNGardener", DBNull.Value);
                }
                if (garden.District != null)
                {
                    dp.Command.Parameters.AddWithValue("@DistrictId", garden.District.DistrictId);
                }
                else
                {
                    dp.Command.Parameters.AddWithValue("@DistrictId", DBNull.Value);
                }
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Garden>(reader, Utility.RowFetchLevel);
                }
                dp.Connection.Close();
            }
            return result;
        }

        public static Common.Entity.Garden Update(Common.Entity.Garden garden)
        {

            var result = new Common.Entity.Garden();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Garden_Update";
                dp.Command.Parameters.AddWithValue("@GardenId", garden.GardenId);
                dp.Command.Parameters.AddWithValue("@Name", garden.Name);
                dp.Command.Parameters.AddWithValue("@Age", garden.Age);
                dp.Command.Parameters.AddWithValue("@Area", garden.Area);
                dp.Command.Parameters.AddWithValue("@Latitude", garden.Latitude);
                dp.Command.Parameters.AddWithValue("@Longitude", garden.Longitude);
                dp.Command.Parameters.AddWithValue("@GardenTypeId", garden.GardenType.GardenTypeId);
                dp.Command.Parameters.AddWithValue("@CellarId", garden.Cellar.CellarId);
                dp.Command.Parameters.AddWithValue("@SSNGardener", garden.Gardener.SSN);
                dp.Command.Parameters.AddWithValue("@DistrictId", garden.District.DistrictId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Garden>(reader, Utility.RowFetchLevel);
                }
                dp.Connection.Close();
            }
            return result;
        }

        public static void Delete(Common.Entity.Garden garden)
        {

            var result = new Common.Entity.Garden();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Garden_Delete";
                dp.Command.Parameters.AddWithValue("@GardenId", garden.GardenId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
                dp.Connection.Close();
            }
            return;
        }

        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Garden_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Garden>(reader, Utility.RowFetchLevel));
                }
            }
            return result;
        }


        public static ArrayList GetInfo(String Name, int AreaFrom,
            int AreaTo, double LongitudeFrom, double LongitudeTo, double LatitudeFrom, double LatitudeTo
            , int AgeFrom, int AgeTo)
        {
            ArrayList result = new ArrayList();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Garden_Info";
                if (Name != null && !Name.Equals(""))
                {dp.Command.Parameters.AddWithValue("@NameGarden", Name);}
                else
                {dp.Command.Parameters.AddWithValue("@NameGarden", DBNull.Value);}
                if (AreaFrom > 0)
                { dp.Command.Parameters.AddWithValue("@AreaFrom", AreaFrom); }
                else
                {dp.Command.Parameters.AddWithValue("@AreaFrom", DBNull.Value);}
                if (AreaTo > 0)
                { dp.Command.Parameters.AddWithValue("@AreaTo", AreaTo); }
                else
                {dp.Command.Parameters.AddWithValue("@AreaTo", DBNull.Value);}
                if (LongitudeFrom > 0)
                { dp.Command.Parameters.AddWithValue("@LongitudeFrom", LongitudeFrom); }
                else
                {dp.Command.Parameters.AddWithValue("@LongitudeFrom", DBNull.Value);}
                if (LongitudeFrom > 0)
                { dp.Command.Parameters.AddWithValue("@LongitudeTo", LongitudeTo); }
                else
                {dp.Command.Parameters.AddWithValue("@LongitudeTo", DBNull.Value);}
                if (LongitudeFrom > 0)
                { dp.Command.Parameters.AddWithValue("@LatitudeFrom", LatitudeFrom); }
                else
                {dp.Command.Parameters.AddWithValue("@LatitudeFrom", DBNull.Value);}
                if (LongitudeFrom > 0)
                { dp.Command.Parameters.AddWithValue("@LatitudeTo", LatitudeTo); }
                else
                {dp.Command.Parameters.AddWithValue("@LatitudeTo", DBNull.Value);}
                if (LongitudeFrom > 0)
                { dp.Command.Parameters.AddWithValue("@AgeFrom", AgeFrom); }
                else
                {dp.Command.Parameters.AddWithValue("@AgeFrom", DBNull.Value);}
                if (LongitudeFrom > 0)
                { dp.Command.Parameters.AddWithValue("@AgeTo", AgeTo); }
                else
                {dp.Command.Parameters.AddWithValue("@AgeTo", DBNull.Value);}
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Garden>(reader, 2));
                }
            }
            return result;
        }

    }
}
