﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public static class ReportDAL
    {
        public static ArrayList getWaterRations()
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Rpt_RiverScrollWaterRationGarden_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetReportValue<Common.Report.WaterRation>(reader));
                }
            }
            return result;
        }

        public static ArrayList getWaterRationsSearch(int areaFrom, int areaTo, int gardenId)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Rpt_RiverScrollWaterRationGarden_Search";
                if(areaFrom > 0)
                {
                    dp.Command.Parameters.AddWithValue("@fromArea", areaFrom);
                }
                if (areaTo > 0)
                {
                    dp.Command.Parameters.AddWithValue("@toArea", areaTo);
                }
                if (gardenId > 0)
                {
                    dp.Command.Parameters.AddWithValue("@gardenId", gardenId);
                }
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetReportValue<Common.Report.WaterRation>(reader));
                }
            }
            return result;
        }



        public static ArrayList getTreeGardenList()
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "rpt_GardenTree_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetReportValue<Common.Report.rpt_GardenTree>(reader));
                }
            }
            return result;
        }

        public static ArrayList getGardenTreeSearch(int areaFrom, int areaTo, int gardenId)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "rpt_GardenTree_Search";
                if (areaFrom > 0)
                {
                    dp.Command.Parameters.AddWithValue("@fromArea", areaFrom);
                }
                if (areaTo > 0)
                {
                    dp.Command.Parameters.AddWithValue("@toArea", areaTo);
                }
                if (gardenId > 0)
                {
                    dp.Command.Parameters.AddWithValue("@gardenId", gardenId);
                }
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetReportValue<Common.Report.WaterRation>(reader));
                }
            }
            return result;
        }
    }
}
