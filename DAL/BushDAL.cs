﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{
    public static class BushDAL 
    {
        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties


        public static Common.Entity.Bush GetRowByLevel(int BushId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(BushId);
        }

        private static Common.Entity.Bush GetRow(int BushId)
        {
            var result = new Common.Entity.Bush();

            string strCacheKey = "Bush_ById" + BushId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Bush_ById";
                    dp.Command.Parameters.AddWithValue("@BushId", BushId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Bush>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Bush>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Bush>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.Bush Insert(Common.Entity.Bush bush)
        {
            var result = new Common.Entity.Bush();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Bush_Insert"; 
                dp.Command.Parameters.AddWithValue("@TermViability", bush.TermViability);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Bush>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Bush Update(Common.Entity.Bush bush)
        {
            var result = new Common.Entity.Bush();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Bush_Update";
                dp.Command.Parameters.AddWithValue("@BushId", bush.BushId);
                dp.Command.Parameters.AddWithValue("@TermViability", bush.TermViability);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Bush>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int BushId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Bush_Delete";
                dp.Command.Parameters.AddWithValue("@BushId", BushId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {

            ArrayList result = new ArrayList();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Bush_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Bush>(reader, FetchLevel));
                }
            }
            return result;
        }

        public static ArrayList GetInfo( int PlantId , int TermViabilityFrom , int TermViabilityTo)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Bush_Info";
                dp.Connection.Open();
                if (PlantId > 0) { dp.Command.Parameters.AddWithValue("@PlantId",PlantId); }
                if (TermViabilityFrom > 0) { dp.Command.Parameters.AddWithValue("@TermViabilityFrom ", TermViabilityFrom); }
                if (TermViabilityTo > 0) { dp.Command.Parameters.AddWithValue("@TermViabilityTo", TermViabilityTo); }
                IDataReader reader = dp.Command.ExecuteReader();
                while( reader.Read())
                    {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Bush>(reader, FetchLevel));
                }
            
            }
            return result;
        }




    }
}
