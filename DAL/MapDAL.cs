﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{
    public static class MapDAL
    {
        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties




        public static Common.Entity.Map GetRowByLevel(int MapId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(MapId);
        }

        private static Common.Entity.Map GetRow(int MapId)
        {
            var result = new Common.Entity.Map();

            string strCacheKey = "Map_ById" + MapId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Map_ById";
                    dp.Command.Parameters.AddWithValue("@MapId", MapId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Map>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Map>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Map>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.Map Insert(Common.Entity.Map map)
        {
            var result = new Common.Entity.Map();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Map_Insert";
                dp.Command.Parameters.AddWithValue("@Name", map.Name);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Map>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Map Update(Common.Entity.Map map)
        {
            var result = new Common.Entity.Map();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Map_Update";
                dp.Command.Parameters.AddWithValue("@MapId", map.MapId);
                dp.Command.Parameters.AddWithValue("@Name", map.Name);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Map>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int MapId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Map_Delete";
                dp.Command.Parameters.AddWithValue("@MapId", MapId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {

            ArrayList result = new ArrayList();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Map_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Map>(reader, FetchLevel));
                }
            }
            return result;
        }

        public static ArrayList GetInfo(String Name)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Map_Info";
                dp.Connection.Open();
                if (Name != null || !Name.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Name", Name); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Map>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
