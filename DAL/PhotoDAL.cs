﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{
    public static class PhotoDAL
    {
        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties



        public static Common.Entity.Photo GetRowByLevel(int PhotoId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(PhotoId);
        }


        private static Common.Entity.Photo GetRow(int PhotoId)
        {
            var result = new Common.Entity.Photo();

            string strCacheKey = "Photo_ById" + PhotoId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Photo_ById";
                    dp.Command.Parameters.AddWithValue("@PhotoId", PhotoId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Photo>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Photo>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Photo>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.Photo Insert(Common.Entity.Photo photo)
        {
            var result = new Common.Entity.Photo();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Photo_Insert";
                dp.Command.Parameters.AddWithValue("@Name", photo.Name);
                dp.Command.Parameters.AddWithValue("@Image", photo.Image);
                dp.Command.Parameters.AddWithValue("@GardenId", photo.Garden.GardenId);
                //dp.Command.Parameters.AddWithValue("@Name", photo.);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Photo>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Photo Update(Common.Entity.Photo photo)
        {
            var result = new Common.Entity.Photo();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Photo_Update";
                dp.Command.Parameters.AddWithValue("@PhotoId", photo.PhotoId);
                dp.Command.Parameters.AddWithValue("@Name", photo.Name);
                dp.Command.Parameters.AddWithValue("@GardenId", photo.Garden.GardenId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Photo>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int PhotoId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Photo_Delete";
                dp.Command.Parameters.AddWithValue("@PhotoId", PhotoId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {

            ArrayList result = new ArrayList();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Photo_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Photo>(reader, FetchLevel));
                }
            }
            return result;
        }


        public static ArrayList GetInfo(int GardenId, String Name)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Photo_Info";
                dp.Connection.Open();
                if (GardenId>0)
                { dp.Command.Parameters.AddWithValue("@GardenId", GardenId); }
                if (Name != null || !Name.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Name", Name); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Photo>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
