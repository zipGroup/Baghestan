﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{
    public class ProductDAL
    {

        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties


        public static Common.Entity.Product GetRowByLevel(int ProductId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(ProductId);
        }



        private static Common.Entity.Product GetRow(int ProductId)
        {
            var result = new Common.Entity.Product();

            string strCacheKey = "Product_ById" + ProductId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Product_ById";
                    dp.Command.Parameters.AddWithValue("@ProductId", ProductId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Product>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Product>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Product>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.Product Insert(Common.Entity.Product product)
        {
            var result = new Common.Entity.Product();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Product_Insert";
                dp.Command.Parameters.AddWithValue("@Name", product.Name);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Product>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Product Update(Common.Entity.Product product)
        {
            var result = new Common.Entity.Product();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Product_Update";
                dp.Command.Parameters.AddWithValue("@ProductId", product.ProductId);
                dp.Command.Parameters.AddWithValue("@Name", product.Name);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Product>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int ProductId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Product_Delete";
                dp.Command.Parameters.AddWithValue("@ProductId", ProductId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Product_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Product>(reader, FetchLevel));
                }
            }
            return result;

        }


        public static ArrayList GetInfo(String Name)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Product_Info";
                dp.Connection.Open();
                if (Name != null || !Name.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Name", Name); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Product>(reader, FetchLevel));
                }

            }
            return result;
        }

    }
}
