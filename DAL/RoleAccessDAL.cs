﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;

namespace DAL
{
    public static class RoleAccessDAL
    {
        public static bool HasPageAccess(int IdUser, string PageName)
        {

            var result = false;
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "UserAccesstoPage_List";
                dp.Command.Parameters.AddWithValue("@IdUser", IdUser);
                dp.Command.Parameters.AddWithValue("@PageName", PageName);
                dp.Connection.Open();
                result = Convert.ToBoolean(dp.Command.ExecuteScalar().ToString());
            }
            return result;
        }
    }
}