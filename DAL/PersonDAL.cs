﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Collections;
using System.Data;

namespace DAL
{
    public static class PersonDAL
    {
        private static int FetchLevel { get; set; }

        public static Common.Entity.Person GetRowByLevel(String SSN, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(SSN);
        }


        private static Common.Entity.Person GetRow(String SSN)
        {
            var result = new Common.Entity.Person();

            string strCacheKey = "Person_ById" + SSN.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Person_ById";
                    dp.Command.Parameters.AddWithValue("@SSN", SSN);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Person>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Person>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Person>(strCacheKey);
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Person Insert(Common.Entity.Person person)
        {
            var result = new Common.Entity.Person();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Person_Insert";
                dp.Command.Parameters.AddWithValue("@SSN", person.SSN);
                dp.Command.Parameters.AddWithValue("@BirthDate", person.BirthDate);
                dp.Command.Parameters.AddWithValue("@FirstName", person.FirstName);
                dp.Command.Parameters.AddWithValue("@LastName", person.LastName);
                dp.Command.Parameters.AddWithValue("@FatherName", person.FatherName);
                dp.Command.Parameters.AddWithValue("@PersonTypeId", person.PersonType.PersonTypeId);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Person>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }
        public static Common.Entity.Person Update(Common.Entity.Person person)
        {
            var result = new Common.Entity.Person();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Person_Update";
                dp.Command.Parameters.AddWithValue("@SSN", person.SSN);
                dp.Command.Parameters.AddWithValue("@BirthDate", person.BirthDate);
                dp.Command.Parameters.AddWithValue("@FirstName", person.FirstName);
                dp.Command.Parameters.AddWithValue("@LastName",person.LastName);
                dp.Command.Parameters.AddWithValue("@FatherName",person.FatherName);
                dp.Command.Parameters.AddWithValue("@PersonTypeId", person.PersonType.PersonTypeId);

                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Person>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(String SSN)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Person_Delete";
                dp.Command.Parameters.AddWithValue("@SSN", SSN);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }
        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();
            
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Person_List";
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    while(reader.Read())
                    {
                        result.Add(DALUtility.GetEntityValue<Common.Entity.Person>(reader, Utility.RowFetchLevel));
                    }
                }
            return result;
        }

        public static ArrayList GetInfo(DateTime BirthDateFrom, DateTime BirthDateTo ,String FirstName, String LastName
            , String FatherName,int PersonTypeId)
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Person_Info";
                dp.Connection.Open();
                if (BirthDateFrom != null)
                { dp.Command.Parameters.AddWithValue("@BirthDateFrom", BirthDateFrom); }
                if (BirthDateTo != null )
                { dp.Command.Parameters.AddWithValue("@BirthDateTo", BirthDateTo); }
                if (FirstName != null || !FirstName.Equals(""))
                { dp.Command.Parameters.AddWithValue("@FirstName", FirstName); }
                if (LastName != null || !LastName.Equals(""))
                { dp.Command.Parameters.AddWithValue("@LastName", LastName); }
                if (FatherName != null || !FatherName.Equals(""))
                { dp.Command.Parameters.AddWithValue("@FatherName", FatherName); }
                if (PersonTypeId > 0) { dp.Command.Parameters.AddWithValue("@PersonTypeId", PersonTypeId ); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Person>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
