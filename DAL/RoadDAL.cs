﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common;
using System.Data.SqlClient;
using System.Collections;

namespace DAL
{
    public static class RoadDAL
    {
        #region Properties
        private static int FetchLevel { get; set; }
        #endregion /Properties



        public static Common.Entity.Road GetRowByLevel(int RoadId, int fetchLevel)
        {
            FetchLevel = fetchLevel;
            return GetRow(RoadId);
        }


        private static Common.Entity.Road GetRow(int RoadId)
        {
            var result = new Common.Entity.Road();

            string strCacheKey = "Road_ById" + RoadId.ToString();
            if (!CacheHelper.Exists(strCacheKey))
            {
                using (DataProvider dp = new DataProvider())
                {
                    dp.Command.CommandText = "Road_ById";
                    dp.Command.Parameters.AddWithValue("@RoadId", RoadId);
                    dp.Connection.Open();
                    IDataReader reader = dp.Command.ExecuteReader();
                    if (reader.Read())
                    {
                        result = DALUtility.GetEntityValue<Common.Entity.Road>(reader, FetchLevel);
                    }
                }
                if (FetchLevel == Utility.RowFetchLevel)
                {
                    CacheHelper.Add<Common.Entity.Road>(result, strCacheKey);
                }
            }
            else
            {
                result = CacheHelper.Get<Common.Entity.Road>(strCacheKey);
            }

            return result.DeepCopy();
        }

        public static Common.Entity.Road Insert(Common.Entity.Road road)
        {
            var result = new Common.Entity.Road();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Road_Insert";
                dp.Command.Parameters.AddWithValue("@Latitude", road.Latitude);
                dp.Command.Parameters.AddWithValue("@Longitude", road.Longitude);
                dp.Command.Parameters.AddWithValue("@Length", road.Length);
                dp.Command.Parameters.AddWithValue("@ConstructionDate", road.ConstructionDate);
                dp.Command.Parameters.AddWithValue("@Name", road.Name);
                dp.Connection.Open(); 
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Road>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static Common.Entity.Road Update(Common.Entity.Road road)
        {
            var result = new Common.Entity.Road();

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Road_Update";
                dp.Command.Parameters.AddWithValue("@RoadId", road.RoadId);
                dp.Command.Parameters.AddWithValue("@Latitude", road.Latitude);
                dp.Command.Parameters.AddWithValue("@Longitude", road.Longitude);
                dp.Command.Parameters.AddWithValue("@Length", road.Length);
                dp.Command.Parameters.AddWithValue("@ConstructionDate", road.ConstructionDate);
                dp.Command.Parameters.AddWithValue("@Name", road.Name);
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                if (reader.Read())
                {
                    result = DALUtility.GetEntityValue<Common.Entity.Road>(reader, FetchLevel);
                }
            }
            return result.DeepCopy();
        }

        public static void Delete(int RoadId)
        {

            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Road_Delete";
                dp.Command.Parameters.AddWithValue("@RoadId", RoadId);
                dp.Connection.Open();
                dp.Command.ExecuteNonQuery();
            }
            return;
        }

        public static ArrayList GetList()
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Road_List";
                dp.Connection.Open();
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Road>(reader, FetchLevel));
                }
            }
            return result;

        }

        public static ArrayList GetInfo(
            double LengthFrom
            , double LengthTo 
            , DateTime ConstructionDateFrom 
            , DateTime ConstructionDateTo 
            , double LatitudeFrom 
            , double LatitudeTo 
            , double LongitudeFrom
            , double LongitudeTo
            , String Name )
        {
            ArrayList result = new ArrayList();
            using (DataProvider dp = new DataProvider())
            {
                dp.Command.CommandText = "Road_Info";
                dp.Connection.Open();
                if (LengthFrom > 0)
                { dp.Command.Parameters.AddWithValue("@LengthFrom", LengthFrom); }
                if (LengthTo > 0)
                { dp.Command.Parameters.AddWithValue("@CellarId", LengthTo); }
                if (ConstructionDateFrom != null) { dp.Command.Parameters.AddWithValue("@ConstructionDateFrom", ConstructionDateFrom); }
                if (ConstructionDateTo != null) { dp.Command.Parameters.AddWithValue("@ConstructionDateTo", ConstructionDateTo); }
                if (LatitudeFrom > 0)
                { dp.Command.Parameters.AddWithValue("@LatitudeFrom", LatitudeFrom); }
                if (LatitudeTo > 0)
                { dp.Command.Parameters.AddWithValue("@LatitudeTo", LatitudeTo); }
                if (LongitudeFrom > 0)
                { dp.Command.Parameters.AddWithValue("@LongitudeFrom", LongitudeFrom); }
                if (LongitudeTo > 0)
                { dp.Command.Parameters.AddWithValue("@LongitudeTo", LongitudeTo); }
                if (Name != null || !Name.Equals(""))
                { dp.Command.Parameters.AddWithValue("@Name", Name); }
                IDataReader reader = dp.Command.ExecuteReader();
                while (reader.Read())
                {
                    result.Add(DALUtility.GetEntityValue<Common.Entity.Road>(reader, FetchLevel));
                }

            }
            return result;
        }
    }
}
