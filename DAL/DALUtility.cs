﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.Common;
using Common;

namespace DAL
{
    public static class DALUtility
    {

        #region Properties

        public static int ListFetchLevel
        {
            get { return 2; }
        }

        #endregion /Properties

        #region Methods

        public static IDataReader ExecuteReader(DbCommand cmd)
        {
            return cmd.ExecuteReader(CommandBehavior.Default);
        }

        public static object ExecuteScalar(DbCommand cmd)
        {
            return cmd.ExecuteScalar();
        }

        public static T GetEntityValue<T>(IDataReader reader) where T : Common.Entity.BaseEntity, new()
        {
            return GetEntityValue<T>(reader, Utility.RowFetchLevel);
        }


        public static T GetReportValue<T>(IDataReader reader) where T : Common.Report.BaseReport, new()
        {
            T entity = new T();

            Type type = typeof(T); // Get entity type
            PropertyInfo[] properties = type.GetProperties(); // Obtain all properties

            foreach (var prop in properties) // Loop through properties
            {
                string strPropName = prop.Name;

                string strColumnValue = GetColumnValue(reader, strPropName);
                if (!string.IsNullOrEmpty(strColumnValue))
                {
                    if (IsNullable(prop.PropertyType))
                    {
                        if (prop.PropertyType == typeof(int?))
                        {
                            prop.SetValue(entity, Converter.ChangeType<int?>(strColumnValue), null);
                        }
                        else if (prop.PropertyType == typeof(Int64?))
                        {
                            prop.SetValue(entity, Converter.ChangeType<Int64?>(strColumnValue), null);
                        }
                        else if (prop.PropertyType == typeof(Int16?))
                        {
                            prop.SetValue(entity, Converter.ChangeType<Int16?>(strColumnValue), null);
                        }
                        else if (prop.PropertyType == typeof(bool?))
                        {
                            prop.SetValue(entity, Converter.ChangeType<bool?>(strColumnValue), null);
                        }
                        else if (prop.PropertyType == typeof(DateTime?))
                        {
                            prop.SetValue(entity, Converter.ChangeType<DateTime?>(strColumnValue), null);
                        }
                    }
                    else if (prop.PropertyType == typeof(Byte[]))
                    {
                        prop.SetValue(entity, (GetColumn(reader, strPropName)), null);
                    }
                    else
                    {
                        prop.SetValue(entity, Convert.ChangeType(strColumnValue, prop.PropertyType), null);
                    }
                }
            }
            return entity;
        }


        public static T GetEntityValue<T>(IDataReader reader, int fetchLevel) where T : Common.Entity.BaseEntity, new()
        {
            T entity = new T();

            Type type = typeof(T); // Get entity type
            PropertyInfo[] properties = type.GetProperties(); // Obtain all properties

            foreach (var prop in properties) // Loop through properties
            {
                string strPropName = prop.Name;

                if (prop.PropertyType.BaseType != typeof(Common.Entity.BaseEntity))
                {
                    string strColumnValue = GetColumnValue(reader, strPropName);
                    if (!string.IsNullOrEmpty(strColumnValue))
                    {
                        if (IsNullable(prop.PropertyType))
                        {
                            if (prop.PropertyType == typeof(int?))
                            {
                                prop.SetValue(entity, Converter.ChangeType<int?>(strColumnValue), null);
                            }
                            else if (prop.PropertyType == typeof(Int64?))
                            {
                                prop.SetValue(entity, Converter.ChangeType<Int64?>(strColumnValue), null);
                            }
                            else if (prop.PropertyType == typeof(Int16?))
                            {
                                prop.SetValue(entity, Converter.ChangeType<Int16?>(strColumnValue), null);
                            }
                            else if (prop.PropertyType == typeof(bool?))
                            {
                                prop.SetValue(entity, Converter.ChangeType<bool?>(strColumnValue), null);
                            }
                            else if (prop.PropertyType == typeof(DateTime?))
                            {
                                prop.SetValue(entity, Converter.ChangeType<DateTime?>(strColumnValue), null);
                            }
                        }
                        else if (prop.PropertyType == typeof(Byte[]))
                        {
                            prop.SetValue(entity, (GetColumn(reader, strPropName)), null);
                        }
                        else
                        {
                            prop.SetValue(entity, Convert.ChangeType(strColumnValue, prop.PropertyType), null);
                        }
                    }
                }
                else
                {
                    string strForeignColumnName = (prop.GetCustomAttributes(typeof(Common.Association), false).Single() as Common.Association).ForeignColumnName;
                    string strRelatedEntityIdentityValue = GetColumnValue(reader, strForeignColumnName);

                    if (!string.IsNullOrEmpty(strRelatedEntityIdentityValue))
                    {
                        Type relatedEntityType = prop.PropertyType;
                        var relatedEntity = Activator.CreateInstance(relatedEntityType);

                        Assembly assembly = typeof(DALUtility).Assembly; // DAL assembly
                        Type relatedDALType = assembly.GetType("DAL." + prop.PropertyType.Name + "DAL");
                        MethodInfo oMethodInfo = relatedDALType.GetMethod("GetRowByLevel");

                        foreach (var relatedProp in relatedEntity.GetType().GetProperties()) // Loop through properties
                        {
                            if (relatedProp.GetCustomAttributes(typeof(Common.IsIdentity), false).Any())
                            {
                                string strPrimaryColumnName = relatedProp.Name;
                                Type relatedColumnType = relatedProp.PropertyType;
                                prop.SetValue(entity, oMethodInfo.Invoke(null, new object[] { Convert.ChangeType(strRelatedEntityIdentityValue, relatedProp.PropertyType), (Utility.RowFetchLevel) }), null);

                                break;
                            }
                        }
                    }
                }
            }

            return entity;
        }

        private static T GetColumnValue<T>(IDataReader reader, string columnName)
        {
            string strColumnValue = GetColumnValue(reader, columnName);
            if (!string.IsNullOrEmpty(strColumnValue))
            {
                return (T)Convert.ChangeType(strColumnValue, typeof(T));
            }
            else
            {
                return default(T);
            }
        }

        private static string GetColumnValue(IDataReader reader, string columnName)
        {
            string strValue = null;
            if (ColumnExists(reader, columnName) && reader[columnName] != DBNull.Value)
            {
                strValue = reader[columnName].ToString();
            }
            return strValue;
        }

        private static object GetColumn(IDataReader reader, string columnName)
        {
            object strValue = null;
            if (ColumnExists(reader, columnName) && reader[columnName] != DBNull.Value)
            {
                strValue = reader[columnName];
            }
            return strValue;
        }

        private static T GetColumnEntityValue<T>(IDataReader reader, string columnName) where T : Common.Entity.BaseEntity
        {
            return (T)Convert.ChangeType(GetColumnValue(reader, columnName), typeof(T));
        }

        private static bool ColumnExists(IDataReader reader, string columnName)
        {
            reader.GetSchemaTable().DefaultView.RowFilter = "ColumnName='" + columnName + "'";
            return (reader.GetSchemaTable().DefaultView.Count > 0);
        }

        public static T GenerateFullObject<T>(IDataReader reader) where T : Common.Entity.BaseEntity, new()
        {
            return GetEntityValue<T>(reader);
        }

        public static void SetFilterParameters(DataProvider dp, Common.ListFilter filter)
        {
            dp.Command.Parameters.AddWithValue("@FromRow", filter.FromRow);
            dp.Command.Parameters.AddWithValue("@ToRow", filter.ToRow);
            if (filter.Sort != null)
            {
                if (!string.IsNullOrEmpty(filter.Sort.SortField))
                {
                    dp.Command.Parameters.AddWithValue("@SortField", filter.Sort.SortField);
                }
                dp.Command.Parameters.AddWithValue("@SortDirection", filter.Sort.SortDirection.ToString());
            }
        }

        public static void SetSortParameters(DataProvider dp, Common.Sort sort)
        {
            if (sort != null)
            {
                if (!string.IsNullOrEmpty(sort.SortField))
                {
                    dp.Command.Parameters.AddWithValue("@SortField", sort.SortField);
                }
                dp.Command.Parameters.AddWithValue("@SortDirection", sort.SortDirection.ToString());
            }
        }

        private static bool IsNullable(Type type)
        {
            return (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>));
        }

        public static string GetFieldValue(object field)
        {
            if (field == null)
            {
                return "NULL";
            }
            else if (field.GetType() == typeof(string))
            {
                return "'" + field.ToString() + "'";
            }
            else if (field.GetType() == typeof(bool))
            {
                return ((bool)field ? "1" : "0");
            }
            else
            {
                return field.ToString();
            }
        }

        public static Boolean TestConection(String ConnectionString)
        {
            try
            {

                SqlConnection con = new SqlConnection(ConnectionString);
                con.Open();
                con.Close();
                return true;
            }
            catch
            {
                return false;
            }

        }

        #endregion /Methods

    }
}
