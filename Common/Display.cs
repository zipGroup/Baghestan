﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Display : System.Attribute
    {

        #region Properties

        public string Name { get; set; }
        public string Icon { get; set; }

        #endregion /Properties

        #region Constructors

        public Display()
        {

        }

        #endregion /Constructors


    }
}
