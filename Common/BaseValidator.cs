﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [AttributeUsage(AttributeTargets.Property)]
    public abstract class BaseValidator : System.Attribute
    {

        public string ErrorMessage { set; get; }
        public abstract Boolean Validate(Object obj);
    }
}
