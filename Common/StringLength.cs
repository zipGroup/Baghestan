﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    
    public class StringLength : BaseValidator
    {

        public int MaxLength { set; get; }
        public int MinLength { set; get; }

        public StringLength(int max,int min)
        {
            this.MaxLength = max;
            this.MinLength = min;
        }

        public override bool Validate (Object value )
        {
            Boolean isValid = true;
            String strvalue = null;
            try
            {
                strvalue = (string)value;
            }
            catch
            {
                throw new Exception("this type is not a String");
            }
            if(strvalue.Length>MaxLength || strvalue.Length < MinLength)
            {
                isValid = false;
            }

            return isValid;
        }
    }
}
