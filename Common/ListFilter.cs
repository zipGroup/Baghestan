﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class ListFilter
    {

        #region Properties

        public Sort Sort { get; set; }

        public int FromRow { get; set; }

        public int ToRow { get; set; }

        #endregion /Properties

        #region Constructors

        private ListFilter()
        {
            this.Sort = new Sort();
            this.FromRow = -1;
            this.ToRow = -1;
        }

        public ListFilter(Sort sort, int fromRow, int toRow)
        {
            this.Sort = sort;
            this.FromRow = fromRow;
            this.ToRow = toRow;
        }

        public ListFilter(int fromRow, int toRow)
        {
            this.FromRow = fromRow;
            this.ToRow = toRow;
        }

        #endregion /Constructors

    }
}
