﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;
using System.Reflection;

namespace Common
{

    public enum SortDirection
    {
        ASC = 0,
        DESC = 1
    }

    [Serializable()]
    public class Sort
    {

        #region Constructors

        public Sort()
        {
            this.SortField = string.Empty;
            this.SortDirection = SortDirection.ASC;
        }

        #endregion /Constructors

        #region Properties

        public string SortField { get; set; }

        public SortDirection SortDirection { get; set; }

        #endregion /Properties

    }

    public class OrderByInfo : Sort
    {
        public bool Initial { get; set; }
    }

}