﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Common.Entity
{


    #region Info
    public struct Owner_info
    {
        public const string OwnerId = "OwnerId";
        public const string MapId = "MapId";
    }

    #endregion


    public class Owner : BaseEntity, IPrototype<Owner> 
    {
        #region Properties
        private int ownerId;
        [IsIdentity]
        public int OwnerId
        {
            get { return ownerId; }
            set { ownerId = value; }
        }


        private Map map;
        [Association(ForeignColumnName = Owner_info.MapId)]
        public Map Map
        {
            get { return map; }
            set { map = value; }
        }

        #endregion

        public Owner Clone()
        {
            return this.MemberwiseClone() as Owner;
        }

        public Owner DeepCopy()
        {
            return this.Clone();
        }
    }
}