﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    #region Info
    public struct Scroll_info
    {
        public const string ScrollId = "ScrollId";
        public const string Verifier = "Verifier";
        public const string WaterRationId = "WaterRationId";
        public const string RiverId = "RiverId";
    }
    #endregion

    public class Scroll : BaseEntity, IPrototype<Scroll>
    {

        #region Properties
        private int scrollId;
        [IsIdentity]
        public int ScrollId
        {
            get
            {
                return scrollId;
            }

            set
            {
                scrollId = value;
            }
        }

        [Association(ForeignColumnName = Scroll_info.WaterRationId)]
        public WaterRation WaterRation
        {
            get
            {
                return waterRation;
            }

            set
            {
                waterRation = value;
            }
        }

        private WaterRation waterRation;

        [Association(ForeignColumnName = Scroll_info.RiverId)]
        public River River
        {
            get
            {
                return river;
            }

            set
            {
                river = value;
            }
        }

        private String verifier;
        public string Verifier
        {
            get
            {
                return verifier;
            }

            set
            {
                verifier = value;
            }
        }

        private River river;
        #endregion

        public Scroll Clone()
        {
            return this.MemberwiseClone() as Scroll;
        }
         
        public Scroll DeepCopy()
        {
            return this.Clone();
        }
    }
}
