﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{

    #region Info
    public struct Expression_info
    {
        public const string ExpessionId = "ExpessionId";
        public const string Title = "Title";
        public const string Description = "Description";
    }
    #endregion
    public class Expression : BaseEntity,IPrototype<Expression>
    {
        #region Properties
        private int expressionId;
        [IsIdentity]
        public int ExpressionId
        {
            get { return expressionId; }
            set { expressionId = value; }
        }

        private String title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }

        private String description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        #endregion

        public Expression Clone()
        {
            return this.MemberwiseClone() as Expression;
        }

        public Expression DeepCopy()
        {
            return this.Clone();
        }
    } 
}
