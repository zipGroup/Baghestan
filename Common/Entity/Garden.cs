﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{

    #region Info
    public struct Garden_info
    {
        public const string GardenId = "GardenId";
        public const string Name = "Name";
        public const string Age = "Age";
        public const string Area = "Area";
        public const string Latitude = "Latitude";
        public const string Longitude = "Longitude";
        public const string GardenTypeId = "GardenTypeId";
        public const string CellarId = "CellarId";
        public const string SSNGardener = "SSNGardener";
        public const string WaterRationId = "WaterRationId";
        public const string DistrictId = "DistrictId";
    }

    #endregion
    public class Garden : BaseEntity, IPrototype<Garden>
    {

        #region Properties
        private int gardenId;
        [Display(Name = "کد")]
        [IsIdentity]
        public int GardenId
        {
            get { return gardenId; }
            set { gardenId = value; }
        }

        private int area;
        [Display(Name = "مساحت")]
        public int Area
        {
            get { return area; }
            set { area = value; }
        }
        private Cellar cellar;
        [Display(Name = "چاه خانه")]
        [Association(ForeignColumnName = Garden_info.CellarId)]
        public Cellar Cellar
        {
            get { return cellar; }
            set { cellar = value; }
        }

        private double longitude;
        [Display(Name = "طول جغرافیایی")]
        public double Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }


        private double latitude;
        [Display(Name = "عرض جغرافیایی")]
        public double Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }

        private int age;
        [Display(Name = "سن")]
        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        private String name;
        [Display(Name = "نام")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private District district;
        [Display(Name = "محل")]
        [Association(ForeignColumnName = Garden_info.DistrictId)]
        public District District
        {
            get { return district; }
            set { district = value; }
        }

        private GardenType gardenType;
        [Display(Name = "نوع باغ")]
        [Association(ForeignColumnName = Garden_info.GardenTypeId)]
        public GardenType GardenType
        {
            get { return gardenType; }
            set { gardenType = value; }
        }

        private Person gardener;
        [Display(Name = "باغ دار")]
        [Association(ForeignColumnName = Garden_info.SSNGardener)]
        public Person Gardener
        {
            get { return gardener; }
            set { gardener = value; }
        }

        private WaterRation waterRation;

        [Association(ForeignColumnName = Garden_info.WaterRationId)]
        public WaterRation WaterRation
        {
            get { return waterRation; }
            set { waterRation = value; }
        }

        #endregion


        #region Methods
        public Garden Clone()
        {
            return this.MemberwiseClone() as Garden;
        }

        public Garden DeepCopy()
        {
            return this.Clone();
        }

        #endregion /Methods 

    }
}
