﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    #region Info 
    public struct Page_info
    {
        public const string PageId = "PageId";
        public const string Name = "Name";
    }
    #endregion

    public class Page : BaseEntity, IPrototype<Page>
    {
         
        #region Properties
        private int pageId;
        [IsIdentity]
        public int PageId
        {
            get { return pageId; }
            set { pageId = value; }
        }

        private String name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        public Page Clone()
        {
            return this.MemberwiseClone() as Page;
        }

        public Page DeepCopy()
        {
            return this.Clone();
        }
        #endregion
    }
}
