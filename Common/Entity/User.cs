﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{

    #region Info
    public struct User_info
    {
        public const string UserId = "UserId";
        public const string UserName = "UserName";
        public const string FirstName = "FirstName";
        public const string LastName = "LastName";
        public const string Password = "PassWord";
    }
    #endregion

    public class User : BaseEntity, IPrototype<User>
    { 
        #region Properties

        private int userId;

        [IsIdentity]
        public int UserId
        {
            get { return userId; }

            set { userId = value; }
        }

        private string userName;

        public string UserName
        {
            get { return userName; }
            set { userName = Utility.CorrectCharCode(value); }
        }

        private string firstName;

        public string FirstName
        {
            get { return firstName; }
            set { firstName = Utility.CorrectCharCode(value); }
        }

        private string lastName;

        public string LastName
        {
            get { return lastName; }
            set { lastName = Utility.CorrectCharCode(value); }
        }

        private string password;

        public string Password
        {
            get { return password; }
            set { password = Utility.CorrectCharCode(value); }
        }

        public Byte[] Photo
        {
            get
            {
                return photo;
            }

            set
            {
                photo = value;
            }
        }

        Byte[] photo;

        #endregion

        #region Methods
        public User Clone()
        {
            return this.MemberwiseClone() as User;
        }

        public User DeepCopy()
        {
            return this.Clone();
        }

        #endregion /Methods
    }
}
