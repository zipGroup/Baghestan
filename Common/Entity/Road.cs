﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{

    #region Info
    public struct Road_info
    {
        public const string RoadId = "RoadId";
        public const string Length = "Length";
        public const string ConstructionDate = "ConstructionDate";
        public const string Latitude = "Latitude";
        public const string Longitude = "Longitude";
    }

    #endregion

    public class Road : BaseEntity, IPrototype<Road>
    {

        #region Properties

        private int roadId;
        [IsIdentity]
        public int RoadId
        {
            get
            {
                return roadId;
            }

            set
            {
                roadId = value;
            }
        }

        private DateTime constructionDate;
        public DateTime ConstructionDate
        {
            get
            {
                return constructionDate;
            }

            set
            {
                constructionDate = value;
            }
        }

        private Double length;
        public double Length
        {
            get
            {
                return length;
            }

            set
            {
                length = value;
            }
        }

        private Double latitude;
        public double Latitude
        {
            get
            {
                return latitude;
            }

            set
            {
                latitude = value;
            }
        }

        private Double longitude;
        public double Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }

        private String name;
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

       
        #endregion 

        public Road Clone()
        {
            return this.MemberwiseClone() as Road;
        }

        public Road DeepCopy()
        {
            return this.Clone();
        }
    }
}
