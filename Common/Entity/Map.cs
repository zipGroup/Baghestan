﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{

    #region Info
    public struct Map_info
    {
        public const string MapId = "MapId";
        public const string Name = "Name";
    }
    #endregion

    public class Map : BaseEntity, IPrototype<Map>
    { 

        #region Properties
        private int mapId;

        [IsIdentity]
        public int MapId
        {
            get { return mapId; }
            set { mapId = value; }
        }

        private String name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        #endregion

        public Map Clone()
        {
            return this.MemberwiseClone() as Map;
        }

        public Map DeepCopy()
        {
            return this.Clone();
        }
    }
}
