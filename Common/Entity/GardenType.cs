﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Common.Entity
{

    #region Info
    public struct GardenType_info
    {
        public const string GardenTypeId = "GardenTypeId";
        public const string Name = "Name";
    }

    #endregion
    public class GardenType : BaseEntity, IPrototype<GardenType>
    {


        #region Properties
        private int gardenTypeId;
        [IsIdentity]
        public int GardenTypeId
        {
            get { return gardenTypeId; }
            set { gardenTypeId = value; }
        }

        private String name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private Boolean haveProduct;
        public bool HaveProduct
        {
            get
            {
                return haveProduct;
            }

            set
            {
                haveProduct = value;
            }
        }

        
        #endregion

        public GardenType Clone()
        {
            return this.MemberwiseClone() as GardenType;
        }

        public GardenType DeepCopy()
        {
            return this.Clone();
        }
    }
}