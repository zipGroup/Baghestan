﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;

namespace Common.Entity
{
    public struct MenuInfo
    {
        public const string TableName = "Menu";
        public const string MenuId = "MenuId";
        public const string ParentId = "ParentId";
        public const string Name = "Name";
        public const string Title = "Title";
        public const string PageId = "PageId";
        public const string NeedAccess = "NeedAccess";
        public const string OrderNo = "OrderNo";
        public const string Code = "Code";
        public const string Icon = "Icon"; 
    }


    public class Menu : BaseEntity, IPrototype<Menu>
    {
        #region Constructors

        public Menu()
        {
            this.MenuId = -1;
            this.Parent = null;
            this.Name = string.Empty;
            this.Title = string.Empty;
            this.Page = null;
            this.NeedAccess = false;
            this.OrderNo = -1;
            this.Code = false;
        }

        #endregion /Constructors

        #region Properties

        private int _menuId;
        [IsIdentity]
        public int MenuId
        {
            get { return _menuId; }
            set { _menuId = value; }
        }

        private String icon;

        private Menu _parent;
        [Association(ForeignColumnName = MenuInfo.ParentId)]
        public Menu Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }

        private string _name;
        public string Name
        {
            get { return _name; }
            set { _name = Utility.CorrectCharCode(value); }
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { _title = Utility.CorrectCharCode(value); }
        }

        private Page _page;
        [Association(ForeignColumnName = MenuInfo.PageId)]
        public Page Page
        {
            get { return _page; }
            set { _page = value; }
        }

        private bool _needAccess;
        public bool NeedAccess
        {
            get { return _needAccess; }
            set { _needAccess = value; }
        }

        private Int16 _orderNo;
        public Int16 OrderNo
        {
            get { return _orderNo; }
            set { _orderNo = value; }
        }

        private bool _code;

        public bool Code
        {
            get { return _code; }
            set { _code = value; }
        }

        public string Icon
        {
            get
            {
                return icon;
            }

            set
            {
                icon = value;
            }
        }


        #endregion /Properties

        #region Methods

        public Menu Clone()
        {
            return this.MemberwiseClone() as Menu;
        }

        public Menu DeepCopy()
        {
            return this.Clone();
        }

        #endregion /Methods

    }
}
