﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{


    #region Info
    public struct Tree_info
    {
        public const string TreeId = "TreeId";
        public const string Age = "Age";
        public const string PlantId = "PlantId";
    }

    #endregion


    public class Tree : Plant, IPrototype<Tree>
    {
        #region Properties
        private int treeId;
        [Display(Name="کد")]
        [IsIdentity]
        public int TreeId
        {
            get
            {
                return treeId;
            }

            set
            {
                treeId = value;
            }
        }
        

        private int age;

        [Display(Name = "سن")]
        public int Age
        {
            get
            {
                return age;
            }

            set
            {
                age = value;
            }
        }



        #endregion

        #region Methods
        public Tree Clone()
        {
            return this.MemberwiseClone() as Tree;
        }

        public Tree DeepCopy()
        {
            return this.Clone();
        }

        #endregion /Methods
    }

} 