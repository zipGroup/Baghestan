﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    public abstract class BaseEntity 
    {
        public BaseEntity()
        {
        }

        public bool IsValid()
        {
            bool isVAlid = true;

            PropertyInfo[] properties = GetType().GetProperties(); 
            foreach(PropertyInfo prop in properties)
            {
                foreach(Attribute attr in prop.GetCustomAttributes(typeof(Common.BaseValidator), true))
                {
                    if (!((BaseValidator)attr).Validate(prop.GetValue(this)))
                    {
                        isVAlid = false;
                    }
                }
            }

            return isVAlid;
        }
    }
}
