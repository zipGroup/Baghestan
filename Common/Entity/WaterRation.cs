﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Common.Entity
{

    #region Info
    public struct WaterRation_info
    {
        public const string WaterRationId = "WaterRationId";
        public const string Date = "Date";
        public const string Time = "Time";
        public const string GardenId = "GardenId";
        public const string ScrollId = "ScrollId";
    }
    #endregion

    public class WaterRation : BaseEntity, IPrototype<WaterRation> 
    {
        #region Properties
        private int waterRationId;
        
       

        [IsIdentity]
        public int WaterRationId
        {
            get
            {
                return waterRationId;
            }

            set
            {
                waterRationId = value;
            }
        }

        private DateTime time;
        public DateTime Time
        {
            get
            {
                return time;
            }

            set
            {
                time = value;
            }
        }

        private DateTime date;
        public DateTime Date
        {
            get
            {
                return date;
            }

            set
            {
                date = value;
            }
        }

        private DateTime timeFrom;
        public DateTime TimeFrom
        {
            get
            {
                return timeFrom;
            }

            set
            {
                timeFrom = value;
            }
        }

        private Scroll scroll;
        [Association(ForeignColumnName = WaterRation_info.ScrollId)]
        public Scroll Scroll
        {
            get
            {
                return scroll;
            }

            set
            {
                scroll = value;
            }
        }

        private Garden garden;
        [Association(ForeignColumnName = WaterRation_info.GardenId)]
        public Garden Garden
        {
            get
            {
                return garden;
            }

            set
            {
                garden = value;
            }
        }

        
       
       
        #endregion

        public WaterRation Clone()
        {
            return this.MemberwiseClone() as WaterRation;
        }

        public WaterRation DeepCopy()
        {
            return this.Clone();
        }

    }

}