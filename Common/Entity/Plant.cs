﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Common.Entity
{

    #region Info
    public struct Plant_info
    {
        public const string PlantId = "PlantId";
        public const string ProductId = "ProductId";
        public const string GeneticInformation = "GeneticInformation";
        public const string Name = "Name";
        public const string Status = "Status";
        public const string GardenId = "GardenId";
    }

    #endregion

    public class Plant : BaseEntity, IPrototype<Plant>
    {
        #region Properties

        [Display(Name = "محصول")]
        [Association(ForeignColumnName = Plant_info.ProductId)]
        public Product Product
        {
            get { return product; }
            set { product = value; }
        }

        private Product product;

        [Display(Name = "باغ")]
        [Association(ForeignColumnName = Plant_info.GardenId)]
        public Garden Garden
        {
            get { return garden; }
            set { garden = value; }
        }

        [StringLength(2,7)]
        [Display(Name = "نام")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }


        [Display(Name = "اطلاعات ژنتیکی")]
        public string GeneticInformation
        {
            get { return geneticInformation; }
            set { geneticInformation = value; }
        }

        [Display(Name = "وضعیت")]
        public string Status
        {
            get { return status; }
            set { status = value; }
        }
        private Garden garden;

        private string geneticInformation;

        private string name;
        #endregion

        #region Methods
        public Plant Clone()
        {
            return this.MemberwiseClone() as Plant;
        }

        public Plant DeepCopy()
        {
            return this.Clone();
        }

        private string status;
        #endregion /Methods
    } 
}