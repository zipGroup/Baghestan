﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{

    #region Info
    public struct Role_info
    {
        public const string RoleId = "RoleId";
        public const string Name = "Name";
    }
    #endregion
    public class Role : BaseEntity,IPrototype<Role>
    {

        #region Properties
        private int roleId;
        [IsIdentity]
        public int RoleId
        {
            get
            {
                return roleId;
            }

            set
            {
                roleId = value;
            }
        }

        private String name;
        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }
        #endregion

        public Role Clone()
        {
            return this.MemberwiseClone() as Role;
        }

        public Role DeepCopy()
        { 
            return this.Clone();
        }
    }
}
