﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{


    #region Info
    public struct PersonType_info
    {
        public const string PersonTypeId = "PersonTypeId";
        public const string Name = "Name";
    }
    #endregion

     
    public class PersonType : BaseEntity, IPrototype<PersonType>
    {
        #region Properties
        private int personTypeId;
        [IsIdentity]
        public int PersonTypeId
        {
            get { return personTypeId; }
            set { personTypeId = value; }
        }

        private String name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        #endregion

        public PersonType Clone()
        {
            return this.MemberwiseClone() as PersonType;
        }

        public PersonType DeepCopy()
        {
            return this.Clone();
        }
    }
}