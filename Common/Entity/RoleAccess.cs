﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    public struct RoleAccessInfo
    {
        public const string pageId = "PageId";
        public const string RoleId = "RoleId";

    }
    public class RoleAccess : BaseEntity, IPrototype<RoleAccess>
    {

        #region Constructors

        public RoleAccess()
        {
            this.Role = null;
            this.Page = null;
        }
        #endregion /Constructors

        #region Properties

        private Page page;
        [Association(ForeignColumnName =RoleAccessInfo.pageId)]
        public Page Page
        {
            get
            {
                return page;
            }

            set
            {
                page = value;
            }
        }

        private Role role;

        [Association(ForeignColumnName = RoleAccessInfo.RoleId)]
        public Role Role
        {
            get
            {
                return role;
            }

            set
            {
                role = value;
            }
        }



        #endregion /Properties
         
        #region Methods

        public RoleAccess Clone()
        {
            return this.MemberwiseClone() as RoleAccess;
        }

        public RoleAccess DeepCopy()
        {
            return this.Clone();
        }

        #endregion /Methods
    }
}
