﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{

    #region Info
    public struct District_info
    {
        public const string DistrictId = "DistrictId";
        public const string CurrentStatus = "CurrentStatus";
        public const string Latitude = "Latitude";
        public const string Longitude = "Longitude";
        public const string IsBlock = "IsBlock";
        public const string History = "History";
        public const string Name = "Name";
        public const string SSNChief = "SSNChief";
        public const string OwnerId = "OwnerId";
    }
    #endregion
    public class District : BaseEntity, IPrototype<District>
    {
        #region Properties
        private int districtId;
        [IsIdentity]
        [Display(Name="کد محل")]
        public int DistrictId
        {
            get { return districtId; }
            set { districtId = value; }
        }

        private String currentStatus;
        [Display(Name = "وضعیت فعلی")]
        public string CurrentStatus
        {
            get { return currentStatus; }
            set { currentStatus = value; }
        }

        private String history;

        public string History
        {
            get { return history; }
            set { history = value; }
        }

        private Double latitude;
        [Display(Name = "عرض جغرافیایی")]
        public double Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }

        private Double longitude;
        [Display(Name = "ظول جغرافیایی")]
        public double Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }

        private bool isBlock;
        public bool IsBlock
        {
            get { return isBlock; }
            set { isBlock = value; }
        }

        private String name;
        [Display(Name = "نام محل")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private Person person;
        [Association(ForeignColumnName = District_info.SSNChief)]
        public Person Person
        {
            get { return person; }
            set { person = value; }
        }

        private Owner owner;
        [Association(ForeignColumnName = District_info.OwnerId)]
        public Owner Owner
        {
            get { return owner; }
            set { owner = value; }
        }

        #endregion
         
        public District Clone()
        {
            return this.MemberwiseClone() as District;
        }

        public District DeepCopy()
        {
            return this.Clone();
        }
    }
}