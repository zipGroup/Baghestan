﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Common.Entity
{

    #region Info
    public struct Photo_info
    { 
        public const string PhotoId = "PhotoId";
        public const string Name = "Name";
        public const string GardenId = "GardenId";
        public const string Image = "Image";
    }

    #endregion

    public class Photo : BaseEntity, IPrototype<Photo>
    {
        #region Properties

        private int photoId;
        [IsIdentity]
        public int PhotoId
        {
            get { return photoId; }
            set { photoId = value; }
        }

        private Garden garden;
        [Association(ForeignColumnName = Photo_info.GardenId)]
        public Garden Garden
        {
            get { return garden; }
            set { garden = value; }
        }

        private String name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public BitArray Image
        {
            get
            {
                return image;
            }

            set
            {
                image = value;
            }
        }

        private BitArray image;

        #endregion

        public Photo Clone()
        {
            return this.MemberwiseClone() as Photo;
        }

        public Photo DeepCopy()
        {
            return this.Clone();
        }
    }
}
