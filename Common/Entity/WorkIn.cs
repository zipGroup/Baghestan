﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    #region Info
    public struct WorkIn_Info
    {
        public const string SSNFarmer = "SSNFarmer";
        public const string GardenId = "GardenId";
    }
    #endregion
    public class WorkIn : BaseEntity, IPrototype<WorkIn>
    {
        #region Properties
        private String sSNFarmer;
        [IsIdentity]
        public string SSNFarmer
        {
            get { return sSNFarmer;}
            set { sSNFarmer = value; }
        }

        private Garden garden;
        [IsIdentity]
        public Garden Garden
        {
            get { return garden;}
            set{ garden = value;}
        }


        #endregion

        public WorkIn Clone()
        {
            return this.MemberwiseClone() as WorkIn;
        }

        public WorkIn DeepCopy()
        {
            return this.Clone();
        }
    }
}
