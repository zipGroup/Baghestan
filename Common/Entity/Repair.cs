﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{

    #region Info
    public struct Repair_info
    {
        public const string RepairId = "RepairId";
        public const string Date = "Date";
        public const string performer = "performer";
        public const string description = "description";
        public const string CellarId = "CellarId";
    }
    #endregion

    public class Repair : BaseEntity, IPrototype<Repair>
    {

        #region Properties
        private int repairId;
        [IsIdentity]
        public int RepairId
        {
            get { return repairId; }
            set { repairId = value; }
        }
        
        private Cellar cellar;
        [Association(ForeignColumnName = Repair_info.CellarId)]
        public Cellar Cellar
        {
            get { return cellar; }
            set { cellar = value; }
        }

        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        private String performer;
        public string Performer
        {
            get { return performer; }
            set { performer = value; }
        }

        private String description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        #endregion

        public Repair Clone()
        { 
            return this.MemberwiseClone() as Repair;
        }

        public Repair DeepCopy()
        {
            return this.Clone();
        }
    }
}
