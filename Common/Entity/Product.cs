﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{

    #region Info
    public struct Product_info
    {
        public const string ProductId = "ProductId";
        public const string Name = "Name";
    }

    #endregion

    public class Product : BaseEntity, IPrototype<Product>
    {
        #region Properties
        private int productId;

        [IsIdentity]
        public int ProductId
        {
            get{return productId;}
            set{productId = value;}
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        #endregion


        #region Methods
        public Product Clone()
        {
            return this.MemberwiseClone() as Product;
        }

        public Product DeepCopy()
        {
            return this.Clone();
        }

        #endregion /Methods
    }

}
 