﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{

    #region Info
    public struct Cellar_info
    {
        public const string CellarId = "CellarId";
        public const string Name = "Name";
        public const string Area = "Area";
        public const string DistrictId = "DistrictId";
    }

    #endregion
    public class Cellar : BaseEntity, IPrototype<Cellar>
    {

        #region peroperties

        private int cellarId;
        [Display(Name = "کد")]
        [IsIdentity]
        public int CellarId
        {
            get { return cellarId; }
            set { cellarId = value; }
        }

        private String name;
        [Display(Name = "نام")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private int area;
        [Display(Name = "مساحت")]
        public int Area
        {
            get { return area; }
            set { area = value; }
        }


        private District district;

        [Association(ForeignColumnName = Cellar_info.DistrictId)]
        public District District
        {
            get { return district; }
            set { district = value; }
        }


        public Cellar Clone()
        {
            return this.MemberwiseClone() as Cellar;
        }

        public Cellar DeepCopy()
        {
            return this.Clone();
        }
    }
    #endregion 
}
