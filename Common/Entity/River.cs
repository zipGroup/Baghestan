﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{
    #region Info
    public struct River_info
    {
        public const string RiverId = "RiverId";
        public const string LatitudeStart = "LatitudeStart";
        public const string LongitudeStart = "LongitudeStart";
        public const string LatitudeEnd = "LatitudeEnd";
        public const string LongitudeEnd = "LongitudeEnd";
        public const string Name = "Name";
        public const string DistrictId = "DistrictId";
        public const string ScrollId = "ScrollId";
        public const string OwnerId = "OwnerId";
    }
    #endregion
     
    public class River : BaseEntity, IPrototype<River>
    {
        #region Properties
        private int riverId;
        [IsIdentity]
        public int RiverId
        {
            get { return riverId; }
            set { riverId = value; }
        }

        private float latitudeStart;
        private float longitudeStart;
        private float latitudeEnd;
        private float longitudeEnd;
        private Scroll scroll;
        District district;
        String name;

        [Association(ForeignColumnName = River_info.OwnerId)]
        public Owner Owner
        {
            get { return owner; }
            set { owner = value; }
        }

        public float LatitudeStart
        {
            get
            {
                return latitudeStart;
            }

            set
            {
                latitudeStart = value;
            }
        }

        public float LongitudeStart
        {
            get
            {
                return longitudeStart;
            }

            set
            {
                longitudeStart = value;
            }
        }

        public float LatitudeEnd
        {
            get
            {
                return latitudeEnd;
            }

            set
            {
                latitudeEnd = value;
            }
        }

        public float LongitudeEnd
        {
            get
            {
                return longitudeEnd;
            }

            set
            {
                longitudeEnd = value;
            }
        }

        public Scroll Scroll
        {
            get
            {
                return scroll;
            }

            set
            {
                scroll = value;
            }
        }

        public District District
        {
            get
            {
                return district;
            }

            set
            {
                district = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        private Owner owner;
        #endregion

        public River Clone()
        {
            return this.MemberwiseClone() as River;
        }

        public River DeepCopy()
        {
            return this.Clone();
        }
    }
}
