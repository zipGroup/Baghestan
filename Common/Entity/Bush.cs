﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Entity
{

    #region Info

    public struct Bush_Info
    {
        public const string BushId = "BushId";
        public const string PlantId = "PlantId";
        public const string TermViability = "TermViability";

    }

    #endregion


    public class Bush : Plant, IPrototype<Bush>
    {
        
        #region Properties

        private int bushId;
        [IsIdentity]
        public int BushId
        {
            get { return bushId; }
            set { bushId = value; }
        }

        private Plant plant;

        [Association(ForeignColumnName = Bush_Info.PlantId)]
        public Plant Plant
        {
            get { return plant; }
            set { plant = value; }
        }

        private int termViability;
        public int TermViability
        {
            get { return termViability; }
            set { termViability = value; }
        }

        
        #endregion
         
        public Bush Clone()
        {
            return this.MemberwiseClone() as Bush;
        }

        public Bush DeepCopy()
        {
            return this.Clone();
        }
    }
}
