﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Common.Entity
{


    #region Info
    public struct Person_info
    {
        public const string SSN = "SSN";
        public const string BirthDate = "BirthDate";
        public const string FirstName = "FirstName";
        public const string LastName = "LastName";
        public const string FatherName = "FatherName";
        public const string PersonTypeId = "PersonTypeId";
    }

    #endregion

    public class Person : BaseEntity, IPrototype<Person>
    {

        #region Properties

        private String _SSN;

        [IsIdentity]
        public String SSN
        {
            get { return _SSN; }
            set { _SSN = value; }
        }
        private DateTime birthDate;

        public DateTime BirthDate
        {
            get { return birthDate; }
            set { birthDate = value; }
        }

        private String firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        private String lastName;
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }


        private String fatherName;
        public string FatherName
        {
            get { return fatherName; }
            set { fatherName = value; }
        }

        private PersonType personType;
        [Association(ForeignColumnName = Person_info.PersonTypeId)]
        public PersonType PersonType
        {
            get { return personType; }
            set { personType = value; }
        }


        public Person Clone()
        {
            return this.MemberwiseClone() as Person;
        }

        public Person DeepCopy()
        {
            return this.Clone();
        }
        #endregion
    }
} 