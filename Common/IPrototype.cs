﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Common
{
    /// <summary>
    /// Summary description for IPrototype
    /// </summary>
    public interface IPrototype<T>
    {
        T Clone();
        T DeepCopy();
    }
}