﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public static class Constants
    {
        public const string Error_InvalidUserNamePassword_Message = "نام کاربری یا کلمه عبور صحیح نمی باشد!";
        public static string Error_Add_Message = "اطلاعات ورودی صحیح نیستند!";
        public const string Error_Login = "خطا در ورود به سیستم";
        public static string Error_Insert = "خطا در ثبت اطلاعات";

        public static string Success_Add_Garden_Message = "اطلاعات باغ با موفقیت ثبت شد";
        public static string Success_Add_Tree_Message = "اطلاعات درخت با موفقیت ثبت شد";
        public static string Success_Insert= "عملیات انجام شد";
    }
}
