﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.IO;
using System.Text;
using System.Configuration;
using System.Security.Cryptography;
using System.ComponentModel;
using System.Reflection;

namespace Common
{
    public static class Utility
    {

        #region Properties

        public static int RowFetchLevel
        {
            get { return 1; }
        }

        #endregion /Properties


        #region Methods
        public static string CorrectCharCode(string s)
        {
            if ((s != null) && (s.Trim() != string.Empty))
            {
                // ي -> ی
                string Str = s.Replace((char)1610, (char)1740);
                // ک -> ک  
                Str = Str.Replace((char)1603, (char)1705);

                return Str.Trim();
            }
            else
                return s;
        }
    }

        #endregion /Methods
}
