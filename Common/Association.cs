﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common
{
    public class Association : System.Attribute
    {

        #region Properties

        public string ForeignColumnName { get; set; }

        #endregion /Properties

        #region Constructors

        public Association()
        {

        }

        #endregion /Constructors

    }
}
