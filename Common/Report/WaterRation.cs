﻿using System;

namespace Common.Report
{
    public class WaterRation : BaseReport
    {
        private String _SSNGardener;
        private int gardenId;
        private String gardenName;
        private int area;
        private int riverId;
        private String riverName;
        private String verifier;
        private int scrollId;
        private DateTime date;
        private int time;
        private int waterRationId;
        private String timeFrom;


        [Display(Name ="کد ملی باغبان")]
        public string SSNGardener { get { return _SSNGardener; } set { _SSNGardener = value; } }
        public int GardenId { get { return gardenId; } set { gardenId = value; } }
        [Display(Name = "نام باغ")]
        public string GardenName { get { return gardenName; } set { gardenName = value; } }
        [Display(Name = " مساحت باغ")]
        public int Area { get { return area; } set { area = value; } }
        public int RiverId { get { return riverId; } set { riverId = value; } }
        [Display(Name = "نام رود")]
        public string RiverName { get { return riverName; } set { riverName = value; } }
        [Display(Name = "تایید کننده")]
        public string Verifier { get { return verifier; } set { verifier = value; } }
        public int ScrollId { get { return scrollId; } set { scrollId = value; } }
        [Display(Name = "در تاریخ")]
        public DateTime Date { get { return date; } set { date = value; } }
        [Display(Name = "از ساعت")]
        public string TimeFrom { get { return timeFrom; } set { timeFrom = value; } }
        [Display(Name = "به مدته ")]
        public int Time { get { return time; } set { time = value; } }
        public int WaterRationId { get { return waterRationId; } set { waterRationId = value; } }
    }
}
