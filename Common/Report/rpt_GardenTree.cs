﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.Report
{
    public class rpt_GardenTree : BaseReport
    {

        #region Properties
        private int gardenId;
        [Display(Name = "کد")]
        [IsIdentity]
        public int GardenId
        {
            get { return gardenId; }
            set { gardenId = value; }
        }

        private int area;
        [Display(Name = "مساحت")]
        public int Area
        {
            get { return area; }
            set { area = value; }
        }

        private double longitude;
        [Display(Name = "طول جغرافیایی")]
        public double Longitude
        {
            get { return longitude; }
            set { longitude = value; }
        }


        private double latitude;
        [Display(Name = "عرض جغرافیایی")]
        public double Latitude
        {
            get { return latitude; }
            set { latitude = value; }
        }

        private int age;
        [Display(Name = "سن")]
        public int Age
        {
            get { return age; }
            set { age = value; }
        }

        private String name;
        [Display(Name = "نام")]
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        [Display(Name = "تعداد بوته")]
        public int Count_Bush { get { return count_Bush; } set { count_Bush = value; } }
        [Display(Name = "تعداد درختان")]
        public int Count_Tree { get { return count_Tree; } set { count_Tree = value; } }

        private int count_Bush;
        private int count_Tree;
        #endregion

    }
}
