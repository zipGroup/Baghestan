﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Common
{

    public enum ExceptionKey : int
    {
        Nothing = -1,
        Timeout = -2,
        HasForeignKey = 547,
        RecordAlreadyExsits = 2601,
    }

    public class CustomException : ApplicationException
    {

        #region Properties

        private Exception _innerException;
        public new Exception InnerException
        {
            get { return _innerException; }
            set { _innerException = value; }
        }

        private ExceptionKey _key;
        public ExceptionKey Key
        {
            get { return _key; }
            set { _key = value; }
        }

        private string _customMessage;
        public string CustomMessage
        {
            get { return _customMessage; }
        }

        #endregion /Properties

        #region Constructors

        public CustomException(ExceptionKey key, Exception innerException)
        {
            this._innerException = innerException;
            this._key = key;
            this._customMessage = GetMessage();
        }

        public CustomException(ExceptionKey key, Exception innerException, string Message)
        {
            this._innerException = innerException;
            this._key = key;
            this._customMessage = Message;
        }

        public CustomException(string Message)
        {
            this._innerException = null;
            this._key = ExceptionKey.Nothing;
            this._customMessage = Message;
        }

        #endregion /Constructors

        #region Methods

        private string GetMessage()
        {
            string result = string.Empty;

            if (_innerException != null)
            {
                result = _innerException.Message;

                if (_innerException is SqlException)
                {
                    Key = (ExceptionKey)((SqlException)_innerException).Number;
                }

                switch (Key)
                {
                    case ExceptionKey.HasForeignKey:
                        result = "ركورد مورد نظر داراي اطلاعات وابسته است و حذف آن امکان ندارد!";
                        break;
                    case ExceptionKey.RecordAlreadyExsits:
                        result = "اطلاعات تكراري است!";
                        break;
                    case ExceptionKey.Timeout:
                        result = "به دلیل حجم زیاد داده خطای زمانی رخ داده است!";
                        break;
                    default:
                        result = "عملیات با اشکال مواجه گردید. لطفا مجددا سعی نمایید!";
                        break;
                }
            }
            else
            {

            }

            return result;
        }

        #endregion /Methods

    }
}
