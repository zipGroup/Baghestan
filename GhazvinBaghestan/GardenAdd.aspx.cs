﻿using Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GhazvinBaghestan
{

    public partial class GardenAdd : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckUserAccess();

            this.fillDistrictDropDown();
            this.fillGardenTypeDropDown();
            this.fillGardenerDropDown();
            this.fillCellarDropDown();
        }

        private void fillGardenTypeDropDown()
        {
            ArrayList gardenTypeList = BLL.GardenType.GetList();
            foreach (Common.Entity.GardenType nextGardenType in gardenTypeList)
            {
                ListItem foo = new ListItem(nextGardenType.Name);
                foo.Value = nextGardenType.GardenTypeId.ToString();
                ddGardenType.Items.Add(foo);
            }
        }

        private void fillDistrictDropDown()
        {
            ArrayList districtList = BLL.DistrictBLL.GetList();
            foreach (Common.Entity.District nextDistrict in districtList)
            {
                ListItem foo = new ListItem(nextDistrict.Name);
                foo.Value = nextDistrict.DistrictId.ToString();
                ddDistrict.Items.Add(foo);
            }
        }

        private void fillCellarDropDown()
        {
            ArrayList cellarList = BLL.CellarBLL.GetList();
            foreach (Common.Entity.Cellar nextCellar in cellarList)
            {
                ListItem foo = new ListItem(nextCellar.Name);
                foo.Value = nextCellar.CellarId.ToString();
                ddCellar.Items.Add(foo);
            }
        }

        private void fillGardenerDropDown()
        {
            ArrayList personList = BLL.PersonBLL.GetList();
            foreach (Common.Entity.Person nextPerson in personList)
            {
                ListItem foo = new ListItem(nextPerson.FirstName+" "+nextPerson.LastName);
                foo.Value = nextPerson.SSN.ToString();
                ddGardener.Items.Add(foo);
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int age = Int32.Parse(txtAgeGarden.Text);
                int area = Int32.Parse(txtArea.Text);
                double latitude = Double.Parse(txtLatitude.Text);
                double longitude = Double.Parse(txtAgeGarden.Text);
                int districtId = Int32.Parse(ddDistrict.SelectedValue);
                int cellarId = Int32.Parse(ddCellar.SelectedValue);
                int gardenTypeId = Int32.Parse(ddGardenType.SelectedValue);
                String name = txtNameGarden.Text;
                String ssnGardener = ddGardener.Text;
                Common.Entity.Garden newGarden = new Common.Entity.Garden()
                {
                    Age = age,
                    Area = area,
                    Name = name,
                    Latitude = latitude,
                    Longitude = longitude,
                    GardenType = new Common.Entity.GardenType()
                    {
                        GardenTypeId = gardenTypeId
                    },
                    District = new Common.Entity.District()
                    {
                        DistrictId = districtId
                    },
                    Cellar = new Common.Entity.Cellar()
                    {
                        CellarId = cellarId
                    },
                    Gardener = new Common.Entity.Person()
                    {
                        SSN = ssnGardener
                    }
                };
                if (newGarden.IsValid())
                {

                    try
                    {
                        BLL.GardenBLL.Insert(newGarden);
                        ShowAlert(Constants.Success_Add_Tree_Message, Constants.Success_Insert);
                    }
                    catch
                    {
                        ShowAlert(Constants.Error_Insert, Constants.Error_Insert);
                    }
                }
                else
                {
                    ShowAlert(Constants.Error_Add_Message, Constants.Error_Insert);
                    return;
                }
            }
            catch
            {
                ShowAlert(Constants.Error_Add_Message, Constants.Error_Insert);
            }
            return;
        }
    }
}