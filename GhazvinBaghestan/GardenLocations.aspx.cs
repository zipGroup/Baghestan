﻿using Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GhazvinBaghestan
{
    public partial class GardenLocations : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckUserAccess();
            SetClientScript();
        }

        private void SetClientScript()
        {
            String markers = getMarkers();
            dvmarkers.InnerHtml = markers;
            String map =  @"
                <script>
                    var layer = new ol.layer.Tile({
                        source: new ol.source.OSM()
                    });
                    var map = new ol.Map({
                        layers: [layer],
                        target: 'map',
                        view: new ol.View({
                            center: ol.proj.fromLonLat([50.058147 ,36.039455]),
                            zoom: 15,
                            maxZoom: 17,
                            minZoom: 13
                        })
                    }); " + addStops() + "</script>";
            dvScript.InnerHtml = map;
        }

        private string getMarkers()
        {
            string strScript = string.Empty;
            ArrayList gardens = BLL.GardenBLL.GetList();
            int counter = 0;
            foreach (Common.Entity.Garden garden in gardens)
            {
                strScript += "<img src = 'assets/global/plugins/OLMap/stop.png' style='margin-top:-24px' id='mark" + (counter++) + "'/>";
            }

            return strScript;
        }

        private string addStops()
        {

            string strScript = string.Empty;
            ArrayList gardens = BLL.GardenBLL.GetList();
            int counter = 0;
            foreach (Common.Entity.Garden garden in gardens)
            {
                strScript += @"
        map.addOverlay(new ol.Overlay({
            position: ol.proj.fromLonLat([" + garden.Longitude + "," + garden.Latitude + @"]),
            element: document.getElementById('mark" + (counter++) + @"')
        })); ";
            }

            return strScript;
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int age = Int32.Parse(txtAge.Text);
                String name = txtName.Text;
                int area = Int32.Parse(txtArea.Text);
                float lat = float.Parse(Lat.Value);
                float lon = float.Parse(Lon.Value);
                Common.Entity.Garden newGarden = new Common.Entity.Garden()
                {
                    Age = age,
                    Name = name,
                    Area = area
                };
                if (newGarden.IsValid())
                {
                    try
                    {
                        BLL.GardenBLL.Insert(newGarden);
                        ShowAlert(Constants.Success_Add_Tree_Message, Constants.Success_Insert);
                    }
                    catch
                    {
                        ShowAlert(Constants.Error_Insert, Constants.Error_Insert);
                    }
                }
                else
                {
                    ShowAlert(Constants.Error_Add_Message, Constants.Error_Insert);
                    return;
                }
            }
            catch
            {
                ShowAlert(Constants.Error_Add_Message, Constants.Error_Insert);
            }
            return;
        }

    }
}
