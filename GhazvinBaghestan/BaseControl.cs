﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;


namespace GhazvinBaghestan
{
    public class BaseControl : System.Web.UI.Page
    {

        #region Properties


        protected int IdUser
        {
            get { return UIUtility.UserId; }
        }

        protected string UserName
        {
            get { return UIUtility.UserName; }
        }

        protected string _warnningMessage;
        protected string WarnningMessage
        {
            get { return _warnningMessage; }

            set { _warnningMessage = value; }
        }
        protected Int16 PageId { get; set; }


        protected string ApplicationPath
        {
            get
            {
                return UIUtility.ApplicationPath;
            }
        }

        protected List<Common.Entity.RoleAccess> ListAccessUser { get; set; }

        #endregion /Properties

        #region Methods

        
        protected void ShowAlert(string message, string title)
        {
            string strScript = "alert('" + message + "', '" + title + "', null);";
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", strScript, true);
        }

        protected void CheckUserAccess()
        {
            if (this.IdUser > 0)
            {
                if (this.Page.AppRelativeVirtualPath.LastIndexOf("~/") > -1)
                {
                    string strPageName = this.Page.AppRelativeVirtualPath.Substring(this.Page.AppRelativeVirtualPath.LastIndexOf("~/") + 2);
                    PageId = BLL.PageBLL.GetPageId(strPageName);
                    if (!BLL.RolesAccessBLL.HasPageAccess(this.IdUser, strPageName))
                    {
                        Response.Redirect(ApplicationPath + "AccessDeined.aspx");
                    }
                }
            }
            else
            {
                Response.Redirect(ApplicationPath + "AccessDeined.aspx");
            }
        }
        #endregion /Methods
    }
}