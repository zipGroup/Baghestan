﻿<%@ Page Language="C#" MasterPageFile="~/Web.Master" AutoEventWireup="true" CodeBehind="GardenList.aspx.cs" Inherits="GhazvinBaghestan.GardenList" %>

<asp:Content runat="server" ContentPlaceHolderID="PlaceHead">
    <meta charset="utf-8" />
    <title>لیست باغ ها</title>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="../assets/global/css/components-md-rtl.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="../assets/global/css/plugins-md-rtl.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <link href="assets/pages/css/Style.css" rel="stylesheet" />
    <!-- END HEAD -->
</asp:Content>


<asp:Content ContentPlaceHolderID="placeLine" runat="server">

    <li>
        <a href="home.aspx">خانه</a>
    </li>
    <li><a href="TreeList.aspx">لیست درختان</a></li>
    <li class="active">لیست باغ ها</li>
</asp:Content>

<asp:Content runat="server" ContentPlaceHolderID="PlaceContent">
    <div class="container-fluid">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-sunglo">
                    <span class="caption-subject bold uppercase">جستجو پیشرفته</span>
                </div>
            </div>
            <div class="portlet-body form">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-3 col-sm-3">
                            <asp:TextBox runat="server" ID="txtName" class="form-control form-control-solid placeholder-no-fix form-group" placeholder="نام باغ" />
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <asp:TextBox runat="server" ID="txtAreaFrom" class="form-control form-control-solid placeholder-no-fix form-group" placeholder="حداقل مساحت باغ" />
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <asp:TextBox runat="server" ID="txtAreaTo" class="form-control form-control-solid placeholder-no-fix form-group" placeholder="حداکثر مساحت باغ" />
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <asp:TextBox runat="server" ID="txtAgeFrom" class="form-control form-control-solid placeholder-no-fix form-group" placeholder="حداقل سن" />
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-3 col-sm-3">
                            <asp:TextBox runat="server" ID="txtAgeTo" class="form-control form-control-solid placeholder-no-fix form-group" placeholder="حداکثر سن" />
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <asp:TextBox runat="server" ID="txtLatitudeFrom" class="form-control form-control-solid placeholder-no-fix form-group" placeholder="حداقل عرض جغرافیایی" />
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <asp:TextBox runat="server" ID="txtLatitudeTo" class="form-control form-control-solid placeholder-no-fix form-group" placeholder="حداکثر عرض جغرافیایی" />
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <asp:TextBox runat="server" ID="txtLongitudeFrom" class="form-control form-control-solid placeholder-no-fix form-group" placeholder="حداقل طول جغرافیایی" />
                        </div>


                    </div>

                </div>



                <div class="row card">

                    <div class="col-md-12 col-sm-12">
                        <div class="col-md-3 col-sm-3">
                            <asp:TextBox runat="server" ID="txtLongitudeTo" class="form-control form-control-solid placeholder-no-fix form-group" placeholder="حداکثر طول جغرافیایی" />
                        </div>

                        <div class="col-md-9 col-sm-9">
                            <asp:Button runat="server" ID="btnSearch" OnClick="btnSearch_Click" Text="جستجو" class="btn blue" />
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption">
                        </div>
                        <div class="actions">
                            <a href="GardenAdd.aspx" class="btn btn-default btn-sm">
                                <i class="fa fa-plus"></i>اضافه کردن</a>
                            <button onclick="printDiv('sample_3');" class="btn btn-default btn-sm" id="mybtnGarden" aria-controls="example">
                                <i class="fa fa-print"></i>چاپ 
                            </button>
                        </div>
                    </div>
                    <table class='table table-bordered table-hover table-checkable order-column' id='sample_3'>
                        <div runat="server" id="tblResult">
                        </div>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>

</asp:Content>



<asp:Content ContentPlaceHolderID="PlaceScrips" runat="server">
    <!-- BEGIN CORE PLUGINS -->
    <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="../assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="../assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
    <script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="../assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->



    <script>
        function printDiv(divID) {
            var oldval = $('select[name=sample_3_length]').val();
            $('select[name=sample_3_length]').val('-1');
            $('select[name=sample_3_length]').trigger('change')

            var styles = "<link href='../assets/global/plugins/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css' />"
                + "<link href='../assets/global/plugins/simple-line-icons/simple-line-icons.min.css' rel='stylesheet' type='text/css' />"
                + "<link href='../assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css' rel='stylesheet' type='text/css' />"
                + "<link href='../assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css' rel='stylesheet' type='text/css' />"
                + "<link href='../assets/global/plugins/datatables/datatables.min.css' rel='stylesheet' type='text/css' />"
                + "<link href='../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap-rtl.css' rel='stylesheet' type='text/css' />"
                + "<link href='../assets/global/css/components-md-rtl.min.css' rel='stylesheet' id='style_components' type='text/css' />"
                + "<link href='../assets/global/css/plugins-md-rtl.min.css' rel='stylesheet' type='text/css' />"
                + "<link href='../assets/layouts/layout5/css/layout-rtl.min.css' rel='stylesheet' type='text/css' />"
                + "<link href='../assets/layouts/layout5/css/custom-rtl.min.css' rel='stylesheet' type='text/css' />"
                + "<link href='assets/pages/css/Style.css' rel='stylesheet' />";

            var printed = "<html dir='rtl'><head><title></title>" + styles + "</head>";
            printed += "<body style='background: #fff;'> <table class='table table-bordered table-hover table-checkable order-column' >" + document.getElementById(divID).innerHTML + "</table>" + "</body>"
            winPrint = window.open("");
            winPrint.document.write(printed);

            setTimeout(function () {
                print_page();
            }, 2000);

            function print_page() {
                winPrint.print();
                winPrint.close();
            }
            $('select[name=sample_3_length]').val(oldval);
            $('select[name=sample_3_length]').trigger('change')
        }
    </script>

</asp:Content>
