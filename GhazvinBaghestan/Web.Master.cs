﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GhazvinBaghestan
{
    public partial class Site1 : System.Web.UI.MasterPage
    {
        Boolean isFirst = true;
        private List<Common.Entity.Menu> UserMenus { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                lblname.InnerHtml = "خوش آمدید," + UIUtility.UserName;
                byte[] imagem = UIUtility.UserPhoto;
                string base64String = Convert.ToBase64String(imagem);
                imgUser.ImageUrl = String.Format("data:image/jpg;base64,{0}", base64String);
                this.SetMenu();

            }
            catch { }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            HttpContext.Current.Session.RemoveAll();
            Response.Redirect("Login.aspx");
        }


        private void SetMenu()
        {
            this.UserMenus = BLL.MenuBLL.GetList(UIUtility.UserId);

            string strMenu = "<ul class='nav navbar-nav'>";
            foreach (var mainMenu in this.UserMenus.Where(m => m.Parent == null).OrderBy(m => m.OrderNo))
            {
                strMenu += GetMenu(mainMenu);
            }
            strMenu += "</ul>";
            MenuContent.InnerHtml = strMenu;
        }

        private string GetMenu(Common.Entity.Menu menu)
        {
            string strMenu = string.Empty;

            if (menu.Page != null && menu.Page.PageId > 0)
            {
                strMenu += "<li> <a href = '" + UIUtility.ApplicationPath + menu.Page.Name + "'> " + menu.Title + "</a>";
            }
            else
            {

                if (menu.Parent != null && menu.Page == null)
                {
                    strMenu += "<li class='dropdown more-dropdown-sub'> <a href = 'javascript:;' > <i class='glyphicon glyphicon-list-alt'></i>" + menu.Title + " </a>" +"<ul class='dropdown-menu'>";
                }
                else
                {
                    if (isFirst)
                    {
                        strMenu += "<li class='dropdown dropdown-fw dropdown-fw-disabled  active open selected'> <a href = 'javascript:;' class='text-uppercase'> <i class='" + menu.Icon + "'></i>" + menu.Title + " </a> <ul class='dropdown-menu dropdown-menu-fw'> ";
                        isFirst = false;
                    }else
                    {
                        strMenu += "<li class='dropdown dropdown-fw dropdown-fw-disabled'> <a href = 'javascript:;' class='text-uppercase'> <i class='" + menu.Icon + "'></i>" + menu.Title + " </a> <ul class='dropdown-menu dropdown-menu-fw'> ";
                    }
                    
                }
                strMenu += GetSubMenus(menu);
            }
            strMenu += "</li>";

            return strMenu;
        }

        private string GetSubMenus(Common.Entity.Menu parentMenu)
        {

            string strMenu = string.Empty;
            var subMenus = this.UserMenus.Where(m => m.Parent != null &&
                                                     m.Parent.MenuId == parentMenu.MenuId).OrderBy(m => m.OrderNo).ToList();

            if (subMenus.Count > 0)
            {
                foreach (var subMenu in subMenus)
                {
                    strMenu += GetMenu(subMenu);
                }
                strMenu += "</ul>";
            }

            return strMenu;
        }
        
    }
}