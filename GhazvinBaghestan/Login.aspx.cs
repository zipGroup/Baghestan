﻿using Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GhazvinBaghestan
{
    public partial class Login : BaseControl
    {

        #region Event

        // Event declaration
        public event EventHandler LoginHandler;

        #endregion /Event

        protected void Page_Load(object sender, EventArgs e)
        {

        }
        
        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            if (BLL.LoginBLL.Login(txtUserName.Text, txtPasswordLogin.Text))
            {
                try
                {
                    String userName = txtUserName.Text;
                    String password = txtPasswordLogin.Text;
                    Common.Entity.User user = new Common.Entity.User()
                    {
                        UserName = userName,
                        Password = password
                    };
                    if (user.IsValid())
                    {
                        var oUser = BLL.UserBLL.GetRow(txtUserName.Text, txtPasswordLogin.Text);
                        var a = txtPasswordLogin.Text;
                        if (oUser.UserId > 0)
                        {
                            UIUtility.SetUserValues(oUser);
                            OnLoginHandler(new System.EventArgs());
                            Response.Redirect("Home.aspx");
                        }
                        else
                        {
                            ShowAlert(Constants.Error_InvalidUserNamePassword_Message, Constants.Error_Login);
                        }
                    }
                }
                catch { }

            }
        }

        protected virtual void OnLoginHandler(EventArgs e)
        {
            // Check if event is not null
            if (LoginHandler != null)
            {
                LoginHandler(this, e);
            }
        }
    }
}