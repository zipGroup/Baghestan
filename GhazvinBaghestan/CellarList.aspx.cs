﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GhazvinBaghestan
{
    public partial class CellarList : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckUserAccess();

            if (!IsPostBack)
            {
                try
                {
                    tblResult.InnerHtml = UIUtility.getTableFromEntity(BLL.CellarBLL.GetList(), typeof(Common.Entity.Cellar));
                }
                catch (HttpException exception)
                {
                    if (exception.ErrorCode == 401)
                        Response.Redirect("AccessDeined.aspx");
                }
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String name = txtCellarName.Text;
                int areaFrom = Int32.Parse(txtCellarAreaFrom.Text.Equals("") ? "-1" : txtCellarAreaFrom.Text);
                int areaTo = Int32.Parse(txtCellarAreaTo.Text.Equals("") ? "-1" : txtCellarAreaTo.Text);
                try
                {

                    tblResult.InnerHtml = UIUtility.getTableFromEntity(
                        BLL.CellarBLL.GetInfo(name,areaFrom,areaTo),
                        typeof(Common.Entity.Cellar));
                }
                catch
                {

                }
            }
            catch
            {

            }
        }
    }
}