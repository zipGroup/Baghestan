﻿using Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GhazvinBaghestan
{
    public partial class TreeAdd : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckUserAccess();

            this.fillGardenDropDown();
            this.fillProductDropDown();

        }

        private void fillProductDropDown()
        {
            ArrayList gardenList = BLL.ProductBLL.GetList();
            foreach (Common.Entity.Product nextProduct in gardenList)
            {
                ListItem foo = new ListItem(nextProduct.Name);
                foo.Value = nextProduct.ProductId.ToString();
                ddProduct.Items.Add(foo);
            }
        }

        private void fillGardenDropDown()
        {
            ArrayList gardenList = BLL.GardenBLL.GetList();
            foreach (Common.Entity.Garden nextGarden in gardenList)
            {
                ListItem foo = new ListItem(nextGarden.Name);
                foo.Value = nextGarden.GardenId.ToString();
                ddGarden.Items.Add(foo);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int age = Int32.Parse(txtAge.Text);
                int gardenId = Int32.Parse(ddGarden.SelectedValue);
                int productId = Int32.Parse(ddProduct.SelectedValue);
                String name = txtName.Text;
                String status = txtStatus.Text;
                String geneticInformation = txtGeneticInformation.Text;
                Common.Entity.Tree newTree = new Common.Entity.Tree(){
                    Age = age,
                    Garden = new Common.Entity.Garden()
                    {
                        GardenId = gardenId
                    },
                    Product = new Common.Entity.Product()
                    {
                        ProductId = productId
                    },
                    Name = name,
                    Status = status,
                    GeneticInformation = geneticInformation
                };
                if (newTree.IsValid())
                {

                    try
                    {
                        BLL.TreeBLL.Insert(newTree);
                        ShowAlert(Constants.Success_Add_Tree_Message, Constants.Success_Insert);
                    }
                    catch
                    {
                        ShowAlert(Constants.Error_Insert, Constants.Error_Insert);
                    }
                }else
                {
                    ShowAlert(Constants.Error_Add_Message, Constants.Error_Insert);
                    return;
                }
            }
            catch
            {
                ShowAlert(Constants.Error_Add_Message, Constants.Error_Insert);
            }
            return;
        }
    }

}