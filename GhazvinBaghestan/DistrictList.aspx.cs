﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GhazvinBaghestan
{
    public partial class DistrictList : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckUserAccess();
            if (!IsPostBack)
            {
                try
                {
                    tblResult.InnerHtml = UIUtility.getTableFromEntity(BLL.DistrictBLL.GetList(), typeof(Common.Entity.District));
                    this.fillChiefDropDown();
                    
                }
                catch (HttpException exception)
                {
                    if (exception.ErrorCode == 401)
                        Response.Redirect("AccessDeined.aspx");
                }
            }
        }

        private void fillChiefDropDown()
        {
            ListItem foo = new ListItem("انتخاب نام دخو");
            foo.Value = "-1";
            ddChief.Items.Add(foo);

            ArrayList PersonList = BLL.PersonBLL.GetList();
            foreach (Common.Entity.Person nextPerson in PersonList)
            {
                foo = new ListItem(nextPerson.LastName + " " + nextPerson.FirstName);
                foo.Value = nextPerson.SSN.ToString();
                ddChief.Items.Add(foo);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                //String name = txtNameDistrict.Text;
                //string currentStatus = txtCurrentStatusDistrict.Text;
                //string history = txtHistoryDistrict.Text;
                //bool isBlock = txtIsBlock;
                //double longitudeFrom = Double.Parse(txtLongitudeFromDistrict.Text.Equals("") ? "-1" : txtLongitudeFromDistrict.Text);
                //double longitudeTo = Double.Parse(txtLongitudeToDistrict.Text.Equals("") ? "-1" : txtLongitudeToDistrict.Text);
                //double latitudeFrom = Double.Parse(txtLatitudeFromDistrict.Text.Equals("") ? "-1" : txtLatitudeFromDistrict.Text);
                //double latitudeTo = Double.Parse(txtLatitudeToDistrict.Text.Equals("") ? "-1" : txtLatitudeToDistrict.Text);
                //string Chief = ddChief.Text;
                try
                {

                    //tblResult.InnerHtml = UIUtility.getTableFromEntity(BLL.GardenBLL.GetList(), typeof(Common.Entity.Garden));
                    //BLL.GardenBLL.GetInfo(name, areaFrom, areaTo, longitudeFrom, longitudeTo, latitudeFrom, latitudeTo, ageFrom
                    //    , ageTo, gardenTypeId, cellarId, ssnGardener, districtId);

                }
                catch
                {

                }
            }
            catch
            {

            }
        }
    }
}