﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GhazvinBaghestan
{
    public partial class rpt_GardenTree : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckUserAccess();

            if (!IsPostBack)
            {
                try
                {
                    tblResult.InnerHtml = UIUtility.getTableFromReport(BLL.ReportBLL.getTreeGardensList(), typeof(Common.Report.rpt_GardenTree));
                    this.fillGardenDropDown();
                }
                catch (HttpException exception)
                {
                    if (exception.ErrorCode == 401)
                        Response.Redirect("AccessDeined.aspx");
                }
            }
        }


        private void fillGardenDropDown()
        {

            ListItem foo = new ListItem("انتخاب نام باغ");
            foo.Value = "-1";
            ddGarden.Items.Add(foo);
            ArrayList gardenList = BLL.GardenBLL.GetList();
            foreach (Common.Entity.Garden nextGarden in gardenList)
            {
                foo = new ListItem(nextGarden.Name);
                foo.Value = nextGarden.GardenId.ToString();
                ddGarden.Items.Add(foo);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                int gardenId = Int32.Parse(ddGarden.SelectedItem.Value);
                int areaFrom = Int32.Parse(txtAgeFrom.Text.Equals("") ? "-1" : txtAgeFrom.Text);
                int areaTo = Int32.Parse(txtAgeTo.Text.Equals("") ? "-1" : txtAgeTo.Text);
                try
                {
                    tblResult.InnerHtml = UIUtility.getTableFromReport(
                        BLL.ReportBLL.getGardenTreeSearch(areaFrom, areaTo, gardenId),
                        typeof(Common.Entity.Tree));
                }
                catch
                {

                }
            }
            catch
            {

            }
        }
    }
}