﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GhazvinBaghestan
{
    public partial class TreeList : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckUserAccess();

            if (!IsPostBack)
            {
                try
                {
                    tblResult.InnerHtml = UIUtility.getTableFromEntity(BLL.TreeBLL.GetList(), typeof(Common.Entity.Tree));
                    this.fillGardenDropDown();
                    this.fillProductDropDown();
                }catch(HttpException exception)
                {
                    if(exception.ErrorCode == 401)
                    Response.Redirect("AccessDeined.aspx");
                }
            }
        }

        private void fillProductDropDown()
        {

            ListItem foo = new ListItem("انتخاب نام محصول");
            foo.Value = "-1";
            ddProduct.Items.Add(foo);

            ArrayList gardenList = BLL.ProductBLL.GetList();
            foreach (Common.Entity.Product nextProduct in gardenList)
            {
                foo = new ListItem(nextProduct.Name);
                foo.Value = nextProduct.ProductId.ToString();
                ddProduct.Items.Add(foo);
            }
        }

        private void fillGardenDropDown()
        {

            ListItem foo = new ListItem("انتخاب نام باغ");
            foo.Value = "-1";
            ddGarden.Items.Add(foo);
            ArrayList gardenList = BLL.GardenBLL.GetList();
            foreach (Common.Entity.Garden nextGarden in gardenList)
            {
                foo = new ListItem(nextGarden.Name);
                foo.Value = nextGarden.GardenId.ToString();
                ddGarden.Items.Add(foo);
            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
            String name = txtName.Text;
            String status = txtStatus.Text;
            String geneticInformation = txtGeneticInformation.Text;
            int gardenId = Int32.Parse(ddGarden.SelectedItem.Value);
            int productId = Int32.Parse(ddProduct.SelectedItem.Value);
            int ageFrom = Int32.Parse(txtAgeFrom.Text.Equals("") ? "-1" : txtAgeFrom.Text);
            int ageTo = Int32.Parse(txtAgeTo.Text.Equals("") ? "-1" : txtAgeTo.Text);
            try
            {

            tblResult.InnerHtml = UIUtility.getTableFromEntity(
                BLL.TreeBLL.GetInfo(ageFrom, ageTo, productId, geneticInformation, name, status, gardenId),
                typeof(Common.Entity.Tree));
                }
                catch
                {

                }
            }catch
            {

            }
        }
    }
}