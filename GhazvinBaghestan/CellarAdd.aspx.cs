﻿using Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GhazvinBaghestan
{
    public partial class CellarAdd : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CheckUserAccess();

            this.fillDisrictDropDown();

        }

        private void fillDisrictDropDown()
        {
            ArrayList gardenList = BLL.DistrictBLL.GetList();
            foreach (Common.Entity.District nextDistrict in gardenList)
            {
                ListItem foo = new ListItem(nextDistrict.Name);
                foo.Value = nextDistrict.DistrictId.ToString();
                ddDistrict.Items.Add(foo);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                int area = Int32.Parse(txtCellarArea.Text);
                String name = txtCellarName.Text;
                int districtId = Int32.Parse(ddDistrict.SelectedValue);
                Common.Entity.Cellar newCellar = new Common.Entity.Cellar()
                {
                    District = new Common.Entity.District()
                    {
                        DistrictId = districtId
                    },
                    Name = name,
                    Area = area
                };
                if (newCellar.IsValid())
                {

                    try
                    {
                        BLL.CellarBLL.Insert(newCellar);
                        ShowAlert(Constants.Success_Add_Tree_Message, Constants.Success_Insert);
                    }
                    catch
                    {
                        ShowAlert(Constants.Error_Insert, Constants.Error_Insert);
                    }
                }
                else
                {
                    ShowAlert(Constants.Error_Add_Message, Constants.Error_Insert);
                    return;
                }
            }
            catch
            {
                ShowAlert(Constants.Error_Add_Message, Constants.Error_Insert);
            }
            return;
        }
    }
}