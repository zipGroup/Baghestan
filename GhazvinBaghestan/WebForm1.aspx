﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="GhazvinBaghestan.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/OLMap/ol.js"></script>
    <link href="assets/global/plugins/OLMap/ol.css" rel="stylesheet" />
    <script src="assets/global/plugins/OLMap/bootstrap.min.js"></script>

    <style>
        #marker {
            width: 20px;
            height: 20px;
            border: 1px solid #088;
            border-radius: 10px;
            background-color: #0FF;
            opacity: 0.5;
        }

        #vienna {
            text-decoration: none;
            color: white;
            font-size: 11pt;
            font-weight: bold;
            text-shadow: black 0.1em 0.1em 0.2em;
        }

        .popover-content {
            min-width: 180px;
        }
    </style>
</head>
<body>
    <div id="map" class="map"></div>
    <div style="display: none;">
        <!-- Clickable label for Vienna -->
        <a class="overlay" id="vienna" target="_blank" href="http://en.wikipedia.org/wiki/Vienna">Vienna</a>
        <div id="marker" title="Marker"></div>
        <!-- Popup -->
        <div id='popup' title='Welcome to OpenLayers'></div>
    </div>
    <script>
        var layer = new ol.layer.Tile({
            source: new ol.source.OSM()
        });

        var map = new ol.Map({
            layers: [layer],
            target: 'map',
            view: new ol.View({
                center: ol.proj.fromLonLat([50.058147, 36.039455]),
                zoom: 15
            })
        });

        var pos = ol.proj.fromLonLat([50.058147, 36.039455]);

        // Vienna marker
        var marker = new ol.Overlay({
            position: pos,
            positioning: 'center-center',
            element: document.getElementById('marker'),
            stopEvent: false
        });
        map.addOverlay(marker);

        // Vienna label
        var vienna = new ol.Overlay({
            position: pos,
            element: document.getElementById('vienna')
        });
        map.addOverlay(vienna);

        // Popup showing the position the user clicked
        var popup = new ol.Overlay({
            element: document.getElementById('popup')
        });
        map.addOverlay(popup);

        map.on('click', function (evt) {
            var element = popup.getElement();
            var coordinate = evt.coordinate;
            var hdms = ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326');

            $(element).popover('destroy');
            popup.setPosition(coordinate);
            // the keys are quoted to prevent renaming in ADVANCED mode.
            $(element).popover({
                'placement': 'top',
                'animation': false,
                'html': true,
                'content': '<p>The location you clicked was:</p><code>' + hdms + '</code>'
            });
            $(element).popover('show');
        });
    </script>
</body>
</html>
