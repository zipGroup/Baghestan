﻿<%@ Page Language="C#" MasterPageFile="~/Web.Master" AutoEventWireup="true" CodeBehind="GardenLocations.aspx.cs" Inherits="GhazvinBaghestan.GardenLocations" %>

<asp:Content ContentPlaceHolderID="PlaceHead" runat="server">


    <!-- BEGIN CORE PLUGINS -->
    <script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="assets/layouts/layout5/scripts/layout.min.js" type="text/javascript"></script>
    <script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
    <script src="assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>

    <!-- BEGIN PAGE LEVEL SCRIPTS -->

    <script src="assets/global/plugins/OLMap/ol.js"></script>
    <link href="assets/global/plugins/OLMap/ol.css" rel="stylesheet" />
    <script src="assets/global/plugins/OLMap/bootstrap.min.js"></script>
    <!-- END PAGE LEVEL SCRIPTS -->

    <style>
        a.skiplink {
            position: absolute;
            clip: rect(1px, 1px, 1px, 1px);
            padding: 0;
            border: 0;
            height: 1px;
            width: 1px;
            overflow: hidden;
        }

            a.skiplink:focus {
                clip: auto;
                height: auto;
                width: auto;
                background-color: #fff;
                padding: 0.3em;
            }

        #map:focus {
            outline: #4A74A8 solid 0.15em;
        }

        #popup {
            margin-left: -110px;
        }
    </style>


</asp:Content>

<asp:Content ContentPlaceHolderID="PlaceContent" runat="server">
    <div class="container-fluid">
        <asp:HiddenField runat="server" ID="Lat" />
        <asp:HiddenField runat="server" ID="Lon" />
        <div id="map" class="map"></div>

        <div style='display: none;'>
            <div id='popup' style="padding: 0px">
                <div class='row'>
                    <div id='PlaceContent_form' class='col-md-9 '>
                        <div class='portlet light bordered'>
                            <div class='portlet-title'>
                                <div class='caption font-white-sunglo'>
                                    <div class='caption font-red-sunglo'>
                                        <span class='caption-subject bold uppercase'>اطلاعات باغ</span>
                                    </div>
                                </div>
                            </div>
                            <div class='portlet-body form'>
                                <div class='form-body' style='padding-bottom: 0px;'>
                                    <div class=' row form-group '>
                                        <div class='col-md-12 '>
                                            <label>نام</label>
                                            <asp:TextBox ID='txtName' type='text' class='form-control form-control-solid placeholder-no-fix form-group' placeholder='نام' runat='server' />
                                        </div>
                                    </div>
                                    <div class=' row form-group '>
                                        <div class='col-md-6 '>
                                            <label>سن</label>
                                            <asp:TextBox ID='txtAge' type='number' min="1" class='form-control form-control-solid placeholder-no-fix form-group' placeholder='وضعیت' runat='server' />
                                        </div>
                                        <div class='col-md-6'>
                                            <label>مساحت</label>
                                            <asp:TextBox runat='server' ID='txtArea' type='number' min="1" class='form-control form-control-solid placeholder-no-fix form-group' placeholder='سن' />
                                        </div>
                                    </div>
                                </div>
                                <div class='form-actions' style='padding-bottom: 0px; padding-top: 0px;'>
                                    <asp:Button ID='btnSubmit' type='submit' class='btn blue' Text='ثبت اطلاعات ' runat='server' OnClientClick="return EncryptPasswordLogin();" OnClick='btnSubmit_Click' />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div runat="server" id="dvmarkers"></div>
        </div>
        <div runat="server" id="dvScript"></div>
        <script>
            var popup = new ol.Overlay({
                element: document.getElementById('popup')
            });

            map.addOverlay(popup);
            var hdms;
            map.on('click', function (evt) {
                var element = popup.getElement();
                var coordinate = evt.coordinate;
                hdms = ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326');

                $(element).popover('destroy');
                popup.setPosition(coordinate);
                $(element).popover('show');
            });
        </script>
    </div>
</asp:Content>
<asp:Content ContentPlaceHolderID="PlaceScrips" runat="server">
    <script type="text/javascript">
            function EncryptPasswordLogin() {
                var latlon = ("" + hdms).split(',');
                document.getElementById('<%= Lat.ClientID %>').value = latlon[0];
                document.getElementById('<%= Lon.ClientID %>').value = latlon[1];
            }
    </script>
</asp:Content>
