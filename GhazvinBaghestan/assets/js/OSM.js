﻿
// Start position for the map (hardcoded here for simplicity)
var zoom = 14;
//complex object of type OpenLayers.Map
var map;
var vectorL;
var dragVectorC;
var point;
var points = new Array();
var pointsStreet = new Array();
var SelectedPoints = new Array();
var busStopIcon = 'Images/icoGarden.png';
var busStopDisabledIconSize = 'Images/BusStop_Disabled';
var busStopIconSize = 'Images/BusStop';
var busStopDisabledIcon = 'Images/BusStop_Disabled.png';
var counter = 0;
var x = {};
var lineLayer = {};
var line;
var Linemarker;
var markerF;
var lonLat;
var styleMap;
var selectedFeature;
var markerColor = "#5AF23F";
var markerLineWidth = 3;
var markerSize = 10;
var markerStyle = {
    fillColor: markerColor,
    strokeColor: markerColor,
    strokeWidth: markerLineWidth,
    graphicXOffset: -markerSize,
    graphicYOffset: -2 * markerSize
};

var randomStyle = {
    fillColor: "",
    strokeColor: "",
    strokeWidth: markerLineWidth,
    graphicXOffset: -markerSize,
    graphicYOffset: -2 * markerSize
};

String.prototype.endsWith = function (str) {
    return this.substr(-(str.length)) == str;
}

function addOrDelecteStreet(feature) {
    var point = new OpenLayers.Geometry.Point(feature.geometry.x, feature.geometry.y);
    if (!(isNaN(feature.geometry.x) && isNaN(feature.geometry.y))) {
        for (var i = 0; i < pointsStreet.length; i++) {
            //    if (feature.geometry.x == feature.attributes.x && feature.geometry.y == feature.attributes.y) {
            if (pointsStreet[i] == feature.attributes.lat) {
                pointsStreet.splice(i, 1);

                if (pointsStreet[i] == feature.attributes.lon) {
                    pointsStreet.splice(i, 1);

                }
                if (pointsStreet[i] == 0) {
                    pointsStreet.splice(i, 1);
                }
                else if (pointsStreet[i] == feature.attributes.streetLocationId) {
                    pointsStreet.splice(i, 1);
                }
                feature.destroy();
            }

        }
        SendPointStreet(pointsStreet);
    }
    // }
}

//Customize size PanZoomBar
function fixZoom() {
    $('.olControlPanZoomBar div').each(function (index, div) {
        var $div = $(div);
        //  console.log('id: ' + div.id);
        if (div.id.indexOf('PanZoomBar_ZoombarOpenLayers.Map_') != -1) {
            $div.css({ 'top': '103px', 'height': '56px' });
        }
        //  console.log(div.id.indexOf('_ZoombarOpenLayers'));
        if (div.id.indexOf('_ZoombarOpenLayers.Map') != -1) {
            $div.css({ 'top': '103px', 'height': '56px' });
        }
        if ((div.id.indexOf('OpenLayers.Control.PanZoomBar_3_zoomout') != -1)) {
            $div.css('top', '159px');
        }
        if ((div.id.indexOf('OpenLayers.Control.PanZoomBar_3_zoomin') != -1)) {
            $div.css('top', '86px');
        }
        if (div.id.endsWith('zoomin')) {
            $div.css('top', '85px');
        }
        if (div.id.endsWith('zoomout')) {
            $div.css('top', '159px');
        }
        $("#OpenLayers.Control.PanZoomBar_3_OpenLayers.Map_9_innerImage").css({ 'height': '9px', 'width': '20px', 'position': 'fixed' });
        fixSizeIcon(zoom);
    });
};

function fixSizeIcon(z) {
    if (typeof z == 'function') {
        return;
    }
    var feat = vectorL.features;
    for (var a = 0; a < feat.length; a++) {
        if (feat[a].data.icon == busStopDisabledIcon) {
            feat[a].data.icon = busStopDisabledIconSize + ".png";
            vectorL.features[a].attributes.icon = busStopDisabledIconSize + ".png";
        }

        if (SelectedPoints.length > 0) {
            for (var i = 0; i < SelectedPoints.length; i++) {
                if (feat[a].data.id == SelectedPoints[i]) {
                    feat[a].data.icon = busStopIcon;
                    vectorL.features[a].attributes.icon = busStopIcon;
                }
            }
        }

        switch (z) {
            case 12:
                vectorL.features[a].layer.styleMap.styles['default'].defaultStyle.graphicWidth = 10;
                vectorL.features[a].layer.styleMap.styles['default'].defaultStyle.graphicHeight = 11;
                break;
            case 13:
                vectorL.features[a].layer.styleMap.styles['default'].defaultStyle.graphicWidth = 15;
                vectorL.features[a].layer.styleMap.styles['default'].defaultStyle.graphicHeight = 17;
                break;
            case 14:
                vectorL.features[a].layer.styleMap.styles['default'].defaultStyle.graphicWidth = 20;
                vectorL.features[a].layer.styleMap.styles['default'].defaultStyle.graphicHeight = 23;
                break;
            case 15:
                vectorL.features[a].layer.styleMap.styles['default'].defaultStyle.graphicWidth = 25;
                vectorL.features[a].layer.styleMap.styles['default'].defaultStyle.graphicHeight = 29;
                break;
            case 16:
                vectorL.features[a].layer.styleMap.styles['default'].defaultStyle.graphicWidth = 32;
                vectorL.features[a].layer.styleMap.styles['default'].defaultStyle.graphicHeight = 37;
                break;
        }

    }
    vectorL.redraw();
    for (i = 0; i < counter; i++) {
        if (!lineLayer[i]) {
            if (lineLayer[i].features != null) {
                lineLayer[i].features[0].style = { fillColor: lineLayer[i].features[0].layer.features[0].layer.features[0].attributes.color,
                    strokeColor: lineLayer[i].features[0].layer.features[0].layer.features[0].attributes.color
                };
                lineLayer[i].redraw();
            }
        }
    }
}

//Draw Line between two points
function DrawPolyline(point) {
    if ((points.length == 0) && (SelectedPoints.length == 0)) {
        return;
    }
    if (markerF) {
        markerF.destroy();
    }
    line = new OpenLayers.Geometry.LineString(points);
    var p = point.clone();
    Linemarker = new OpenLayers.Geometry.Collection(new Array(line, p));
    markerF = new OpenLayers.Feature.Vector(line, null, markerStyle);
    vectorL.addFeatures(markerF);
}

function get_random_color() {

    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.round(Math.random() * 15)];
    }
    return color;
}

function addPoint(lat, lng) {
    var position = getPosition(lng, lat);
    point = new OpenLayers.Geometry.Point(position.lon, position.lat);
    return point;
}

//Draw Line between two points
function DrawPolylinePath(points, busId, Latitude, Longitude) {
    if ((points.length == 0)) {
        return;
    }
    line = new OpenLayers.Geometry.LineString(points);
    var p = point.clone();
    Linemarker = new OpenLayers.Geometry.Collection(new Array(line, p));
    counter = counter + 1;
    var color = get_random_color();
    randomStyle.fillColor = color;
    randomStyle.strokeColor = color;

    //add New Layer
    lineLayer[counter] = new OpenLayers.Layer.Vector("Line Layer_" + counter + "");
    map.addLayer(lineLayer[counter]);


    //add New Feature
    x[counter] = new OpenLayers.Feature.Vector(Linemarker, null, randomStyle);
    x[counter].attributes = {
        counter: "test_" + counter,
        color: color,
        busId: busId
    }
    lineLayer[counter].addFeatures(x[counter]);

    //add point
    vectorL.addFeatures([new OpenLayers.Feature.Vector(addPoint(Latitude, Longitude), { name: name, busId: busId })]);
    feature.id = busId;
    lineLayer[counter].addFeatures(feature);
    lineLayer[counter].redraw();
}



//For show icon points in time draw polyline
function firstLoadPloyline() {
    var feat = vectorL.features;
    for (var a = 0; a < feat.length; a++) {
        for (var i = 0; i < SelectedPoints.length; i++) {

            if (feat[a].data.id == SelectedPoints[i]) {
                feat[a].data.icon = busStopIcon;
                vectorL.features[a].attributes.icon = busStopIcon;
            }

        }
    }
    // for refresh vector layer
    vectorL.redraw();
}


//refresh table stops
function refresh() {
    $('#sortable li:even').removeClass('GridCell GridRowStyle').addClass('GridCell GridAlternateRowStyle');
    $('#sortable li:odd').removeClass('GridCell GridAlternateRowStyle').addClass('GridCell GridRowStyle');

    $('#Order tr td:even').removeClass('GridCell GridRowStyle').addClass('GridCell GridAlternateRowStyle');
    $('#Order tr td:odd').removeClass('GridCell GridAlternateRowStyle').addClass('GridCell GridRowStyle');
}

//Initialise the 'map' object
function init() {
    var proj = new OpenLayers.Projection("EPSG:4326");
    var mapProj = new OpenLayers.Projection("EPSG:900913");

    var fullExtent = new OpenLayers.Bounds.fromArray(mapBounds).transform(proj, mapProj);
    map = new OpenLayers.Map("map", {
        controls: [
                    new OpenLayers.Control.Navigation(),
                    new OpenLayers.Control.PanZoomBar()
                  ],
        maxExtent: fullExtent,
        //      restrictedExtent: fullExtent,
        maxResolution: 156543.0339,
        numZoomLevels: 5,
        minZoomLevel: 12,
        maxZoomLevel: 16,
        units: 'm',
        projection: mapProj,
        displayProjection: proj
    });

    map.zoomIn = function (z) {
        var z = map.getZoom(); // current zoom
        switch (z) {
            case 12:
                z = 13;
                fixSizeIcon(z);
                map.zoomTo(z);
                break;
            case 13:
                z = 14;
                fixSizeIcon(z);
                map.zoomTo(z);
                break;
            case 14:
                z = 15;
                fixSizeIcon(z);
                map.zoomTo(z);
                break;
            case 15:
                z = 16;
                fixSizeIcon(z);
                map.zoomTo(z);
                break;
            case 16:
                z = 16;
                fixSizeIcon(z);
                map.zoomTo(z);
                break;
        }
    }

    map.zoomOut = function () {
        var z = map.getZoom(); // current zoom
        switch (z) {
            case 12:
                z = 12;
                fixSizeIcon(z);
                map.zoomTo(z);
                break;
            case 13:
                z = 12;
                fixSizeIcon(z);
                map.zoomTo(z);
                break;
            case 14:
                z = 13;
                fixSizeIcon(z);
                map.zoomTo(z);
                break;
            case 15:
                z = 14;
                fixSizeIcon(z);
                map.zoomTo(z);
                break;
            case 16:
                z = 15;
                fixSizeIcon(z);
                map.zoomTo(z);
                break;
        }
    }

    map.isValidZoomLevel = function (zoomLevel) {
        if (zoomLevel < 12 || zoomLevel > 16) {
            return;
        }
        zoom = zoomLevel;
        $.JSONCookie('z', { name: zoom }, { path: '/' });
        fixSizeIcon(zoomLevel);
        return (zoomLevel != null) && (zoomLevel >= 12 && zoomLevel <= 16);
    }

    map.zoomTo = function (a) {
        var proj = new OpenLayers.Projection("EPSG:4326");
        var mapProj = new OpenLayers.Projection("EPSG:900913");

        var fullExtent = new OpenLayers.Bounds.fromArray(mapBounds).transform(proj, mapProj);
//        map.restrictedExtent = fullExtent;

        zoom = a;
        if (a < 12) {
            a = 12;
        }
        if (this.isValidZoomLevel(a)) {
            this.setCenter(null, a);
            fixSizeIcon(a);
        }
        else {
            if (a > map.zoom) {
                map.zoomIn();
                zoom = a;
            } else {
                map.zoomOut();
                zoom = a;
            }
        }
    }

    layerMapnik = new OpenLayers.Layer.OSM.Mapnik("Mapnik");
    layerMapnik.setOpacity(1);
    map.addLayer(layerMapnik);

    layerCycleMap = new OpenLayers.Layer.OSM.CycleMap("CycleMap");
    layerCycleMap.setOpacity(1);
    map.addLayer(layerCycleMap);


    //add Vector Layer to map
    var style = OpenLayers.Util.applyDefaults(
                    {
                        externalGraphic: "${icon}",
                        pointRadius: 15,
                        graphicWidth: 31,
                        graphicHeight: 31,
                        graphicOpacity: 1
                    }, OpenLayers.Feature.Vector.style["default"]);


    styleMap = new OpenLayers.StyleMap({ "default": style
    });

    vectorL = new OpenLayers.Layer.Vector("Vector Layer", { styleMap: styleMap });
    map.addLayer(vectorL);


    //Add drag to Vector Layer map
    dragVectorC = new OpenLayers.Control.DragFeature(vectorL);
    map.addControl(dragVectorC);


    // This is the layer that uses the locally stored tiles
    var newLayer = new OpenLayers.Layer.OSM("Local Tiles", "tiles/gm_${x}_${y}_${z}.png", { numZoomLevels: 5, zoomOffset: 13, alpha: true, isBaseLayer: false });
    map.addLayer(newLayer);

    //Add a selector control to the vectorLayer with popup functions

    var controls = {
        selector: new OpenLayers.Control.SelectFeature(vectorL, {
            hover: true, highlightOnly: true, renderIntent: "temporary",
            eventListeners: {
                featurehighlighted: tooltipSelect,
                featureunhighlighted: tooltipUnselect
            },
            callbacks: {
                click: function (f) {
                    selectMarker(f);
                }

            }
        })
    };
    map.addControl(controls['selector']);
    controls['selector'].activate();

    function tooltipSelect(event) {
        var feature = event.feature;
        if (feature.attributes.name == undefined) {
            return;
        }
        var selectedFeature = feature;
        //if there is already an opened details window, don\'t draw the      tooltip
        if (feature.popup != null) {
            return;
        }
        //if there are other tooltips active, destroy them
        if (tooltipPopup != null) {
            map.removePopup(tooltipPopup);
            tooltipPopup.destroy();
            if (lastFeature != null) {
                delete lastFeature.popup;
                tooltipPopup = null;
            }
        }
        lastFeature = feature;
        var tooltipPopup = new OpenLayers.Popup("activetooltip",
                    feature.geometry.getBounds().getCenterLonLat(),
                    null,
                    '<div style="padding-right:12px;text-align:right;">' + feature.attributes.name + '</div>'
                    , true);
        //this is messy, but I'm not a CSS guru
        tooltipPopup.contentDiv.style.backgroundColor = '#ffecb5';
        tooltipPopup.backgroundColor = '#ffecb5';
        tooltipPopup.border = '1px solid #000';
        tooltipPopup.contentDiv.style.overflow = 'hidden';
        tooltipPopup.contentDiv.style.padding = '3px';
        tooltipPopup.contentDiv.style.margin = '0';
        tooltipPopup.contentDiv.style.direction = 'rtl';
        tooltipPopup.autoSize = true;
        feature.popup = tooltipPopup;
        map.addPopup(tooltipPopup);
    }

    function tooltipUnselect(event) {
        var feature = event.feature;
        if (feature != null && feature.popup != null) {
            map.removePopup(feature.popup);
            feature.popup.destroy();
            delete feature.popup;
            tooltipPopup = null;
            lastFeature = null;
            while (map.popups.length) {
                map.removePopup(map.popups[0]);
            }
        }
    }

    function onFeatureSelect(feature) {
        selectedFeature = feature;
        popup = new OpenLayers.Popup.FramedCloud("chicken",
        feature.geometry.getBounds().getCenterLonLat(),
        new OpenLayers.Size(400, 200), '<div style="padding-right:12px;width:200px;text-align:right;"> نام ايستگاه : ' + feature.data.name + '<br/> کد ايستگاه : ' + feature.data.id + '</div>', null, true);
        popup.panMapIfOutOfView = true;
        popup.autoSize = true;
        feature.popup = popup;
        map.addPopup(popup);
    }
    function onFeatureUnselect(feature) {
        map.removePopup(feature.popup);
        feature.popup.destroy();
        feature.popup = null;
    }

    //Show position new marker
    var showPositonMarker = new OpenLayers.Control.MousePosition({
        div: document.getElementById('showPositonMarker')
    });
    map.addControl(showPositonMarker);

    //refresh column order table stop
    function refreshTable() {
        if (points.length == 0) {
            $("#Order").empty();
            $("#sortable").empty();
        }
        $("#Order").empty();
        for (var i = 0; i < points.length; i++) {
            if (i % 2 == 0) {
                $("#Order").append('<tr style="height:29px"><td class="GridCell GridRowStyle">' + (i + 1) + '</td></tr>');
            }
            else {

                $("#Order").append('<tr style="height:29px"><td class="GridCell GridAlternateRowStyle">' + (i + 1) + '</td></tr>');
            }
        }
    }

    //Click point 
    function selectMarker(feature) {
        if (feature.data.streetId != null) {
            addOrDelecteStreet(feature);
            return;
        }
        if (feature.data.id == null) {
            return;
        }
        var point = new OpenLayers.Geometry.Point(feature.geometry.x, feature.geometry.y);
        if (!(isNaN(feature.geometry.x) && isNaN(feature.geometry.y))) {
            for (var i = 0; i < points.length; i++) {
                if (feature.geometry.x == points[i].x && feature.geometry.y == points[i].y) {
                    feature.attributes.icon = busStopDisabledIcon;
                    SelectedPoints.splice(i, 1);
                    points.splice(i, 1);
                    DrawPolyline(point);
                    selectPoints();
                    if ($('#sortable li')[i].id == '' + feature.data.id + '') {
                        var child = document.getElementById(feature.data.id);
                        var parent = document.getElementById('sortable');
                        parent.removeChild(child);
                    }
                    refreshTable();
                    refresh();
                    vectorL.redraw();
                    return;
                }
            }
            feature.attributes.icon = busStopIcon;
            SelectedPoints.push(feature.attributes.id);
            points.push(point);
            point.id = feature.data.id;
            DrawPolyline(point);
            selectPoints();
            $("#sortable").append('<li class="GridCell GridRowStyle" id=' + feature.data.id + ' lat=' + feature.data.lat + ' log =' + feature.data.lng + '> ' + feature.data.name + '</li>');
            $("#Order").append('<tr  style="height:29px"><td class="GridCell GridRowStyle">' + (points.length) + '</td></tr>');
            refresh();
            vectorL.redraw();
        }
    }

}


