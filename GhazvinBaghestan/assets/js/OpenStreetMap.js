/**
* Namespace: Util.OSM
*/
OpenLayers.Util.OSM = {};

/**
* Constant: MISSING_TILE_URL
* {String} URL of image to display for missing tiles
*/
OpenLayers.Util.OSM.MISSING_TILE_URL = "img/no-tile-image.png";

/**
* Property: originalOnImageLoadError
* {Function} Original onImageLoadError function.
*/
OpenLayers.Util.OSM.originalOnImageLoadError = OpenLayers.Util.onImageLoadError;

/**
* Function: onImageLoadError
*/
OpenLayers.Util.onImageLoadError = function () {
    if (this.src.match(/^tiles\//)) {
        this.src = OpenLayers.Util.OSM.MISSING_TILE_URL;
    } else if (this.src.match(/^http:\/\/[def]\.tah\.openstreetmap\.org\//)) {
        // do nothing - this layer is transparent
    } else {
        OpenLayers.Util.OSM.originalOnImageLoadError;
    }
};

var cityCode = '';
var mapBounds = '';
var busStopIcon = 'Images/BusStop.png';
var busStopDisabledIcon = 'Images/BusStop_Disabled.png';
var busIcon = 'Images/BusMarker.png';
var busTimeOutIcon = 'Images/BusTimeOutMarker.png';
var markerStreet = 'Images/marker-blue.png';
var busPoints = [];
var showFullScreen = false;

function SetMapCity(code, westBound, southBound, eastBound, northBound) {
    cityCode = code;
    mapBounds = [westBound, southBound, eastBound, northBound];
}

function clearMap() {
    vectorL.removeFeatures(vectorL.features);
}

function addPoint(lat, lng) {
    var position = getPosition(lng, lat);
    point = new OpenLayers.Geometry.Point(position.lon, position.lat);
    return point;
}

function addStop(lat, lng) {
    vectorL.removeFeatures(vectorL.features);
    vectorL.addFeatures([new OpenLayers.Feature.Vector(addPoint(lat, lng), { icon: busStopIcon })]);
    dragVectorC.activate();
}

function addBus(lat, lng) {
    vectorL.removeFeatures(vectorL.features);
    vectorL.addFeatures([new OpenLayers.Feature.Vector(addPoint(lat, lng))]);
}

function addStreet(id, lat, lng) {
    vectorL.addFeatures([new OpenLayers.Feature.Vector(addPoint(lat, lng), { streetId: id, icon: markerStreet, lat: lat, lon: lng })]);
}

function addFixedStop(lat, lng, name) {
    vectorL.addFeatures([new OpenLayers.Feature.Vector(addPoint(lat, lng), { name: name, icon: busStopIcon })]);
}

function addDisabledStop(id, lat, lng, name) {
    vectorL.addFeatures([new OpenLayers.Feature.Vector(addPoint(lat, lng), { id: id, name: name, icon: busStopDisabledIcon })]);
}

function addLineStop(id, lat, lng) {
    points.push(addPoint(lat, lng));
    SelectedPoints.push(id);
}

function showBus(id, lat, lng, name, hasTimeOut) {
    //    var position = getPosition(lng, lat);
    if (vectorL.getFeatureById(id) != null) {
        var feature = vectorL.getFeatureById(id);
        //        if (feature.geometry.x == position.lon &&
        //            feature.geometry.y == position.lat) {
        //            feature.data.name = name;
        //            return;
        //        }
        vectorL.removeFeatures(feature);
    }
    var feature = new OpenLayers.Feature.Vector(addPoint(lat, lng), { name: name, icon: (!hasTimeOut ? busIcon : busTimeOutIcon) });
    feature.id = id;
    vectorL.addFeatures(feature);
}

function getPosition(lng, lat) {
    return new OpenLayers.LonLat(lng, lat).transform(new OpenLayers.Projection('EPSG:4326'), map.getProjectionObject());
}

function setMapFullScreen() {
    $(".map").append("<a onclick='setMapNormalScreen();' href='#' id='lnkExitFullScreen'>خروج از نمایش تمام صفحه</a>");
    $('.map').css({ 'width': '' + $(window).width() + 'px', 'height': '' + ($(window).height() + 50) + 'px', 'top': '0', 'left': '0', 'right': '0', 'bottom': '0', 'background-color': '#EEE', 'z-index': '1000' });
    $('#map').css({ 'position': 'fixed', 'width': '' + $(window).width() + 'px', 'height': '' + ($(window).height() + 50) + 'px', 'top': '0', 'left': '0', 'right': '0', 'bottom': '0', 'background-color': '#EEE', 'z-index': '1000' });
}

function setMapNormalScreen() {
    $('.map').css({ 'width': '100%', 'height': '400px', 'z-index': '-1' });
    $('#map').css({ 'position': 'static', 'width': '100%', 'height': '400px', 'z-index': '-1', 'margin': '0 auto' });
    $('.map').find('a').remove();
}

function drawBusLines(busPoints) {

    var counter = 0;
    while (typeof vectorL.getFeatureById('busLine' + counter) != 'undefined' &&
           vectorL.getFeatureById('busLine' + counter) != null) {
        vectorL.removeFeatures(vectorL.getFeatureById('busLine' + counter));
        counter++;
    }

    if (busPoints.length > 1) {
        for (var i = 0; i < busPoints.length; i++) {
            line = new OpenLayers.Geometry.LineString(busPoints);
            Linemarker = new OpenLayers.Geometry.Collection(new Array(line, busPoints[i].clone()));
            markerF = new OpenLayers.Feature.Vector(Linemarker, null, markerStyle);
            markerF.id = 'busLine' + i;
            vectorL.addFeatures(markerF);
        }
    }
}

/**
* Class: OpenLayers.Layer.OSM.Mapnik
*
* Inherits from:
*  - <OpenLayers.Layer.OSM>
*/
OpenLayers.Layer.OSM.Mapnik = OpenLayers.Class(OpenLayers.Layer.OSM, {
    /**
    * Constructor: OpenLayers.Layer.OSM.Mapnik
    *
    * Parameters:
    * name - {String}
    * options - {Object} Hashtable of extra options to tag onto the layer
    */
    initialize: function (name, options) {
        var url = [
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png",
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png",
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png"
        ];
        options = OpenLayers.Util.extend({ numZoomLevels: 19, buffer: 0 }, options);
        var newArguments = [name, url, options];
        OpenLayers.Layer.OSM.prototype.initialize.apply(this, newArguments);
    },

    CLASS_NAME: "OpenLayers.Layer.OSM.Mapnik"
});

/**
* Class: OpenLayers.Layer.OSM.MapQuestOpen
*
* Inherits from:
*  - <OpenLayers.Layer.OSM>
*/
OpenLayers.Layer.OSM.MapQuestOpen = OpenLayers.Class(OpenLayers.Layer.OSM, {
    /**
    * Constructor: OpenLayers.Layer.OSM.MapQuestOpen
    *
    * Parameters:
    * name - {String}
    * options - {Object} Hashtable of extra options to tag onto the layer
    */
    initialize: function (name, options) {
        var url = [
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png",
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png",
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png"
        ];
        options = OpenLayers.Util.extend({ numZoomLevels: 19, buffer: 0 }, options);
        var newArguments = [name, url, options];
        OpenLayers.Layer.OSM.prototype.initialize.apply(this, newArguments);
    },

    CLASS_NAME: "OpenLayers.Layer.OSM.MapQuestOpen"
});

/**
* Class: OpenLayers.Layer.OSM.Osmarender
*
* Inherits from:
*  - <OpenLayers.Layer.OSM>
*/
OpenLayers.Layer.OSM.Osmarender = OpenLayers.Class(OpenLayers.Layer.OSM, {
    /**
    * Constructor: OpenLayers.Layer.OSM.Osmarender
    *
    * Parameters:
    * name - {String}
    * options - {Object} Hashtable of extra options to tag onto the layer
    */
    initialize: function (name, options) {
        var url = [
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png",
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png",
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png"
        ];
        options = OpenLayers.Util.extend({ numZoomLevels: 18, buffer: 0 }, options);
        var newArguments = [name, url, options];
        OpenLayers.Layer.OSM.prototype.initialize.apply(this, newArguments);
    },

    CLASS_NAME: "OpenLayers.Layer.OSM.Osmarender"
});

/**
* Class: OpenLayers.Layer.OSM.CycleMap
*
* Inherits from:
*  - <OpenLayers.Layer.OSM>
*/
OpenLayers.Layer.OSM.CycleMap = OpenLayers.Class(OpenLayers.Layer.OSM, {

    /**
    * Constructor: OpenLayers.Layer.OSM.CycleMap
    *
    * Parameters:
    * name - {String}
    * options - {Object} Hashtable of extra options to tag onto the layer
    */
    initialize: function (name, options) {
        var url = [
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png",
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png",
            "Images/Maps/" + cityCode + "/${z}/gm_${x}_${y}_${z}.png"
        ];
        options = OpenLayers.Util.extend({ numZoomLevels: 19, buffer: 0 }, options);
        var newArguments = [name, url, options];
        OpenLayers.Layer.OSM.prototype.initialize.apply(this, newArguments);
    },

    CLASS_NAME: "OpenLayers.Layer.OSM.CycleMap"
});
