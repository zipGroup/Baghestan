﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace GhazvinBaghestan
{
    public partial class GardenList : BaseControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            CheckUserAccess();

            if (!IsPostBack)
            {
                try
                {
                    tblResult.InnerHtml = UIUtility.getTableFromEntity(BLL.GardenBLL.GetList(), typeof(Common.Entity.Garden));
                }
                catch (HttpException exception)
                {
                    if (exception.ErrorCode == 401)
                        Response.Redirect("AccessDeined.aspx");
                }
            }
        }
        


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                String name = txtName.Text;
                int areaFrom = Int32.Parse(txtAreaFrom.Text.Equals("") ? "-1" : txtAreaFrom.Text);
                int areaTo = Int32.Parse(txtAreaTo.Text.Equals("") ? "-1" : txtAreaTo.Text);
                int ageFrom = Int32.Parse(txtAgeFrom.Text.Equals("") ? "-1" : txtAgeFrom.Text);
                int ageTo = Int32.Parse(txtAgeTo.Text.Equals("") ? "-1" : txtAgeTo.Text);
                double longitudeFrom = Double.Parse(txtLongitudeFrom.Text.Equals("") ? "-1" : txtLongitudeFrom.Text);
                double longitudeTo = Double.Parse(txtLongitudeTo.Text.Equals("") ? "-1" : txtLongitudeTo.Text);
                double latitudeFrom = Double.Parse(txtLatitudeFrom.Text.Equals("") ? "-1" : txtLatitudeFrom.Text);
                double latitudeTo = Double.Parse(txtLatitudeTo.Text.Equals("") ? "-1" : txtLatitudeTo.Text);
                try
                {

                    tblResult.InnerHtml = UIUtility.getTableFromEntity(BLL.GardenBLL.GetInfo(name, areaFrom, areaTo, longitudeFrom, longitudeTo, latitudeFrom, latitudeTo, ageFrom
                        , ageTo), typeof(Common.Entity.Garden));

                }
                catch
                {

                }
            }
            catch
            {

            }
        }
    }
}