﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.Data;
using System.Collections;
using System.Reflection;

namespace GhazvinBaghestan
{
    public static class UIUtility
    {


        public static string ApplicationPath
        {
            get
            {
                return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath;
            }
        }

        public static int UserId
        {
            get
            {
                if (HttpContext.Current.Session["IdUser"] == null)
                {
                    return -1;
                }
                return int.Parse(HttpContext.Current.Session["IdUser"].ToString());
            }
            private set
            {
                HttpContext.Current.Session["IdUser"] = value;
            }
        }


        public static Byte[] UserPhoto
        {
            get
            {
                if (HttpContext.Current.Session["UserPhoto"] == null)
                {
                    return null;
                }
                return (byte[])HttpContext.Current.Session["UserPhoto"];
            }
            private set
            {
                HttpContext.Current.Session["UserPhoto"] = value;
            }
        }

        public static string UserName
        {
            get
            {
                if (HttpContext.Current.Session["UserName"] == null)
                {
                    HttpContext.Current.Session["UserName"] = string.Empty;
                }
                return HttpContext.Current.Session["UserName"].ToString();
            }
            private set { HttpContext.Current.Session["UserName"] = value; }
        }

        public static void SetUserValues(Common.Entity.User user)
        {
            UserId = user.UserId;
            UserName = user.UserName;
            UserPhoto = user.Photo;
        }


        public static String getTableFromReport(ArrayList list, Type classType)
        {
            String result = String.Empty;
            PropertyInfo[] properties = classType.GetProperties();
            result += " <thead> <tr> ";
            result += "<th class='table-checkbox'> <label class='mt-checkbox mt-checkbox-single mt-checkbox-outline'> <input type='checkbox' class='group-checkable' data-set='#sample_3 .checkboxes' /> <span></span></label> </th> ";

            foreach (var prop in properties)
            {
                if (prop.GetCustomAttributes(typeof(Common.Display), false).Length > 0)
                {
                    string strDisPlayName = (prop.GetCustomAttributes(typeof(Common.Display), false).Single() as Common.Display).Name;
                    result += "<th> " + strDisPlayName + "</th>";
                }
            }
            result += "</tr> </thead>";
            result += "<tbody>";
            foreach (var entity in list)
            {
                result += "<tr class=''> <td> <label class='mt-checkbox mt-checkbox-single mt-checkbox-outline'> <input type='checkbox' class='checkboxes' value='1' /> <span></span> </label> </td>";

                foreach (var prop in properties)
                {
                    if (prop.GetCustomAttributes(typeof(Common.Display), false).Length > 0)
                    {
                        if (prop.GetValue(entity) != null)
                        {
                            result += "<td>" + prop.GetValue(entity).ToString() + "</td>";
                        }
                        else
                        {
                            result += "<td>" + "   " + "</td>";
                        }
                    }
                }
                result += "</tr>";
            }
            result += "</tbody>";

            return result;
        }

        public static String getTableFromEntity(ArrayList list, Type classType)
        {
            String result = String.Empty;
            PropertyInfo[] properties = classType.GetProperties();
            result += " <thead> <tr> ";
            result += "<th class='table-checkbox'> <label class='mt-checkbox mt-checkbox-single mt-checkbox-outline'> <input type='checkbox' class='group-checkable' data-set='#sample_3 .checkboxes' /> <span></span></label> </th> ";

            foreach (var prop in properties)
            {
                if (prop.PropertyType.BaseType != typeof(Common.Entity.BaseEntity))
                {
                    if (prop.GetCustomAttributes(typeof(Common.Display), false).Length > 0)
                    {
                        string strDisPlayName = (prop.GetCustomAttributes(typeof(Common.Display), false).Single() as Common.Display).Name;
                        result += "<th> " + strDisPlayName + "</th>";
                    }
                }
            }
            result += "</tr> </thead>";
            result += "<tbody>";
            foreach (var entity in list)
            {
                result += "<tr class=''> <td> <label class='mt-checkbox mt-checkbox-single mt-checkbox-outline'> <input type='checkbox' class='checkboxes' value='1' /> <span></span> </label> </td>";

                foreach (var prop in properties)
                {
                    if (prop.PropertyType.BaseType != typeof(Common.Entity.BaseEntity))
                    {
                        if (prop.GetCustomAttributes(typeof(Common.Display), false).Length > 0)
                        {
                            if (prop.GetValue(entity) != null)
                            {
                                if (prop.GetCustomAttributes(typeof(Common.IsIdentity), false).Length > 0)
                                {
                                    result += "<td><a href='" + classType.Name + "Edit.aspx?id=" + prop.GetValue(entity).ToString() + "'>" + prop.GetValue(entity).ToString() + "</td>";
                                }
                                else
                                {
                                    result += "<td>" + prop.GetValue(entity).ToString() + "</td>";
                                }
                            }
                            else
                            {
                                result += "<td>" + "   " + "</td>";
                            }
                        }
                    }
                }
                result += "</tr>";
            }
            result += "</tbody>";

            return result;
        }

    }
}