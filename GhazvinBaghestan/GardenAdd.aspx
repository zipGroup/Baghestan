﻿<%@ Page Language='C#' UnobtrusiveValidationMode="None" MasterPageFile='~/Web.Master' AutoEventWireup='true' CodeBehind='GardenAdd.aspx.cs' Inherits='GhazvinBaghestan.GardenAdd' %>


<asp:Content ContentPlaceHolderID='PlaceHead' runat='server'>

    <meta charset='utf-8' />
    <title>Metronic Admin RTL Theme #5 | Bootstrap Form Controls</title>
    <meta http-equiv='X-UA-Compatible' content='IE=edge' />
    <meta content='width=device-width, initial-scale=1' name='viewport' />
    <meta content='Preview page of Metronic Admin RTL Theme #5 for bootstrap inputs, input groups, custom checkboxes and radio controls and more' name='description' />
    <meta content='' name='author' />
    <!-- BEGIN LAYOUT FIRST STYLES -->
    <!-- END LAYOUT FIRST STYLES -->
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href='../assets/global/plugins/font-awesome/css/font-awesome.min.css' rel='stylesheet' type='text/css' />
    <link href='../assets/global/plugins/simple-line-icons/simple-line-icons.min.css' rel='stylesheet' type='text/css' />
    <link href='../assets/global/plugins/bootstrap/css/bootstrap-rtl.min.css' rel='stylesheet' type='text/css' />
    <link href='../assets/global/plugins/bootstrap-switch/css/bootstrap-switch-rtl.min.css' rel='stylesheet' type='text/css' />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href='../assets/global/css/components-md-rtl.min.css' rel='stylesheet' id='style_components' type='text/css' />
    <link href='../assets/global/css/plugins-md-rtl.min.css' rel='stylesheet' type='text/css' />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href='../assets/layouts/layout5/css/layout-rtl.min.css' rel='stylesheet' type='text/css' />
    <link href='../assets/layouts/layout5/css/custom-rtl.min.css' rel='stylesheet' type='text/css' />
    <!-- END THEME LAYOUT STYLES -->


</asp:Content>

<asp:Content ContentPlaceHolderID='placeLine' runat='server'>

    <li>
        <a href='home.aspx'>خانه</a>
    </li>
    <li class='active'>لیست درختان</li>
</asp:Content>


<asp:Content ContentPlaceHolderID='PlaceContent' runat='server'>

    <div class='container-fluid'>
        <div class='row'>
            <div id='PlaceContent_form' class='col-md-12 '>
                <div class='portlet light bordered'>
                    <div class='portlet-title'>
                        <div class='caption font-white-sunglo'>
                            <div class='caption font-red-sunglo'>
                                <span class='caption-subject bold uppercase'>اطلاعات باغ جدید</span>
                            </div>
                        </div>
                    </div>
                    <div class='portlet-body form'>
                        <div class='form-body' style='padding-bottom: 0px;'>
                            <div class=' row form-group '>
                                <div class='col-md-4 '>
                                    <label>نام</label>
                                    <asp:TextBox ID='txtNameGarden' type='text' class='form-control form-control-solid placeholder-no-fix form-group' placeholder='نام' runat='server' />
                                </div>
                                <div class='col-md-4 '>
                                    <label>سن</label>
                                    <asp:TextBox ID='txtAgeGarden' type='text' class='form-control form-control-solid placeholder-no-fix form-group' placeholder='سن' runat='server' />
                                </div>

                                <div class='col-md-4'>
                                    <label>مساحت</label>
                                    <asp:TextBox runat='server' ID='txtArea' type='text' class='form-control form-control-solid placeholder-no-fix form-group' placeholder='مساحت' />
                                </div>
                            </div>

                            <div class=' row form-group '>
                                <div class='col-md-4 '>
                                    <label>عرض جغرافیایی</label>
                                    <asp:TextBox ID='txtLatitude' type='text' class='form-control form-control-solid placeholder-no-fix form-group' placeholder='عرض جغرافیایی' runat='server' />
                                </div>
                                <div class='col-md-4 '>
                                    <label>طول جغرافیایی</label>
                                    <asp:TextBox ID='txtLongitude' type='text' class='form-control form-control-solid placeholder-no-fix form-group' placeholder='طول جغرافیایی' runat='server' />
                                </div>
                                <div class='col-md-4'>
                                    <label>نوع باغ</label>
                                    <asp:DropDownList ID='ddGardenType' class='form-control ' runat='server'>
                                    </asp:DropDownList>
                                </div>

                                
                            </div>

                            <div class=' row form-group '>
                                <div class='col-md-4'>
                                    <label>محل</label>
                                    <asp:DropDownList ID='ddDistrict' runat='server' class='form-control'>
                                    </asp:DropDownList>
                                </div>
                                <div class='col-md-4'>
                                    <label>چاه خانه</label>
                                    <asp:DropDownList ID='ddCellar' runat='server' class='form-control'>
                                    </asp:DropDownList>
                                </div>
                                <div class='col-md-4'>
                                    <label>باغ دار</label>
                                    <asp:DropDownList ID='ddGardener' runat='server' class='form-control'>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class='form-actions' style='padding-bottom: 0px; padding-top: 0px;'>
                            <asp:Button ID='btnSubmit' type='submit' class='btn blue' Text='ثبت اطلاعات ' runat='server' OnClick='btnSubmit_Click' />
                            <asp:Button type='button' class='btn default' Text='انصراف' runat='server' />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>


<asp:Content ContentPlaceHolderID='PlaceScrips' runat='server'>
    <!-- BEGIN CORE PLUGINS -->
    <script src='../assets/global/plugins/jquery.min.js' type='text/javascript'></script>
    <script src='../assets/global/plugins/bootstrap/js/bootstrap.min.js' type='text/javascript'></script>
    <script src='../assets/global/plugins/js.cookie.min.js' type='text/javascript'></script>
    <script src='../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js' type='text/javascript'></script>
    <script src='../assets/global/plugins/jquery.blockui.min.js' type='text/javascript'></script>
    <script src='../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js' type='text/javascript'></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src='../assets/global/scripts/app.min.js' type='text/javascript'></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src='../assets/layouts/layout5/scripts/layout.min.js' type='text/javascript'></script>
    <script src='../assets/layouts/global/scripts/quick-sidebar.min.js' type='text/javascript'></script>
    <script src='../assets/layouts/global/scripts/quick-nav.min.js' type='text/javascript'></script>

    <!-- END THEME LAYOUT SCRIPTS -->
</asp:Content>