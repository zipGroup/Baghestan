﻿using Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace GhazvinBaghestan
{

    public partial class TreeEdit : BaseControl
    {

        int treeId;
        Common.Entity.Tree treeInputed;
        protected void Page_Load(object sender, EventArgs e)
        {

            CheckUserAccess();

            if (!IsPostBack)
            {
                if (Int32.TryParse(Request.QueryString["id"], out treeId) &&
                    (treeInputed = BLL.TreeBLL.GetRowByLevel(treeId, 2)) != null &&
                    treeInputed.TreeId > 0)
                {
                    txtAge.Text = treeInputed.Age.ToString();
                    txtGeneticInformation.Text = treeInputed.GeneticInformation;
                    txtName.Text = treeInputed.Name;
                    txtStatus.Text = treeInputed.Status;
                    this.fillGardenDropDown();
                    this.fillProductDropDown();

                }
                else
                {
                    Response.Redirect("NotFound.aspx");
                }
            }
        }


        private void fillProductDropDown()
        {
            ArrayList gardenList = BLL.ProductBLL.GetList();
            foreach (Common.Entity.Product nextProduct in gardenList)
            {
                ListItem foo = new ListItem(nextProduct.Name);
                foo.Value = nextProduct.ProductId.ToString();
                ddProduct.Items.Add(foo);
            }
        }

        private void fillGardenDropDown()
        {
            ArrayList gardenList = BLL.GardenBLL.GetList();
            foreach (Common.Entity.Garden nextGarden in gardenList)
            {
                ListItem foo = new ListItem(nextGarden.Name);
                foo.Value = nextGarden.GardenId.ToString();
                ddGarden.Items.Add(foo);
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {

            try
            {
                int age = Int32.Parse(txtAge.Text);
                int gardenId = Int32.Parse(ddGarden.SelectedValue);
                int productId = Int32.Parse(ddProduct.SelectedValue);
                String name = txtName.Text;
                String status = txtStatus.Text;
                String geneticInformation = txtGeneticInformation.Text;
                Common.Entity.Tree newTree = new Common.Entity.Tree()
                {
                    TreeId = treeInputed.TreeId,
                    Age = age,
                    Garden = new Common.Entity.Garden()
                    {
                        GardenId = gardenId
                    },
                    Product = new Common.Entity.Product()
                    {
                        ProductId = productId
                    },
                    Name = name,
                    Status = status,
                    GeneticInformation = geneticInformation
                };
                if (newTree.IsValid())
                {

                    try
                    {
                        BLL.TreeBLL.Update(newTree);
                        ShowAlert(Constants.Success_Add_Tree_Message, Constants.Success_Insert);
                    }
                    catch
                    {
                        ShowAlert(Constants.Error_Insert, Constants.Error_Insert);
                    }
                }
                else
                {
                    ShowAlert(Constants.Error_Add_Message, Constants.Error_Insert);
                    return;
                }
            }
            catch
            {
                ShowAlert(Constants.Error_Add_Message, Constants.Error_Insert);
            }
            return;

        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            BLL.TreeBLL.Delete(treeId);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("TreeList.aspx");
        }
    }
}